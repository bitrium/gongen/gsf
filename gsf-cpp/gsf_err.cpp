#include "gsf_err.hpp"
#include <gsf_err.h>

using namespace gsf;

error::error(int errno) : errno(errno) {
}

const char *error::what() const noexcept {
	return gsf_errno_string(errno);
}

error capture_error() {
	return error(gsf_errno_retrieve());
}

const char *not_found::what() const noexcept {
	return "not found or not available";
}
