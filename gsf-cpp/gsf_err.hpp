#include <exception>

namespace gsf {

class exception : public std::exception {
};

class error : public std::exception {
public:
	const int errno;

	explicit error(int errno);
	const char* what() const noexcept override;
};

class not_found : public std::exception {
	const char* what() const noexcept override;
};

error capture_error();

} // namespace gsf
