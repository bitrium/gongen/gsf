#include "gsf_types.hpp"
#include <gsf_types.h>

#include "gsf_err.hpp"
#include "gsf_context.hpp"
#include "gsf_data.hpp"

using namespace gsf;

#define GSF_EH(x) do {if (!(x)) throw capture_error();} while (0)


oTyping gsf::typing_from_string(cContext context, const char* string, size_t length) {
	oTyping result = oTyping();
	if (length) GSF_EH(gsf_typing_from_string_buf(result.c, string, length, context.c));
	else GSF_EH(gsf_typing_from_string(result.c, string, context.c));
	return result;
}

void gsf::typing_to_string(cTyping typing, char* string, size_t length) {
	GSF_EH(gsf_typing_to_string_buf(string, length, typing.c));
}


cTyping::cTyping(const gsf_typing* c) noexcept : c(c) {
}

cContext cTyping::get_context() const noexcept {
	return cContext(gsf_typing_get_context(c));
}

cType cTyping::get_type(size_t index) const {
	const gsf_type* result = gsf_typing_get_type(c, index);
	if (!result) throw not_found();
	return cType(result);
}

cType cTyping::get_default() const {
	const gsf_type* result = gsf_typing_get_default(c);
	if (!result) throw not_found();
	return cType(result);
}

cType cTyping::find_match(cType preset) const {
	const gsf_type* result = gsf_typing_matching_type(c, preset.c);
	if (!result) throw not_found();
	return cType(result);
}


mTyping::mTyping(gsf_typing* c) noexcept : cTyping(c), c(c) {
}

cTyping mTyping::as_const() const noexcept {
	return cTyping(c);
}

cType mTyping::add_type(cType type) const {
	const gsf_type* result = gsf_typing_add_type(c, type.c);
	GSF_EH(result);
	return cType(result);
}

cType mTyping::add_type(const char* type_string) const {
	const gsf_type* result = gsf_typing_add_type(c, type_string);
	GSF_EH(result);
	return cType(result);
}

void mTyping::set_default(size_t index) const {
	if (!gsf_typing_set_default(c, index)) throw not_found();
}

void mTyping::reset_default() const noexcept {
	gsf_typing_reset_default(c);
}

void mTyping::remove_type(size_t index) const noexcept {
	gsf_typing_remove_type(c, index);
}


oTyping::oTyping() : mTyping(nullptr) {
}

oTyping::oTyping(cContext context, size_t type_amount) : mTyping(nullptr) {
	c = gsf_typing_new(context.c, type_amount);
	GSF_EH(c);
}

oTyping::~oTyping() noexcept {
	gsf_delete_typing(c);
}


oType gsf::type_from_string(cContext context, const char* string, size_t length) {
	oType result = oType();
	if (length) GSF_EH(gsf_type_from_string_buf(result.c, string, length, context.c));
	else GSF_EH(gsf_type_from_string(result.c, string, context.c));
	return result;
}

void gsf::type_to_string(cType type, char* string, size_t length) {
	GSF_EH(gsf_type_to_string_buf(string, length, type.c));
}


cType::cType(const gsf_type* c) noexcept : c(c) {
}

cType cType::i8() {
	const gsf_type* result = gsf_get_type_i8();
	GSF_EH(result);
	return cType(result);
}

cType cType::i16() {
	const gsf_type* result = gsf_get_type_i16();
	GSF_EH(result);
	return cType(result);
}

cType cType::i32() {
	const gsf_type* result = gsf_get_type_i32();
	GSF_EH(result);
	return cType(result);
}

cType cType::i64() {
	const gsf_type* result = gsf_get_type_i64();
	GSF_EH(result);
	return cType(result);
}

cType cType::u8() {
	const gsf_type* result = gsf_get_type_u8();
	GSF_EH(result);
	return cType(result);
}

cType cType::u16() {
	const gsf_type* result = gsf_get_type_u16();
	GSF_EH(result);
	return cType(result);
}

cType cType::u32() {
	const gsf_type* result = gsf_get_type_u32();
	GSF_EH(result);
	return cType(result);
}

cType cType::u64() {
	const gsf_type* result = gsf_get_type_u64();
	GSF_EH(result);
	return cType(result);
}

cType cType::f32() {
	const gsf_type* result = gsf_get_type_f32();
	GSF_EH(result);
	return cType(result);
}

cType cType::f64() {
	const gsf_type* result = gsf_get_type_f64();
	GSF_EH(result);
	return cType(result);
}

cType cType::str() {
	const gsf_type* result = gsf_get_type_str();
	GSF_EH(result);
	return cType(result);
}

bool cType::matches(cType preset) const noexcept {
	return gsf_type_matches(c, preset.c);
}

bool cType::matches_strict(cType preset) const noexcept {
	return gsf_type_matches_strict(c, preset.c);
}

bool cType::is_primitive() const noexcept {
	return gsf_type_is_primitive(c);
}

bool cType::is_string() const noexcept {
	return gsf_type_is_string(c);
}

bool cType::is_scalar() const noexcept {
	return gsf_type_is_scalar(c);
}

bool cType::is_int() const noexcept {
	return gsf_type_is_int(c);
}

bool cType::is_uint() const noexcept {
	return gsf_type_is_uint(c);
}

bool cType::is_float() const noexcept {
	return gsf_type_is_float(c);
}

bool cType::is_non_primitive() const noexcept {
	return gsf_type_is_non_primitive(c);
}

bool cType::is_collection() const noexcept {
	return gsf_type_is_collection(c);
}

bool cType::is_array() const noexcept {
	return gsf_type_is_array(c);
}

bool cType::is_list() const noexcept {
	return gsf_type_is_list(c);
}

bool cType::is_map() const noexcept {
	return gsf_type_is_map(c);
}

bool cType::is_object() const noexcept {
	return gsf_type_is_object(c);
}

cType cType::element_type() const {
	const gsf_type* result = gsf_type_get_element_type(c);
	GSF_EH(result);
	return cType(result);
}

cTyping cType::element_typing() const {
	const gsf_typing* result = gsf_type_get_element_typing(c);
	GSF_EH(result);
	return cTyping(result);
}

cClass cType::object_class() const {
	const gsf_class* result = gsf_type_get_object_class(c);
	GSF_EH(result);
	return cClass(result);
}


mType::mType(gsf_type* c) noexcept : cType(c), c(c) {
}

cType mType::to_const() const noexcept {
	return cType(c);
}


oType::oType() : mType(nullptr) {
}

oType oType::array(cContext context, cType element) {
	oType result = oType();
	GSF_EH(gsf_type_init_array(result.c, element.c, context.c));
	return result;
}

oType oType::array(cContext context, const char* element_string) {
	oType result = oType();
	GSF_EH(gsf_type_init_array(result.c, element_string, context.c));
	return result;
}

oType oType::list(cContext context, cTyping element) {
	oType result = oType();
	GSF_EH(gsf_type_init_list(result.c, element.c, context.c));
	return result;
}

oType oType::list(cContext context, const char* element_string) {
	oType result = oType();
	GSF_EH(gsf_type_init_list(result.c, element_string, context.c));
	return result;
}

oType oType::map(cContext context, cTyping element) {
	oType result = oType();
	GSF_EH(gsf_type_init_map(result.c, element.c, context.c));
	return result;
}

oType oType::map(cContext context, const char* element_string) {
	oType result = oType();
	GSF_EH(gsf_type_init_map(result.c, element_string, context.c));
	return result;
}

oType oType::object(cClass type) {
	oType result = oType();
	GSF_EH(gsf_type_init_object(result.c, type.c));
	return result;
}

oType::~oType() noexcept {
	gsf_delete_type(c);
}
