#include "gsf_data.hpp"
#include <gsf_data.h>

#include <gsf_err.h>

#include "gsf_err.hpp"
#include "gsf_context.hpp"
#include "gsf_types.hpp"

#define GSF_EH(x) do {if (!(x)) throw capture_error();} while (0)

using namespace gsf;

cDescriptor::cDescriptor(const gsf_descriptor *c) noexcept : c(c) {
}

const char* cDescriptor::get_group() const noexcept {
	return gsf_descriptor_get_group(c);
}

const char* cDescriptor::get_name() const noexcept {
	return gsf_descriptor_get_name(c);
}

const char* cDescriptor::get_version() const noexcept {
	return gsf_descriptor_get_version(c);
}


mDescriptor::mDescriptor(gsf_descriptor* c) noexcept : cDescriptor(c), c(c) {
}

cDescriptor mDescriptor::as_const() const noexcept {
	return cDescriptor(c);
}

void mDescriptor::edit(const char* group, const char* name, const char* version) const {
	GSF_EH(gsf_descriptor_edit(c, group, name, version));
}

void mDescriptor::set_group(const char* group) const {
	GSF_EH(gsf_descriptor_edit(c, group, nullptr, nullptr));
}

void mDescriptor::set_name(const char* name) const {
	GSF_EH(gsf_descriptor_edit(c, nullptr, name, nullptr));
}

void mDescriptor::set_version(const char* version) const {
	GSF_EH(gsf_descriptor_edit(c, nullptr, nullptr, version));
}


oDescriptor::oDescriptor(const char* group, const char* name, const char* version) :
		mDescriptor(nullptr) {
	c = gsf_descriptor_new(group, name, version);
	GSF_EH(c);
}

oDescriptor::~oDescriptor() noexcept {
	gsf_delete_descriptor(c);
}


cSchema::cSchema(const gsf_schema* c) noexcept : c(c) {
}

cContext cSchema::get_context() const noexcept {
	return cContext(gsf_schema_get_context(c));
}

cDescriptor cSchema::get_descriptor() const noexcept {
	return cDescriptor(gsf_schema_get_descriptor(c));
}

cTyping cSchema::get_root() const noexcept {
	return cTyping(gsf_schema_get_root_const(c));
}

size_t cSchema::class_amount() const noexcept {
	return gsf_schema_classes_amount(c);
}

cClass cSchema::get_class(size_t index) const noexcept {
	return cClass(gsf_schema_get_class(c, index));
}


mSchema::mSchema(gsf_schema* c) noexcept : cSchema(c), c(c) {
}

cSchema mSchema::as_const() const noexcept {
	return cSchema(c);
}

void mSchema::import_other(cSchema imported) const {
	GSF_EH(gsf_schema_import_other(c, imported.c));
}

void mSchema::set_descriptor(cDescriptor descriptor) const noexcept {
	gsf_schema_set_descriptor(c, descriptor.c);
}

cTyping mSchema::get_root() const noexcept {
	return cTyping(gsf_schema_get_root(c));
}

void mSchema::set_root(cTyping typing) const {
	GSF_EH(gsf_schema_set_root(c, typing.c));
}

void mSchema::set_root(const char* typing_string) const {
	GSF_EH(gsf_schema_set_root(c, typing_string));
}

mClass mSchema::add_class(const char* name, size_t label_amount) const {
	gsf_class* result = gsf_schema_add_class(c, name, label_amount);
	GSF_EH(result);
	return mClass(result);
}

mClass mSchema::get_class(size_t index) const {
	gsf_class* result = gsf_schema_get_class(c, index);
	if (!result) throw not_found();
	return mClass(result);
}

void mSchema::remove_class(size_t index) const noexcept {
	gsf_schema_remove_class(c, index);
}


oSchema::oSchema(cDescriptor descriptor, cTyping root, size_t class_amount) : mSchema(nullptr) {
	c = gsf_schema_new(descriptor.c, root.c, class_amount);
	GSF_EH(c);
}

oSchema::oSchema(cDescriptor descriptor, const char* root_string, size_t class_amount) : mSchema(nullptr) {
	c = gsf_schema_new(descriptor.c, root_string, class_amount);
	GSF_EH(c);
}

oSchema::~oSchema() noexcept {
	gsf_delete_schema(c);
}


cClass::cClass(const gsf_class* c) noexcept : c(c) {
}

const char* cClass::get_name() const noexcept {
	return gsf_class_get_name(c);
}

size_t cClass::label_amount() const noexcept {
	return gsf_class_labels_amount(c);
}

cLabel cClass::get_label(size_t index) const {
	const gsf_label* result = gsf_class_get_label(c, index);
	if (!result) throw not_found();
	return cLabel(result);
}

cLabel cClass::get_label(const char* key) const {
	const gsf_label* result = gsf_class_get_label_at_key(c, key);
	if (!result) throw not_found();
	return cLabel(result);
}

size_t cClass::get_label_index(cLabel label) const {
	size_t result = gsf_class_index_at_label(c, label.c);
	if (!result--) throw not_found();
	return result;
}

size_t cClass::get_label_index(const char* key) const {
	size_t result = gsf_class_index_at_key(c, key);
	if (!result--) throw not_found();
	return result;
}


mClass::mClass(gsf_class* c) noexcept : cClass(c), c(c) {
}

cClass mClass::as_const() const noexcept {
	return cClass(c);
}

void mClass::set_name(const char* name) const {
	GSF_EH(gsf_class_set_name(c, name));
}

mLabel mClass::add_label(const char* key, cTyping typing) const {
	gsf_label* result = gsf_class_add_label(c, key, typing.c);
	GSF_EH(result);
	return mLabel(result);
}

mLabel mClass::add_label(const char* key, const char* typing_string) const {
	gsf_label* result = gsf_class_add_label(c, key, typing_string);
	GSF_EH(result);
	return mLabel(result);
}

mLabel mClass::get_label(size_t index) const {
	gsf_label* result = gsf_class_get_label(c, index);
	if (!result) throw not_found();
	return mLabel(result);
}

mLabel mClass::get_label(const char* key) const {
	gsf_label* result = gsf_class_get_label_at_key(c, key);
	if (!result) throw not_found();
	return mLabel(result);
}

void mClass::remove_label(size_t index) const noexcept {
	gsf_class_remove_label(c, index);
}


cLabel::cLabel(const gsf_label* c) noexcept : c(c) {
}

const char* cLabel::get_key() const noexcept {
	return gsf_label_get_key(c);
}

cTyping cLabel::get_typing() const noexcept {
	return cTyping(gsf_label_get_typing(c));
}

const char* cLabel::get_doc() const noexcept {
	return gsf_label_get_doc(c);
}


mLabel::mLabel(gsf_label* c) noexcept : cLabel(c), c(c) {
}

cLabel mLabel::as_const() const noexcept {
	return cLabel(c);
}

void mLabel::set_key(const char* key) const {
	GSF_EH(gsf_label_set_key(c, key));
}

void mLabel::set_typing(cTyping typing) const {
	GSF_EH(gsf_label_set_typing(c, typing.c));
}

void mLabel::set_typing(const char* typing_string) const {
	GSF_EH(gsf_label_set_typing(c, typing_string));
}

void mLabel::set_doc(const char* doc) const {
	GSF_EH(gsf_label_set_doc(c, doc));
}


cArticle::cArticle(const gsf_article* c) noexcept : c(c) {
}

cValue cArticle::root() const noexcept {
	return cValue(gsf_article_access_root(c));
}


mArticle::mArticle(gsf_article* c) noexcept : cArticle(c), c(c) {
}

cArticle mArticle::as_const() const noexcept {
	return cArticle(c);
}

mValue mArticle::root() const noexcept {
	return mValue(gsf_article_access_root(c));
}

oArticle::oArticle(mSchema schema) : mArticle(nullptr) {
	c = gsf_article_new(schema.c);
	GSF_EH(c);
}

oArticle::oArticle(cSchema schema) : mArticle(nullptr) {
	c = gsf_article_new_const_schema(schema.c);
	GSF_EH(c);
}

oArticle::~oArticle() noexcept {
	gsf_delete_article(c);
}


cValue::cValue(const gsf_value* c) noexcept : c(c) {
}

cTyping cValue::get_typing() const noexcept {
	return cTyping(gsf_value_get_typing(c));
}

cType cValue::get_type() const {
	const gsf_type* result = gsf_value_get_type(c);
	if (!result) throw not_found();
	return cType(result);
}

cObject cValue::decode_object() const {
	const gsf_object* result = gsf_value_decode_object(c);
	GSF_EH(result);
	return cObject(result);
}

cArray cValue::decode_array() const {
	const gsf_array* result = gsf_value_decode_array(c);
	GSF_EH(result);
	return cArray(result);
}

cList cValue::decode_list() const {
	const gsf_list* result = gsf_value_decode_list(c);
	GSF_EH(result);
	return cList(result);
}

cMap cValue::decode_map() const {
	const gsf_map* result = gsf_value_decode_map(c);
	GSF_EH(result);
	return cMap(result);
}

int8_t cValue::decode_i8() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	int8_t result = gsf_value_decode_i8(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

int16_t cValue::decode_i16() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	int16_t result = gsf_value_decode_i16(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

int32_t cValue::decode_i32() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	int32_t result = gsf_value_decode_i32(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

int64_t cValue::decode_i64() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	int64_t result = gsf_value_decode_i64(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

uint8_t cValue::decode_u8() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	uint8_t result = gsf_value_decode_u8(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

uint16_t cValue::decode_u16() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	uint16_t result = gsf_value_decode_u16(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

uint32_t cValue::decode_u32() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	uint32_t result = gsf_value_decode_u32(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

uint64_t cValue::decode_u64() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	uint64_t result = gsf_value_decode_u64(c);
	if (!result && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

float cValue::decode_f32() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	float result = gsf_value_decode_f32(c);
	if (result == 0 && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

double cValue::decode_f64() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	double result = gsf_value_decode_f64(c);
	if (result == 0 && gsf_errno) throw capture_error();
	gsf_errno = errno_bak;
	return result;
}

const char* cValue::decode_str() const {
	int errno_bak = gsf_errno;
	gsf_errno = gsf_enone;
	const char* result = gsf_value_decode_str(c);
	GSF_EH(result);
	gsf_errno = errno_bak;
	return result;
}


mValue::mValue(gsf_value* c) noexcept : cValue(c), c(c) {
}

cValue mValue::as_const() const noexcept {
	return cValue(c);
}

mObject mValue::encode_object(cClass type) const {
	gsf_object* result = gsf_value_encode_object(c, type.c);
	GSF_EH(result);
	return mObject(result);
}

mArray mValue::encode_array(cType type, size_t size) const {
	gsf_array* result = gsf_value_encode_array(c, type.c, size);
	GSF_EH(result);
	return mArray(result);
}

mArray mValue::encode_array(const char* type_string, size_t size) const {
	gsf_array* result = gsf_value_encode_array(c, type_string, size);
	GSF_EH(result);
	return mArray(result);
}

mList mValue::encode_list(cTyping typing, size_t size) const {
	gsf_list* result = gsf_value_encode_list(c, typing.c, size);
	GSF_EH(result);
	return mList(result);
}

mList mValue::encode_list(const char* typing_string, size_t size) const {
	gsf_list* result = gsf_value_encode_list(c, typing_string, size);
	GSF_EH(result);
	return mList(result);
}

mMap mValue::encode_map(cTyping typing, size_t size) const {
	gsf_map* result = gsf_value_encode_map(c, typing.c, size);
	GSF_EH(result);
	return mMap(result);
}

mMap mValue::encode_map(const char* typing_string, size_t size) const {
	gsf_map* result = gsf_value_encode_map(c, typing_string, size);
	GSF_EH(result);
	return mMap(result);
}

void mValue::encode_i8(int8_t value) const {
	GSF_EH(gsf_value_encode_i8(c, value));
}

void mValue::encode_i16(int16_t value) const {
	GSF_EH(gsf_value_encode_i16(c, value));
}

void mValue::encode_i32(int32_t value) const {
	GSF_EH(gsf_value_encode_i32(c, value));
}

void mValue::encode_i64(int64_t value) const {
	GSF_EH(gsf_value_encode_i64(c, value));
}

void mValue::encode_u8(uint8_t value) const {
	GSF_EH(gsf_value_encode_u8(c, value));
}

void mValue::encode_u16(uint16_t value) const {
	GSF_EH(gsf_value_encode_u16(c, value));
}

void mValue::encode_u32(uint32_t value) const {
	GSF_EH(gsf_value_encode_u32(c, value));
}

void mValue::encode_u64(uint64_t value) const {
	GSF_EH(gsf_value_encode_u64(c, value));
}

void mValue::encode_f32(float value) const {
	GSF_EH(gsf_value_encode_f32(c, value));
}

void mValue::encode_f64(double value) const {
	GSF_EH(gsf_value_encode_f64(c, value));
}

void mValue::encode_str(const char* value) const {
	GSF_EH(gsf_value_encode_str(c, value));
}


cObject::cObject(const gsf_object* c) noexcept : c(c) {
}

cClass cObject::get_class() const noexcept {
	return cClass(gsf_object_get_class(c));
}

cValue cObject::at_label(cLabel label) const {
	const gsf_value* result = gsf_object_access_at_label(c, label.c);
	GSF_EH(result);
	return cValue(result);
}

cValue cObject::at_key(const char* key) const {
	const gsf_value* result = gsf_object_access_at_key(c, key);
	GSF_EH(result);
	return cValue(result);
}


mObject::mObject(gsf_object* c) noexcept : cObject(c), c(c) {
}

cObject mObject::as_const() const noexcept {
	return cObject(c);
}

mValue mObject::at_label(cLabel label) const {
	gsf_value* result = gsf_object_access_at_label(c, label.c);
	GSF_EH(result);
	return mValue(result);
}

mValue mObject::at_key(const char* key) const {
	gsf_value* result = gsf_object_access_at_key(c, key);
	GSF_EH(result);
	return mValue(result);
}


cArray::cArray(const gsf_array* c) noexcept : c(c) {
}

size_t cArray::get_size() const noexcept {
	return gsf_array_get_size(c);
}

cType cArray::get_type() const noexcept {
	return cType(gsf_array_get_type(c));
}

const void* cArray::as_any() const noexcept {
	return gsf_array_as_any(c);
}

const int8_t* cArray::as_i8() const {
	const int8_t* result = gsf_array_as_i8(c);
	GSF_EH(result);
	return result;
}

const int16_t* cArray::as_i16() const {
	const int16_t* result = gsf_array_as_i16(c);
	GSF_EH(result);
	return result;
}

const int32_t* cArray::as_i32() const {
	const int32_t* result = gsf_array_as_i32(c);
	GSF_EH(result);
	return result;
}

const int64_t* cArray::as_i64() const {
	const int64_t* result = gsf_array_as_i64(c);
	GSF_EH(result);
	return result;
}

const uint8_t* cArray::as_u8() const {
	const uint8_t* result = gsf_array_as_u8(c);
	GSF_EH(result);
	return result;
}

const uint16_t* cArray::as_u16() const {
	const uint16_t* result = gsf_array_as_u16(c);
	GSF_EH(result);
	return result;
}

const uint32_t* cArray::as_u32() const {
	const uint32_t* result = gsf_array_as_u32(c);
	GSF_EH(result);
	return result;
}

const uint64_t* cArray::as_u64() const {
	const uint64_t* result = gsf_array_as_u64(c);
	GSF_EH(result);
	return result;
}

const float* cArray::as_f32() const {
	const float* result = gsf_array_as_f32(c);
	GSF_EH(result);
	return result;
}

const double* cArray::as_f64() const {
	const double* result = gsf_array_as_f64(c);
	GSF_EH(result);
	return result;
}


mArray::mArray(gsf_array* c) noexcept : cArray(c), c(c) {
}

cArray mArray::as_const() const noexcept {
	return cArray(c);
}

void mArray::resize(size_t size) const {
	GSF_EH(gsf_array_resize(c, size));
}

void* mArray::as_any() const noexcept {
	return gsf_array_as_any(c);
}

int8_t* mArray::as_i8() const {
	int8_t* result = gsf_array_as_i8(c);
	GSF_EH(result);
	return result;
}

int16_t* mArray::as_i16() const {
	int16_t* result = gsf_array_as_i16(c);
	GSF_EH(result);
	return result;
}

int32_t* mArray::as_i32() const {
	int32_t* result = gsf_array_as_i32(c);
	GSF_EH(result);
	return result;
}

int64_t* mArray::as_i64() const {
	int64_t* result = gsf_array_as_i64(c);
	GSF_EH(result);
	return result;
}

uint8_t* mArray::as_u8() const {
	uint8_t* result = gsf_array_as_u8(c);
	GSF_EH(result);
	return result;
}

uint16_t* mArray::as_u16() const {
	uint16_t* result = gsf_array_as_u16(c);
	GSF_EH(result);
	return result;
}

uint32_t* mArray::as_u32() const {
	uint32_t* result = gsf_array_as_u32(c);
	GSF_EH(result);
	return result;
}

uint64_t* mArray::as_u64() const {
	uint64_t* result = gsf_array_as_u64(c);
	GSF_EH(result);
	return result;
}

float* mArray::as_f32() const {
	float* result = gsf_array_as_f32(c);
	GSF_EH(result);
	return result;
}

double* mArray::as_f64() const {
	double* result = gsf_array_as_f64(c);
	GSF_EH(result);
	return result;
}


cList::cList(const gsf_list* c) noexcept : c(c) {
}

cTyping cList::get_typing() const noexcept {
	return cTyping(gsf_list_get_typing(c));
}

size_t cList::get_size() const noexcept {
	return gsf_list_get_size(c);
}

size_t cList::element_count() const noexcept {
	return gsf_list_elements_count(c);
}

cValue cList::get_element(size_t index) const {
	const gsf_value* result = gsf_list_get_element(c, index);
	if (!result) throw not_found();
	return cValue(result);
}


mList::mList(gsf_list* c) noexcept : cList(c), c(c) {
}

cList mList::as_const() const noexcept {
	return cList(c);
}

void mList::resize(size_t size) const {
	GSF_EH(gsf_list_resize(c, size));
}

mValue mList::get_element(size_t index) const {
	gsf_value* result = gsf_list_get_element(c, index);
	if (!result) throw not_found();
	return mValue(result);
}

mValue mList::add_element() const {
	gsf_value* result = gsf_list_add_element(c);
	GSF_EH(result);
	return mValue(result);
}


cMap::cMap(const gsf_map* c) noexcept : c(c) {
}

cTyping cMap::get_typing() const noexcept {
	return cTyping(gsf_map_get_typing(c));
}

size_t cMap::get_size() const noexcept {
	return gsf_map_get_size(c);
}

size_t cMap::element_count() const noexcept {
	return gsf_map_elements_count(c);
}

const char* cMap::get_key(size_t index) const {
	const char* result = gsf_map_get_key(c, index);
	if (!result) throw not_found();
	return result;
}

cValue cMap::get_value(size_t index) const {
	const gsf_value* result = gsf_map_get_value(c, index);
	if (!result) throw not_found();
	return cValue(result);
}

cValue cMap::get_value(const char* key) const {
	const gsf_value* result = gsf_map_get_value_at_key(c, key);
	if (!result) throw not_found();
	return cValue(result);
}


mMap::mMap(gsf_map* c) noexcept : cMap(c), c(c) {
}

cMap mMap::as_const() const noexcept {
	return cMap(c);
}

void mMap::resize(size_t size) const {
	GSF_EH(gsf_map_resize(c, size));
}

mValue mMap::get_value(size_t index) const {
	gsf_value* result = gsf_map_get_value(c, index);
	if (!result) throw not_found();
	return mValue(result);
}

mValue mMap::get_value(const char* key) const {
	gsf_value* result = gsf_map_get_value_at_key(c, key);
	if (!result) throw not_found();
	return mValue(result);
}

mValue mMap::add_element(const char* key) const {
	gsf_value* result = gsf_map_add_element(c, key);
	GSF_EH(result);
	return mValue(result);
}
