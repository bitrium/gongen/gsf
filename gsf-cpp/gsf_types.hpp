#pragma once

#include <cstdlib>

typedef struct gsf_typing gsf_typing;
typedef struct gsf_type gsf_type;

namespace gsf {

class cContext;
class cClass;

class cTyping;	class oTyping;
class cType;	class oType;

oTyping typing_from_string(cContext context, const char* string, size_t length = 0);
void typing_to_string(cTyping typing, char* string, size_t length);

class cTyping {
public:
	const gsf_typing* c;
	explicit cTyping(const gsf_typing* c) noexcept;

	cContext get_context() const noexcept;

	cType get_type(size_t index) const;
	cType get_default() const;
	cType find_match(cType preset) const;
};

class mTyping : public cTyping {
public:
	gsf_typing* c;
	explicit mTyping(gsf_typing* c) noexcept;
	cTyping as_const() const noexcept;

	cType add_type(cType type) const;
	cType add_type(const char* type_string) const;
	void set_default(size_t index) const;
	void reset_default() const noexcept;
	void remove_type(size_t index) const noexcept;
};

class oTyping : public mTyping {
private:
	oTyping();
	friend oTyping typing_from_string(cContext, const char*, size_t);

public:
	oTyping(cContext context, size_t type_amount);
	~oTyping() noexcept;
};


oType type_from_string(cContext context, const char* string, size_t length = 0);
void type_to_string(cType type, char* string, size_t length);

class cType {
public:
	const gsf_type* c;
	explicit cType(const gsf_type* c) noexcept;
	static cType i8();
	static cType i16();
	static cType i32();
	static cType i64();
	static cType u8();
	static cType u16();
	static cType u32();
	static cType u64();
	static cType f32();
	static cType f64();
	static cType str();

	bool matches(cType preset) const noexcept;
	bool matches_strict(cType preset) const noexcept;

	bool is_primitive() const noexcept;
	bool is_string() const noexcept;
	bool is_scalar() const noexcept;
	bool is_int() const noexcept;
	bool is_uint() const noexcept;
	bool is_float() const noexcept;
	bool is_non_primitive() const noexcept;
	bool is_collection() const noexcept;
	bool is_array() const noexcept;
	bool is_list() const noexcept;
	bool is_map() const noexcept;
	bool is_object() const noexcept;

	cType element_type() const;
	cTyping element_typing() const;
	cClass object_class() const;
};

class mType : public cType {
public:
	gsf_type* c;
	explicit mType(gsf_type* c) noexcept;
	cType to_const() const noexcept;
};

class oType : public mType {
private:
	oType();
	friend oType type_from_string(cContext, const char*, size_t);

public:
	static oType array(cContext context, cType element);
	static oType array(cContext context, const char* element_string);
	static oType list(cContext context, cTyping element);
	static oType list(cContext context, const char* element_string);
	static oType map(cContext context, cTyping element);
	static oType map(cContext context, const char* element_string);
	static oType object(cClass type);
	~oType() noexcept;
};

} // namespace gsf
