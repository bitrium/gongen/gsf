#include "gsf_context.hpp"
#include <gsf_context.h>

#include "gsf_err.hpp"
#include "gsf_data.hpp"

#define GSF_EH(x) do {if (!(x)) throw capture_error();} while (0)

using namespace gsf;

cContext::cContext(const gsf_context *c) noexcept : c(c) {
}

cClass cContext::get_class(const char *name) const {
	const gsf_class* result = gsf_context_class_by_name(c, name);
	if (!result) throw not_found();
	return cClass(result);
}


mContext::mContext(gsf_context* c) noexcept : cContext(c), c(c) {
}

cContext mContext::to_const() const noexcept {
	return cContext(c);
}

void mContext::import_schema(cSchema imported) const {
	GSF_EH(gsf_context_import_schema(c, imported.c));
}

void mContext::add_class(cClass added) const {
	GSF_EH(gsf_context_add_class(c, added.c));
}


oContext::oContext() : mContext(nullptr) {
	c = gsf_context_new();
	GSF_EH(c);
}

oContext::~oContext() noexcept {
	gsf_delete_context(c);
}
