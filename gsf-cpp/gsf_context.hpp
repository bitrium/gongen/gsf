#pragma once

typedef struct gsf_context gsf_context;

namespace gsf {

class cSchema;
class cClass;

class cContext {
public:
	const gsf_context *c;
	explicit cContext(const gsf_context *c) noexcept;

	cClass get_class(const char *name) const;
};

class mContext : public cContext {
public:
	gsf_context* c;
	explicit mContext(gsf_context* c) noexcept;
	cContext to_const() const noexcept;

	void import_schema(cSchema imported) const;
	void add_class(cClass added) const;
};

class oContext : public mContext {
public:
	oContext();
	~oContext() noexcept;
};

} // namespace gsf
