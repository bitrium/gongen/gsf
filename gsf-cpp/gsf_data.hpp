#pragma once

#include <cstdlib>
#include <cstdint>

typedef struct gsf_descriptor gsf_descriptor;
typedef struct gsf_schema gsf_schema;
typedef struct gsf_class gsf_class;
typedef struct gsf_label gsf_label;
typedef struct gsf_article gsf_article;
typedef struct gsf_value gsf_value;
typedef struct gsf_object gsf_object;
typedef struct gsf_array gsf_array;
typedef struct gsf_map gsf_map;
typedef struct gsf_list gsf_list;

namespace gsf {

class cContext;	class mContext;
class cTyping;	class mTyping;
class cType;	class mType;
class cClass;	class mClass;
class cLabel;	class mLabel;
class cValue;	class mValue;
class cObject;	class mObject;
class cArray;	class mArray;
class cList;	class mList;
class cMap;		class mMap;

// TODO iterator classes

class cDescriptor {
public:
	const gsf_descriptor* c;
	explicit cDescriptor(const gsf_descriptor* c) noexcept;

	const char* get_group() const noexcept;
	const char* get_name() const noexcept;
	const char* get_version() const noexcept;
};

class mDescriptor : public cDescriptor {
public:
	gsf_descriptor* c;
	explicit mDescriptor(gsf_descriptor* c) noexcept;
	cDescriptor as_const() const noexcept;

	void edit(const char* group, const char* name, const char* version) const;
	void set_group(const char* group) const;
	void set_name(const char* name) const;
	void set_version(const char* version) const;
};

class oDescriptor : public mDescriptor {
public:
	oDescriptor(const char* group, const char* name, const char* version);
	~oDescriptor() noexcept;
};


class cSchema {
public:
	const gsf_schema* c;
	explicit cSchema(const gsf_schema* c) noexcept;

	cContext get_context() const noexcept;
	cDescriptor get_descriptor() const noexcept;
	cTyping get_root() const noexcept;

	size_t class_amount() const noexcept;
	cClass get_class(size_t index) const noexcept;
};

class mSchema : public cSchema {
public:
	gsf_schema* c;
	explicit mSchema(gsf_schema* c) noexcept;
	cSchema as_const() const noexcept;

	void import_other(cSchema imported) const;

	void set_descriptor(cDescriptor descriptor) const noexcept;
	cTyping get_root() const noexcept;

	void set_root(cTyping typing) const;
	void set_root(const char* typing_string) const;

	mClass add_class(const char* name, size_t label_amount) const;
	mClass get_class(size_t index) const;
	void remove_class(size_t index) const noexcept;
};

class oSchema : public mSchema {
public:
	oSchema(cDescriptor descriptor, cTyping root, size_t class_amount);
	oSchema(cDescriptor descriptor, const char* root_string, size_t class_amount);
	~oSchema() noexcept;
};


class cClass {
public:
	const gsf_class* c;
	explicit cClass(const gsf_class* c) noexcept;

	const char* get_name() const noexcept;

	size_t label_amount() const noexcept;
	cLabel get_label(size_t index) const;
	cLabel get_label(const char* key) const;
	size_t get_label_index(cLabel label) const;
	size_t get_label_index(const char* key) const;
};

class mClass : public cClass {
public:
	gsf_class* c;
	explicit mClass(gsf_class* c) noexcept;
	cClass as_const() const noexcept;

	void set_name(const char* name) const;

	mLabel add_label(const char* key, cTyping typing) const;
	mLabel add_label(const char* key, const char* typing_string) const;
	mLabel get_label(size_t index) const;
	mLabel get_label(const char* key) const;
	void remove_label(size_t index) const noexcept;
};


class cLabel {
public:
	const gsf_label* c;
	explicit cLabel(const gsf_label* c) noexcept;

	const char* get_key() const noexcept;
	cTyping get_typing() const noexcept;
	const char* get_doc() const noexcept;
};

class mLabel : public cLabel {
public:
	gsf_label* c;
	explicit mLabel(gsf_label* c) noexcept;
	cLabel as_const() const noexcept;

	void set_key(const char* key) const;
	void set_typing(cTyping typing) const;
	void set_typing(const char* typing_string) const;
	void set_doc(const char* doc) const;
};


class cArticle {
public:
	const gsf_article* c;
	explicit cArticle(const gsf_article* c) noexcept;

	cValue root() const noexcept;
};

class mArticle : public cArticle {
public:
	gsf_article* c;
	explicit mArticle(gsf_article* c) noexcept;
	cArticle as_const() const noexcept;

	mValue root() const noexcept;
};


class oArticle : public mArticle {
public:
	explicit oArticle(mSchema schema);
	explicit oArticle(cSchema schema);
	~oArticle() noexcept;
};


class cValue {
public:
	const gsf_value* c;
	explicit cValue(const gsf_value* c) noexcept;

	cTyping get_typing() const noexcept;
	cType get_type() const;

	cObject decode_object() const;
	cArray decode_array() const;
	cList decode_list() const;
	cMap decode_map() const;
	int8_t decode_i8() const;
	int16_t decode_i16() const;
	int32_t decode_i32() const;
	int64_t decode_i64() const;
	uint8_t decode_u8() const;
	uint16_t decode_u16() const;
	uint32_t decode_u32() const;
	uint64_t decode_u64() const;
	float decode_f32() const;
	double decode_f64() const;
	const char* decode_str() const;
};

class mValue : public cValue {
public:
	gsf_value* c;
	explicit mValue(gsf_value* c) noexcept;
	cValue as_const() const noexcept;

	mObject encode_object(cClass type) const;
	mArray encode_array(cType type, size_t size) const;
	mArray encode_array(const char* type_string, size_t size) const;
	mList encode_list(cTyping typing, size_t size) const;
	mList encode_list(const char* typing_string, size_t size) const;
	mMap encode_map(cTyping typing, size_t size) const;
	mMap encode_map(const char* typing_string, size_t size) const;
	void encode_i8(int8_t value) const;
	void encode_i16(int16_t value) const;
	void encode_i32(int32_t value) const;
	void encode_i64(int64_t value) const;
	void encode_u8(uint8_t value) const;
	void encode_u16(uint16_t value) const;
	void encode_u32(uint32_t value) const;
	void encode_u64(uint64_t value) const;
	void encode_f32(float value) const;
	void encode_f64(double value) const;
	void encode_str(const char* value) const;
};


class cObject {
public:
	const gsf_object* c;
	explicit cObject(const gsf_object* c) noexcept;

	cClass get_class() const noexcept;

	cValue at_label(cLabel label) const;
	cValue at_key(const char* key) const;
};

class mObject : public cObject {
public:
	gsf_object* c;
	explicit mObject(gsf_object* c) noexcept;
	cObject as_const() const noexcept;

	mValue at_label(cLabel label) const;
	mValue at_key(const char* key) const;
};


class cArray {
public:
	const gsf_array* c;
	explicit cArray(const gsf_array* c) noexcept;

	size_t get_size() const noexcept;
	cType get_type() const noexcept;

	const void* as_any() const noexcept;
	const int8_t* as_i8() const;
	const int16_t* as_i16() const;
	const int32_t* as_i32() const;
	const int64_t* as_i64() const;
	const uint8_t* as_u8() const;
	const uint16_t* as_u16() const;
	const uint32_t* as_u32() const;
	const uint64_t* as_u64() const;
	const float* as_f32() const;
	const double* as_f64() const;
};

class mArray : public cArray {
public:
	gsf_array* c;
	explicit mArray(gsf_array* c) noexcept;
	cArray as_const() const noexcept;

	void resize(size_t size) const;

	void* as_any() const noexcept;
	int8_t* as_i8() const;
	int16_t* as_i16() const;
	int32_t* as_i32() const;
	int64_t* as_i64() const;
	uint8_t* as_u8() const;
	uint16_t* as_u16() const;
	uint32_t* as_u32() const;
	uint64_t* as_u64() const;
	float* as_f32() const;
	double* as_f64() const;
};


class cList {
public:
	const gsf_list* c;
	explicit cList(const gsf_list* c) noexcept;

	cTyping get_typing() const noexcept;

	size_t get_size() const noexcept;
	size_t element_count() const noexcept;

	cValue get_element(size_t index) const;
};

class mList : public cList {
public:
	gsf_list* c;
	explicit mList(gsf_list* c) noexcept;
	cList as_const() const noexcept;

	void resize(size_t size) const;

	mValue get_element(size_t index) const;
	mValue add_element() const;
};


class cMap {
public:
	const gsf_map* c;
	explicit cMap(const gsf_map* c) noexcept;

	cTyping get_typing() const noexcept;

	size_t get_size() const noexcept;
	size_t element_count() const noexcept;

	const char* get_key(size_t index) const;
	cValue get_value(size_t index) const;
	cValue get_value(const char* key) const;
};

class mMap : public cMap {
public:
	gsf_map* c;
	explicit mMap(gsf_map* c) noexcept;
	cMap as_const() const noexcept;

	void resize(size_t size) const;

	mValue get_value(size_t index) const;
	mValue get_value(const char* key) const;

	mValue add_element(const char* key) const;
};

} // namespace gsf
