add_executable(gsf-test "main.c")
target_include_directories(gsf-test PRIVATE ${gsf-lib-directories})
target_link_libraries(gsf-test ${gsf-lib-binary})
