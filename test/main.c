#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include <gsf_err.h>
#include <gsf_context.h>
#include <gsf_data.h>
#include <gsf_types.h>
#include <gsf_itm.h>
#include <gsf_codec.h>

/*
// notation hash tester
int main(int argc, char* argv[]) {
	gsf_itm* itm = gsf_create_itm();
	if (!itm || !gsf_itm_init(itm)) {
		gsf_errno_display();
		gsf_delete_itm(itm);
		return 0;
	}
	char str[32];
	uint32_t hash;
	while (1) {
		fgets(str, 32, stdin);
		if (str[strlen(str) - 1] == '\n') str[strlen(str) - 1] = '\0';
		if (gsf_itm_hash_notation(itm, str, strlen(str), &hash)) printf("%i\n", hash);
		else gsf_errno_display();
	}
	return 0;
}

#define ERR_CHECK(x) do { if (!(x)) { gsf_errno_display_keep(); return gsf_errno; } } while (0)

int main(int argc, char* argv[]) {
	gsf_context* ctx = gsf_context_new();
	ERR_CHECK(ctx);
	gsf_schema* schema = gsf_context_new_schema_name(ctx, "test", "Main", 2);
	ERR_CHECK(schema);

	gsf_enum* enu = gsf_schema_add_enum(schema, "Color", 3);
	ERR_CHECK(enu);
	gsf_variant* var_red = gsf_enum_add_variant(enu, "red");
	ERR_CHECK(var_red);
	gsf_variant* var_green = gsf_enum_add_variant(enu, "green");
	ERR_CHECK(var_green);
	gsf_variant* var_blue = gsf_enum_add_variant(enu, "blue");
	ERR_CHECK(var_blue);

	gsf_class* class;
	gsf_label* label;
	ERR_CHECK(class = gsf_schema_add_class(schema, "Inner", 2));
	ERR_CHECK(label = gsf_class_add_label(class, "name", "str"));
	ERR_CHECK(gsf_label_set_doc(label, "the name"));
	ERR_CHECK(label = gsf_class_add_label(class, "values", "arr[i64]"));
	ERR_CHECK(gsf_label_set_doc(label, "some numbers"));
	ERR_CHECK(class = gsf_schema_add_class(schema, "Main", 3));
	ERR_CHECK(gsf_class_add_label(class, "color", "Color"));
	ERR_CHECK(gsf_class_add_label(class, "objects", "lst[str|Inner]"));
	ERR_CHECK(label = gsf_class_add_label(class, "recursive", "!Main|nul"));
	ERR_CHECK(gsf_value_encode_null(gsf_label_access_default(label)));

	const char* test =
			"@test\n"
			"\n"
			"color: red\n"
			"objects:\n"
			"  .str: some string\n"
			"  .Inner:\n"
			"    name: inner object\n"
			"    values: 10; -20; 0; 85; 5\n"
			"  .str:\n"
			"    another string\n"
			"    but multiline\n";
	gsf_article* article = gsf_create_article();
	ERR_CHECK(article);
	ERR_CHECK(gsf_txt_article_read(article, schema, test, 0, GSF_TXT_READ_STRING));

	return 0;
}
*/

static const char* schema_txt =
		"group: com.example\n"
		"name: example\n"
		"version: 0.2.3\n"
		"root: MainObject\n"
		"classes:\n"
		"  SomeObject:\n"
		"    name: str\n"
		"    data: arr[u32]|!lst[str]\n"
		"    more_data: !arr[u16]|nul.nul =\n"
		"  MainObject:\n"
		"    number1: u8 / a number\n"
		"    number2: u32 / another number\n"
		"    number3: u32 = 10 / a number with a default\n"
		"    float1: f32 / doc for a float\n"
		"    float2: f64 / doc for another float\n"
		"    float3: f32|f64\n"
		"    text1: str / doc\n"
		"    text2: str\n"
		"    data1: lst[!u32|u16] / list of mixed (default) u32's and u16's\n"
		"    data2: map[str]\n"
		"    data3: lst[SomeObject]\n"
		"    data4: lst[str]\n"
		"    data5: lst[SomeObject|i32|!str]\n"
		"    choice: SomeEnum\n"
		"    any: ?\n"
		"enums:\n"
		"  SomeEnum:\n"
		"    : some_choice / the first choice\n"
		"    : another_choice\n";
static const char* article_txt =
		"@com.example:example:0.2.3\n"
		"\n"
		"number1.u08: 110\n"
		"number2: 1734914781\n"
		"float1: 1.6532524\n"
		"float2.f64: 12.12478195915124\n"
		"float3.f32: 10.543\n"
		"text1: # aaa\n"
		"  This is a test text! # bla bla\n"
		"  A second line\n"
		"  and the third one.\n"
		"text2: An another test text :)\n"
		"data1:\n"
		"  : 91749701\n"
		"  : 10470174\n"
		"  : 101290\n"
		"  : 82018\n"
		"  .u32: 7198\n"
		"  .u32: 8401\n"
		"  .u16: 1\n"
		"  .u16: 0\n"
		"  .u16: 24\n"
		"  .u16: 57\n"
		"data2:\n"
		"  key1: str1\n"
		"  key2.str: str5\n"
		"  key3: 19410704\n"
		"data3:\n"
		"  :\n"
		"    name: Example object\n"
		"    data.arr[u32]:\n"
		"      13\n"
		"      45\n"
		"  :\n"
		"    name: Compact array notation\n"
		"    data.arr[u32]: 13; 45\n"
		"  :\n"
		"    : Another example object\n"
		"    .lst[str]:\n"
		"      : First value\n"
		"      : Second value\n"
		"    : # more data\n"
		"      12; 19; 10;\n"
		"      10; 2323\n"
		"  :\n"
		"    name: Last object\n"
		"    data:\n"
		"      : A value\n"
		"      .str: Another value\n"
		"      : One last value\n"
		"data4:\n"
		"data5:\n"
		"  .SomeObject:\n"
		"    name: An object\n"
		"    data:\n"
		"  :\n"
		"    name: I am actually not SomeObject!\n"
		"    data:\n"
		"      : This is actually\n"
		"      : just a string\n"
		"      : and not an object\n"
		"  .i32: 12\n"
		"  .str: 12\n"
		"choice: another_choice\n"
		"any.lst[?]:\n"
		"  .i32: -12\n"
		"  .SomeEnum: some_choice\n";

void test_data(gsf_descriptor* desc, gsf_schema* schema, gsf_article* article) {
	gsf_descriptor_init(desc, "com.example", "test", "1.0.0");
	gsf_schema_init(schema, desc, "Class2", 2);

	gsf_class* class1 = gsf_schema_add_class(schema, "Class1", 1);
	gsf_label* int_label = gsf_class_add_label(class1, "int_value", "i32");
	gsf_label_set_doc(int_label, "An integer.");

	gsf_class* class2 = gsf_schema_add_class(schema, "Class2", 3);
	gsf_class_add_label(class2, "string_values_1", "str|!lst[str]");
	int_label = gsf_class_add_label(class2, "string_values_2", "str|!lst[str]");
	puts(gsf_typing_to_string(gsf_label_get_typing(int_label)));
	gsf_class_add_label(class2, "integer_compound", "Class1");

	gsf_article_init(article, schema);

	gsf_object* root = gsf_value_encode_object(gsf_article_access_root(article), class2);
	gsf_value_encode_str(gsf_object_access_at_key(root, "string_values_1"), "String value.");
	gsf_list* list = gsf_value_encode_list(gsf_object_access_at_key(root, "string_values_2"), 0, 2);
	gsf_value_encode_str(gsf_list_add_element(list), "List member.");
	gsf_value_encode_str(gsf_list_add_element(list), "Another member.");
	gsf_object* object = gsf_value_encode_object(gsf_object_access_at_key(root, "integer_compound"), class1);
	gsf_value_encode_i32(gsf_object_access_at_label(object, int_label), 123);

	gsf_article_destroy(article);
	gsf_schema_destroy(schema);
	gsf_descriptor_destroy(desc);
}

void test_textual(gsf_descriptor* desc, gsf_schema* schema, gsf_article* article) {
	struct gsf_reader reader = {0};
	reader.format = GSF_CODEC_TEXTUAL;
	gsf_reader_create_buf(&reader, schema_txt, strlen(schema_txt) + 1);
	if (!gsf_read_schema(&reader, schema, desc)) {
		gsf_errno_display();
		goto CLEANUP;
	}
	gsf_reader_delete_buf(&reader);
	gsf_reader_create_buf(&reader, article_txt, strlen(article_txt) + 1);
	if (!gsf_read_article(&reader, article, schema)) {
		gsf_errno_display();
		goto CLEANUP;
	}
	gsf_reader_delete_buf(&reader);

	gsf_object* root = gsf_value_as_object(gsf_article_access_root(article), 0);
	puts(gsf_value_decode_str(gsf_object_access_at_key(root, "text1")));
	puts(gsf_value_decode_str(gsf_object_access_at_key(root, "text2")));
	gsf_map* data2 = gsf_value_as_map(gsf_object_access_at_key(root, "data2"), 0, 0);
	printf("%s\n", gsf_value_decode_str(gsf_map_get_value_at_key(data2, "key2")));
	for (size_t i = 0; i < 3; i++) {
		const char* key = gsf_map_get_key(data2, i);
		const char* val = gsf_value_decode_str(gsf_map_get_value(data2, i));
		printf("%s: %s\n", key, val);
	}
	gsf_map_remove_at_key(data2, "key2");

	char* write_test = malloc(1024);
	struct gsf_writer writer;
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = GSF_TXT_WRITE_SIGNATURE /*| GSF_TXT_WRITE_ARTICLE_STRONG_TYPES_ALL*/ | GSF_TXT_WRITE_LEADING_ZERO;
	gsf_writer_create_growing_buf(&writer, (void**) &write_test, 1024);
	if (gsf_write_article(&writer, article)) {
		puts("");
		puts(write_test);
	} else {
		gsf_errno_display();
	}
	gsf_writer_delete_growing_buf(&writer);
	// gsf_txt_schema_write(schema, write_test, 10000, GSF_TXT_WRITE_SIGNATURE | GSF_TXT_WRITE_LEADING_ZERO);
	// puts("");
	// puts(write_test);
	free(write_test);

	/* TODO schemaless write of an article with classes
	gsf_article* schemaless = gsf_article_new(0);
	gsf_value_copy(gsf_article_access_root(schemaless), gsf_article_access_root(article));
	write_test = malloc(1024);
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = GSF_TXT_WRITE_SIGNATURE | GSF_TXT_WRITE_LEADING_ZERO;
	gsf_writer_create_growing_buf(&writer, (void**) &write_test, 1024);
	if (gsf_write_article(&writer, schemaless)) {
		puts("");
		puts(write_test);
	} else {
		gsf_errno_display();
	}
	gsf_writer_delete_growing_buf(&writer);
	free(write_test);
	*/

	CLEANUP:
	puts("");
	gsf_article_destroy(article);
	gsf_schema_destroy(schema);
	gsf_descriptor_destroy(desc);
}

void test_binary(gsf_descriptor* desc, gsf_schema* schema, gsf_article* article) {
	struct gsf_reader reader = {0};
	struct gsf_writer writer = {0};

	reader.format = GSF_CODEC_TEXTUAL;
	gsf_reader_create_buf(&reader, schema_txt, strlen(schema_txt) + 1);
	if (!gsf_read_schema(&reader, schema, desc)) {
		goto CLEANUP;
	}
	gsf_reader_delete_buf(&reader);
	gsf_reader_create_buf(&reader, article_txt, strlen(article_txt) + 1);
	if (!gsf_read_article(&reader, article, schema)) {
		goto CLEANUP;
	}
	clock_t start, end;

	writer.format = GSF_CODEC_BINARY;
	void* buf = 0;
	gsf_writer_create_growing_buf(&writer, &buf, 0);
	start = clock();
	if (!gsf_write_article(&writer, article)) {
		gsf_errno_print();
		goto CLEANUP;
	}
	size_t buf_size = writer.ptr - buf;
	gsf_writer_delete_growing_buf(&writer);
	end = clock();
	printf("write took: %fs\n", (double) (end - start) / CLOCKS_PER_SEC);
	gsf_article_destroy(article);
	reader.format = GSF_CODEC_BINARY;
	gsf_reader_create_buf(&reader, buf, buf_size);
	start = clock();
	if (!gsf_read_article(&reader, article, schema)) {
		gsf_errno_print();
		goto CLEANUP;
	}
	end = clock();
	printf("read took: %fs\n", (double) (end - start) / CLOCKS_PER_SEC);
	printf("size: %zuB\n", buf_size);
	free(buf);

	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = GSF_TXT_WRITE_SIGNATURE | GSF_TXT_WRITE_ARTICLE_STRONG_TYPES_ALL | GSF_TXT_WRITE_LEADING_ZERO;
	char* write_test = 0;
	gsf_writer_create_growing_buf(&writer, (void**) &write_test, 0);
	puts("\nas textual:");
	gsf_write_article(&writer, article);
	gsf_writer_delete_growing_buf(&writer);
	puts(write_test);
	free(write_test);

	CLEANUP:
	puts("");
	gsf_article_destroy(article);
	gsf_schema_destroy(schema);
	gsf_descriptor_destroy(desc);
}

void test_schemaless(gsf_descriptor* desc, gsf_schema* schema, gsf_article* article) {
	gsf_article_init(article, 0);

	gsf_map* root = gsf_value_encode_map(gsf_article_access_root(article), "?", 3);
	gsf_map* stuff = gsf_value_encode_map(gsf_map_add_element(root, "stuff"), "?", 2);
	gsf_value_encode_str(gsf_map_add_element(stuff, "something"), "some data");
	gsf_value_encode_i64(gsf_map_add_element(stuff, "a_number"), 12423);
	gsf_list* list = gsf_value_encode_list(gsf_map_add_element(root, "strings"), "str", 2);
	gsf_value_encode_str(gsf_list_add_element(list), "a");
	gsf_value_encode_str(gsf_list_add_element(list), "b");
	gsf_value_encode_list(gsf_map_add_element(root, "empty"), "?", 0);

	char* write_test = malloc(1024);
	struct gsf_writer writer = {0};
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = GSF_TXT_WRITE_SIGNATURE | GSF_TXT_WRITE_LEADING_ZERO;
	gsf_writer_create_growing_buf(&writer, (void**) &write_test, 1024);
	if (gsf_write_article(&writer, article)) {
		puts("");
		puts(write_test);
	} else {
		gsf_errno_display();
	}
	gsf_writer_delete_growing_buf(&writer);
	free(write_test);

	gsf_article_destroy(article);
}

extern void* gsf_bin_vsize_write(size_t value, void* ptr, void** buf, size_t* size, size_t rsize);
extern const void* gsf_bin_vsize_read(size_t* value, const void* buf, size_t buf_size);

/*
void test_vsize(size_t val) {
	printf("%010zu -> ", val);
	size_t size = 10;
	uint8_t* buf = calloc(size, 1);
	gsf_bin_vsize_write(val, buf, (void**) &buf, &size, 0);
	for (uint8_t i = 0; i < 10; i++) {
		printf("%.2x ", buf[i]);
	}
	gsf_bin_vsize_read(&val, buf, 10);
	printf("-> %010zu\n", val);
}
*/

int main() {
	/*
	test_vsize(589234149);
	test_vsize(12);
	test_vsize(26195091);
	test_vsize(3244193);
	test_vsize(2060220265);
	test_vsize(8932);
	test_vsize(22668996);
	test_vsize(636);
	puts("");
	*/

	gsf_descriptor* desc = gsf_create_descriptor();
	gsf_schema* schema = gsf_create_schema();
	gsf_article* article = gsf_create_article();

	test_data(desc, schema, article);
	puts("");
	test_textual(desc, schema, article);
	puts("");
	test_binary(desc, schema, article);
	puts("");
	test_schemaless(desc, schema, article);
	puts("");

// 	const char* test =
// "group: wars-of-nations\n"
// "name: package\n"
// "version: 1.0\n"
// "root: Map\n"
// "classes:\n"
// "  Map:\n";

// 	if (!gsf_txt_schema_read(schema, desc, test, strlen(test), 0)) {
// 		printf("%s\n", gsf_errno_retrieve_string());
// 	}

	gsf_delete_article(article);
	gsf_delete_schema(schema);
	gsf_delete_descriptor(desc);

	return EXIT_SUCCESS;
}
