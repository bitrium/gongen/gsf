import gongsf as gsf
import faulthandler

schema_txt = '''
group: bitrium.gongen
name: gongen-sources
version: 0.0.0
root: Engine
classes:
  File:
    path: str
    platforms: lst[str]
  Module:
    name: str
    sources: lst[!str|File]
  Engine:
    name: str
    codename: str
    version: str
    modules: lst[Module]
'''

faulthandler.enable()

descriptor, schema = gsf.codec.read_schema_txt(schema_txt)

print(f'Descriptor: {descriptor.group}:{descriptor.name}:{descriptor.version}')
print(f'Root: {schema.root}')
print('Classes:')
for i in range(schema.class_count):
    class_ = schema.get_class(i)
    print(class_.name)
    for j in range(class_.label_count):
        label = class_.get_label(j)
        print(f'- {label.key}: {label.typing}')

faulthandler.disable()
