/**
 * @file gsf_itm.h
 * @brief Declares a type manager used by contexts or loose schemas (without a context).
 *
 * 'ITM' expands into 'Internal Type Manager'.
 *
 * This is an internal functionality and few applications will find this interface 
 * directly useful (most functions that take types or typings as arguments invoke string
 * parsing by themselves). For interfering with types or typings, use 'gsf_types.h'.
 *
 * The interface also includes conversion between strings and types or typings.
 */
#pragma once

#include <stddef.h>

#ifndef __cplusplus
#include <stdint.h>
#else
#include <cstdint>
#define class class_
extern "C" {
#endif

typedef struct gsf_itm gsf_itm;
typedef struct gsf_class gsf_class;
typedef struct gsf_enum gsf_enum;
typedef struct gsf_type gsf_type;
typedef struct gsf_typing gsf_typing;
typedef void gsf_typing_or_char;
typedef void gsf_type_or_char;


/**
 * @brief Gets the parent manager of a type.
 * @param self Type specification.
 * @return Manager for that type.
 */
gsf_itm* gsf_type_manager(const gsf_type* self);
/**
 * @brief Gets the parent manager of a typing.
 * @param self Typing specification
 * @return Manager for that typing.
 */
gsf_itm* gsf_typing_manager(const gsf_typing* self);


/**
 * @brief Creates a type manager.
 * @return Allocated type manager.
 */
gsf_itm* gsf_create_itm(void);
/**
 * @brief Deletes a type manager.
 * @param manager Freed type manager.
 */
void gsf_delete_itm(gsf_itm* manager);

/**
 * @brief Initializes a type manager.
 * @param self Type manager.
 * @return Same as self or null pointer on error.
 */
gsf_itm* gsf_itm_init(gsf_itm* self);
/**
 * @brief Destroys a type manager.
 * @param self Type manager.
 */
void gsf_itm_destroy(gsf_itm* self);

int gsf_itm_hash_name(const gsf_itm* self, const char* data, size_t length, uint32_t* out);
int gsf_itm_hash_notation(const gsf_itm* self, const char* data, size_t length, uint32_t* out);

int gsf_itm_add_alias_bufs(gsf_itm* self, const char* a, size_t a_len, const char* b, size_t b_len);
int gsf_itm_add_alias(gsf_itm* self, const char* a, const char* b);

/**
 * @brief Reserves additional memory for types.
 * @param self Type manager.
 * @param amount Minimal count of new uninitialized types.
 * @return Amount of free space after operation or 0 on error.
 */
size_t gsf_itm_reserve_types(gsf_itm* self, size_t amount);
/**
 * @brief Reserves additional memory for typings.
 * @param self Type manager.
 * @param amount Minimal count of new uninitialized typings.
 * @return Amount of free space after operation or 0 on error.
 */
size_t gsf_itm_reserve_typings(gsf_itm* self, size_t amount);

/**
 * @brief Adds a class to the type manager and prepares it for type usage.
 * @param self Type manager.
 * @param class The added class.
 * @return Type specification for the added class or null pointer on error.
 */
gsf_type* gsf_itm_add_class(gsf_itm* self, const gsf_class* class);
/**
 * @brief Adds an enum to the type manager and prepares it for type usage.
 * @param self Type manager.
 * @param enum_ The added enum.
 * @return Type specification for the added enum or null pointer on error.
 */
gsf_type* gsf_itm_add_enum(gsf_itm* self, const gsf_enum* enum_);

/**
 * @brief Gets a type in this manager that is adequate to one from any manager.
 * @param self Target type manager.
 * @param type The type from any/unknown manager.
 * @return Casted type or null pointer on error.
 */
const gsf_type* gsf_itm_cast_type(gsf_itm* self, const gsf_type* type);
/**
 * @brief Gets a type from another type or from a string representation.
 * @param self Type manager.
 * @param spec Any type (from any manager) or its null-terminated string representation.
 * @return The type or null pointer on error.
 */
const gsf_type* gsf_itm_get_type(gsf_itm* self, const gsf_type_or_char* spec);
/**
 * @brief Gets a typing in this manager that is adequate to one from any manager.
 * @param self Target type manager.
 * @param typing The typing from any/unknown manager.
 * @return Casted type or null pointer on error.
 */
const gsf_typing* gsf_itm_cast_typing(gsf_itm* self, const gsf_typing* typing);
/**
 * @brief Gets a typing from another typing or from a string representation.
 * @param self Type manager.
 * @param spec Any typing (from any manager) or its null-terminated string representation.
 * @return The typing or null pointer on error.
 */
const gsf_typing* gsf_itm_get_typing(gsf_itm* self, const gsf_typing_or_char* spec);

/**
 * @brief Gets a type from a textual representation.
 * @param self Type manager.
 * @param buf Character array containing the textual representation.
 * @param buf_size Size of buf.
 * @return The represented type or null pointer on error.
 */
const gsf_type* gsf_itm_str_buf_to_type(gsf_itm* self, const char* buf, size_t buf_size);
/**
 * @brief Gets a type from a textual representation.
 * @param self Type manager.
 * @param string Null-terminated string containing the textual representation.
 * @return The represented type or null pointer on error.
 */
const gsf_type* gsf_itm_str_to_type(gsf_itm* self, const char* string);
/**
 * @brief Gets a typing from a textual representation.
 * @param self Type manager.
 * @param buf Character array containing the textual representation.
 * @param buf_size Size of buf.
 * @return The represented typing or null pointer on error.
 */
const gsf_typing* gsf_itm_str_buf_to_typing(gsf_itm* self, const char* buf, size_t buf_size);
/**
 * @brief Gets a typing from a textual representation.
 * @param self Type manager.
 * @param string Null-terminated string containing the textual representation.
 * @return The represented typing or null pointer on error.
 */
const gsf_typing* gsf_itm_str_to_typing(gsf_itm* self, const char* string);

/**
 * @brief Gets a (not null-terminated) textual representation of a type.
 * @param self Type manager.
 * @param type The type (will be casted/verified; @see gsf_itm_cast_type).
 * @param buf Character array to be filled with the representation.
 * @param buf_size Max size. If no limit is needed, copy from @see gsf_itm_type_to_str.
 * @return End of the written representation or null pointer on error.
 */
char* gsf_itm_type_to_str_buf(gsf_itm* self, const gsf_type* type, char* buf, size_t buf_size);
/**
 * @brief Gets a textual representation of a type.
 * @param self Type manager.
 * @param type The type (will be casted/verified; @see gsf_itm_cast_type).
 * @return Manager-owned string that contains the textual representation.
 */
const char* gsf_itm_type_to_str(gsf_itm* self, const gsf_type* type);
/**
 * @brief Gets a (not null-terminated) textual representation of a type.
 * @param self Type manager.
 * @param type The type (will be casted/verified; @see gsf_itm_cast_type).
 * @param buf Character array to be filled with the representation.
 * @param buf_size Max size. If no limit is needed, copy from @see gsf_itm_type_to_str.
 * @return End of the written representation or null pointer on error.
 */
char* gsf_itm_typing_to_str_buf(gsf_itm* self, const gsf_typing* typing, char* buf, size_t buf_size);
/**
 * @brief Gets a textual representation of a type.
 * @param self Type manager.
 * @param type The type (will be casted/verified; @see gsf_itm_cast_type).
 * @return Manager-owned string that contains the textual representation.
 */
const char* gsf_itm_typing_to_str(gsf_itm* self, const gsf_typing* typing);

const gsf_typing* gsf_itm_typing_any(gsf_itm* self);
const gsf_typing* gsf_itm_typing_make(gsf_itm* self, const gsf_type_or_char* type);
const gsf_type* gsf_itm_type_make_array(gsf_itm* self, const gsf_type_or_char* elems);
const gsf_type* gsf_itm_type_make_list(gsf_itm* self, const gsf_typing_or_char* elems);
const gsf_type* gsf_itm_type_make_map(gsf_itm* self, const gsf_typing_or_char* elems);

#ifdef __cplusplus
#undef class
}
#endif
