#pragma once

#include "gsf_com.h"

#ifdef __cplusplus
#define class class_
extern "C" {
#endif

typedef void gsf_typing_or_char;

typedef struct gsf_class gsf_class;
typedef struct gsf_enum gsf_enum;

gsf_context* gsf_context_new(void);
gsf_context* gsf_context_init(gsf_context* context);
void gsf_context_destroy(gsf_context* context);

gsf_schema* gsf_context_new_schema(gsf_context* self, const gsf_descriptor* desc,
		const gsf_typing_or_char* root, size_t class_amount);
gsf_schema* gsf_context_new_schema_name(gsf_context* self, const char* desc_name,
		const gsf_typing_or_char* root, size_t class_amount);
size_t gsf_context_schema_count(const gsf_context* self);
gsf_schema* gsf_context_get_schema_by_idx(const gsf_context* self, size_t index);
#define gsf_context_get_schema_by_idx(S, I) _Generic(								\
	1 ? (S) : (gsf_context*) 0,														\
	const gsf_context*: (const gsf_schema*) gsf_context_get_schema_by_idx(S, I),	\
	default: gsf_context_get_schema_by_idx(S, I)									\
)
gsf_schema* gsf_context_get_schema(const gsf_context* self, const gsf_descriptor* desc);
#define gsf_context_get_schema(S, D) _Generic(								\
	1 ? (S) : (gsf_context*) 0,												\
	const gsf_context*: (const gsf_schema*) gsf_context_get_schema(S, D),	\
	default: gsf_context_get_schema(S, D)									\
)


#ifdef __cplusplus
#undef class
} // extern "C"
#endif
