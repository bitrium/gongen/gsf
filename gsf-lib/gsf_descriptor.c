#include "gsf_data.h"

#include <string.h>
#include <ctype.h>
#include "gsf_err.h"

#include "gsf_util.c"


struct gsf_descriptor {
	char* full;
	char* group;
	char* name;
	char* version;
};

// TODO only allow alphanumeric characters, '_', '-' and dots!

gsf_descriptor* gsf_create_descriptor(void) {
	gsf_descriptor* descriptor = gsf_alloc(sizeof(gsf_descriptor));
	if (!descriptor) return 0;
	descriptor->name = 0;
	return descriptor;
}
void gsf_delete_descriptor(gsf_descriptor* descriptor) {
	if (!descriptor) return;
	if (descriptor->name) gsf_descriptor_destroy(descriptor);
	gsf_free(descriptor);
}

gsf_descriptor* gsf_descriptor_init(gsf_descriptor* self, const char* group,
									const char* name, const char* version) {
	if (!self) GSF_RAISE(gsf_enullptr);
	self->full = 0;
	self->group = 0;
	self->name = 0;
	self->version = 0;
	if (!gsf_descriptor_edit(self, group, name, version)) return 0;
	return self;
}
gsf_descriptor* gsf_descriptor_new(const char* group, const char* name,
								   const char* version) {
	gsf_descriptor* self = gsf_create_descriptor();
	if (self && !gsf_descriptor_init(self, group, name, version)) { 
		gsf_delete_descriptor(self);
		return 0;
	}
	return self;
}
void gsf_descriptor_destroy(gsf_descriptor* self) {
	if (!self) return;
	gsf_free(self->full);
	gsf_free(self->group);
	gsf_free(self->name);
	gsf_free(self->version);
	self->name = 0;
}

int gsf_descriptor_from_str_buf(gsf_descriptor* self, const char* buf, size_t size) {
	const char* ptr = buf;
	self->group = 0;
	self->name = 0;
	self->version = 0;

	while (isspace(*ptr)) ptr++;
	while (isspace(buf[size - 1])) size--;

	size_t len = size - (ptr - buf);
	self->full = gsf_alloc(len + 1);
	if (!self->full) GSF_RAISE(gsf_emem);
	memcpy(self->full, ptr, len);
	self->full[len] = '\0';

	const char* start = ptr;
	while (ptr - buf < size) {
		if (*ptr == ':') {
			len = ptr - start;
			char* str = gsf_alloc(len + 1);
			if (!str) GSF_RAISE(gsf_emem);
			if (!self->name) {
				self->name = str;
			} else if (!self->version) {
				self->version = str;
			} else GSF_RAISE(gsf_ebadname, "descriptor must have no more than two colons (':')");
			memcpy(str, start, len);
			str[len] = '\0';
			start = ptr + 1;
		}
		ptr++;
	}
	len = ptr - start;
	char* str = gsf_alloc(len + 1);
	if (!str) GSF_RAISE(gsf_emem);
	int ret;
	if (!self->name) {
		self->name = str;
		ret = 1;
	} else if (!self->version) {
		self->version = str;
		ret = 2;
	} else {
		self->group = self->name;
		self->name = self->version;
		self->version = str;
		ret = 3;
	}
	memcpy(str, start, len);
	str[len] = '\0';

	return ret;
}
int gsf_descriptor_from_str(gsf_descriptor* self, const char* string) {
	return gsf_descriptor_from_str_buf(self, string, strlen(string));
}
char* gsf_descriptor_to_str_buf(const gsf_descriptor* self, char* buf, size_t size) {
	size_t len = strlen(self->full);
	if (size < len) GSF_RAISE(gsf_eshortbuf);
	memcpy(buf, self->full, len);
	return buf + len;
}
const char* gsf_descriptor_to_str(const gsf_descriptor* self) {
	return self->full;
}

gsf_descriptor* gsf_descriptor_edit(gsf_descriptor* self, const char* group,
									const char* name, const char* version) {
	size_t group_len = group ? strlen(group) : 0;
	size_t name_len = name ? strlen(name) : 0;
	size_t version_len = version ? strlen(version) : 0;
	char* new_group = 0;
	char* new_name = 0;
	char* new_version = 0;
	if ( // reallocations will only be performed when needed thanks to short-circuit eval
		group && !(new_group = gsf_realloc(self->group, group_len + 1)) ||
		name && !(new_name = gsf_realloc(self->name, name_len + 1)) ||
		version && !(new_version = gsf_realloc(self->version, version_len + 1))
	) GSF_RAISE(gsf_emem);
	if (group) strcpy(new_group, group);
	if (name) strcpy(new_name, name);
	if (version) strcpy(new_version, version);
	self->group = new_group;
	self->name = new_name;
	self->version = new_version;

	// FIXME at this point it is already changed and errors won't revert the edit
	if (!self->name) GSF_RAISE(gsf_ebadname, "all descriptors must specify a name");
	if (self->group && !self->version) GSF_RAISE(gsf_ebadname, "descriptors with groups must also specify a version");
	group_len = self->group ? strlen(self->group) : 0;
	name_len = strlen(self->name);
	version_len = self->version ? strlen(self->version) : 0;
	size_t full_len = group_len + name_len + version_len + (self->group ? 1 : 0) + (self->version ? 1 : 0);
	char* new_full = 0;
	if (!(new_full = gsf_realloc(self->full, full_len + 1))) GSF_RAISE(gsf_emem);
	self->full = new_full;
	char* ptr = self->full;
	if (self->group) {
		memcpy(ptr, self->group, group_len);
		ptr += group_len;
		*ptr++ = ':';
	}
	memcpy(ptr, self->name, name_len);
	ptr += name_len;
	if (self->version) {
		*ptr++ = ':';
		memcpy(ptr, self->version, version_len);
		ptr += version_len;
	}
	GSF_ASSERT(ptr == self->full + full_len);
	*ptr = '\0';

	return self;
}

const char* gsf_descriptor_get_group(const gsf_descriptor* self) {
	return self->group;
}
const char* gsf_descriptor_get_name(const gsf_descriptor* self) {
	return self->name;
}
const char* gsf_descriptor_get_version(const gsf_descriptor* self) {
	return self->version;
}

int gsf_descriptor_match(const gsf_descriptor* self, const gsf_descriptor* preset) {
	if (self == preset) return 1;
	if (self->group != preset->group && strcmp(self->group, preset->group) != 0) return 0;
	if (self->name != preset->name && strcmp(self->name, preset->name) != 0) return 0;
	if (self->version != preset->version && strcmp(self->version, preset->version) != 0) return 0;
	return 1;
}
