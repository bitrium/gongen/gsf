#include "gsf_codec.h"

#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include "gsf_data.h"
#include "gsf_context.h"
#include "gsf_types.h"
#include "gsf_util.c"

extern void* gsf_value_encode_any(gsf_value* holder, const gsf_type* type);

static inline void buf_next_no_comms(const char** ptr) {
	if (**ptr == '\r') *ptr += 1;
	*ptr += 1;
}
static inline void buf_next_comms(const char** ptr) {
	if (**ptr == '#') {
		// skip to line end
		while (**ptr != '\n' && **ptr != '\0') {
			*ptr += 1;
		}
		*ptr += 1;
	} else {
		if (**ptr == '\r') ptr += 1;
		*ptr += 1;
	}
}

static inline bool buf_is_le_no_comms(const char* ptr) {
	return *ptr == '\n' || *ptr == '\r' || *ptr == '\0';
}
static inline bool buf_is_le_comms(const char* ptr) {
	return *ptr == '\n' || *ptr == '\r' || *ptr == '\0' || *ptr == '#';
}

static inline const char* buf_le_no_comms(const char* ptr) {
	while (!buf_is_le_no_comms(ptr)) ptr++;
	return ptr;
}
static inline const char* buf_le_comms(const char* ptr) {
	while (!buf_is_le_comms(ptr)) ptr++;
	return ptr;
}

#define IS_END(ptr, buf, buf_size) (ptr - buf >= buf_size || *ptr == '\0')

struct buf_set {
	void (*next)(const char**);
	bool (*is_le)(const char*); // is line end; if false, ptr++ can be used
	const char* (*le)(const char*); // get line end
};
struct buf_set make_buf_set(int flags) {
	struct buf_set set;
	set.next = flags & GSF_TXT_READ_NO_COMMENTS ? buf_next_no_comms : buf_next_comms;
	set.le = flags & GSF_TXT_READ_NO_COMMENTS ? buf_le_no_comms : buf_le_comms;
	set.is_le = flags & GSF_TXT_READ_NO_COMMENTS ? buf_is_le_no_comms : buf_is_le_comms;
	return set;
}

int gsf_txt_article_read_proper(struct gsf_reader* reader, gsf_article* article,
		const gsf_schema* schema);
int gsf_txt_value_read(struct gsf_reader* reader, gsf_value* value, const gsf_type* type);

int gsf_txt_schema_read(struct gsf_reader* reader, gsf_schema* schema, gsf_descriptor* descriptor) {
	gsf_schema* master_schema = gsf_get_master_schema();
	if (!master_schema) GSF_RAISE(gsf_emem);

	int article_flags = (reader->flags & GSF_TXT_READ_BYTES) | GSF_TXT_READ_ARTICLE_STRING_BLEND;
	gsf_article* schema_article = gsf_create_article();
	if (!gsf_txt_article_read_proper(reader, schema_article, master_schema)) {
		if (gsf_errno == gsf_etxtart) gsf_errno = gsf_etxtsch;
		return 0;
	}

	const gsf_object* root = gsf_value_decode_object(gsf_article_access_root(schema_article));

	if (descriptor) {
		const char* group_str = gsf_value_decode_str(gsf_object_access_at_key(root, "group"));
		const char* name_str = gsf_value_decode_str(gsf_object_access_at_key(root, "name"));
		const char* version_str = gsf_value_decode_str(gsf_object_access_at_key(root, "version"));
		gsf_descriptor_init(descriptor, group_str, name_str, version_str);
	}

	// flags for default value reading
	// comments are already cropped in article read, so no more handling is needed
	article_flags = reader->flags & GSF_TXT_READ_NO_COMMENTS;
	const char* root_str
		= gsf_value_decode_str(gsf_object_access_at_key(root, "root"));
	const gsf_map* enums = gsf_value_decode_map(gsf_object_access_at_key(root, "enums"));
	size_t enum_count = gsf_map_element_count(enums);
	const gsf_map* classes = gsf_value_decode_map(gsf_object_access_at_key(root, "classes"));
	size_t class_count = gsf_map_element_count(classes);
	if (!gsf_schema_init(schema, descriptor, root_str, class_count)) return 0;

	for (size_t i = 0; i < enum_count; i++) {
		const gsf_list* variants = gsf_value_decode_list(gsf_map_get_value(enums, i));
		size_t variant_count = gsf_list_get_size(variants);
		gsf_enum* enum_ = gsf_schema_add_enum(schema, gsf_map_get_key(enums, i), variant_count);
		for (size_t j = 0; j < variant_count; j++) {
			const char* name = gsf_value_decode_str(gsf_list_get_element(variants, j));
			char* name_buf = 0;
			const char* doc = 0;
			if ((doc = strchr(name, '/'))) {
				const char* name_end = doc;
				while (name_end > name && isblank(*(name_end - 1))) name_end--;
				name_buf = gsf_alloc(doc - name_end + 1);
				if (!name_buf) GSF_RAISE(gsf_emem);
				memcpy(name_buf, name, name_end - name);
				name_buf[name_end - name] = '\0';
				name = name_buf;
				doc++;
				if (*doc == ' ') doc++;
			}
			gsf_variant* variant = gsf_enum_add_variant(enum_, name);
			if (doc) gsf_variant_set_doc(variant, doc);
			gsf_free(name_buf);
		}
	}

	for (size_t i = 0; i < class_count; i++) {
		const gsf_map* labels
			= gsf_value_decode_map(gsf_map_get_value(classes, i));
		size_t label_count = gsf_map_element_count(labels);
		const char* class_name = gsf_map_get_key(classes, i);
		gsf_class* class = gsf_schema_add_class(schema, class_name, label_count);
		for (size_t j = 0; j < label_count; j++) {
			const char* label_key = gsf_map_get_key(labels, j);
			const char* buf = gsf_value_decode_str(gsf_map_get_value(labels, j));
			char ending_char = '\n';
			char* end = strchr(buf, ending_char);
			char* full_end = strchr(buf, '\0');
			if (!end) {
				ending_char = '\0';
				end = full_end;
			}
			const char* formatted_end = end; // end of formatted stuff
			const char* doc_start = 0;
			const char* finding_doc_start = buf;
			while (!doc_start) {
				doc_start = memchr(finding_doc_start, '/', end - finding_doc_start);
				if (!doc_start) break;
				finding_doc_start = doc_start + 1;
				if (!isblank(*(doc_start - 1))) {
					doc_start = 0;
					if (*finding_doc_start == '\0') break;
				}
			}
			if (doc_start) {
				formatted_end = doc_start;
				doc_start++;
			}
			const char* typing_end = formatted_end;
			const char* default_val_type_start = memchr(buf, '.', formatted_end - buf);
			const char* default_val_start;
			if (default_val_type_start) {
				typing_end = default_val_type_start;
				default_val_type_start++;
				default_val_start = memchr(typing_end, '=', formatted_end - typing_end);
				if (!default_val_start) GSF_RAISE(gsf_etxtsch);
			} else {
				default_val_start = memchr(buf, '=', formatted_end - buf);
				if (default_val_start) {
					typing_end = default_val_start;
				}
			}
			size_t len = typing_end - buf;
			char* typing_spec = gsf_alloc(len + 1);
			memcpy(typing_spec, buf, len);
			typing_spec[len] = '\0';
			char* default_val_type = 0;
			if (default_val_type_start) {
				size_t len = default_val_start - default_val_type_start;
				default_val_type = gsf_alloc(len + 1);
				memcpy(default_val_type, default_val_type_start, len);
				default_val_type[len] = '\0';
			}
			const char* default_val_buf = 0;
			size_t default_val_buf_len = 0;
			if (default_val_start) {
				default_val_start++;
				if (*default_val_start == ' ') default_val_start++;
				default_val_buf_len = formatted_end - default_val_start;
				if (default_val_buf_len > 0) {
					// cannot be multi-line if a value is defined here
					if (ending_char == '\n') GSF_RAISE(gsf_etxtsch);
					default_val_buf = default_val_start;
					default_val_buf_len = default_val_buf_len;
				}
			}
			char* doc = 0;
			if (doc_start) {
				while (isblank(*doc_start)) doc_start++;
				size_t len = end - doc_start;
				doc = gsf_alloc(len + 1);
				memcpy(doc, doc_start, len);
				doc[len] = '\0';
			}
			if (ending_char == '\n') {
				default_val_start = end + 1;
				default_val_buf = default_val_start;
				default_val_buf_len = full_end - default_val_start;
			}
			if (default_val_buf) {
				while (*(default_val_buf + default_val_buf_len - 1) == ' ') {
					default_val_buf_len--;
				}
			}

			gsf_label* label = gsf_class_add_label(class, label_key, typing_spec);
			if (label && doc && !gsf_label_set_doc(label, doc)) label = 0;
			gsf_free(typing_spec);
			gsf_free(doc);
			if (!label) return 0;
			if (default_val_start) {
				if (!default_val_buf) {
					default_val_buf = "";
				}
				gsf_value* default_val = gsf_label_access_default(label);
				const gsf_type* selected_type = 0;
				if (default_val_type) {
					int result = gsf_typing_match_type(gsf_value_get_typing(default_val), default_val_type, &selected_type);
					if (result < 0) return 0;
					if (!result) GSF_RAISEF(
						gsf_etxtsch,
						"selected type of default value of label '%s' of class '%s' is not allowed",
						label_key, class_name
					);
				} else {
					selected_type = gsf_typing_get_default(gsf_label_get_typing(label));
					if (!selected_type) GSF_RAISEF(
						gsf_etxtsch,
						"no default type for label '%s' of class '%s' (chose a type for the default value)",
						label_key, class_name
					);
				}
				char* default_val_buf_copy = 0;
				if (ending_char == '\0' && !doc) default_val_buf_len += 1;
				// if (ending_char == '\0' && !doc) {
				// 	article_flags |= GSF_TXT_READ_STRING;
				// } else {
				// 	article_flags &= ~GSF_TXT_READ_STRING;
					if (doc) {
						// make it end with a line end
						default_val_buf_copy = gsf_alloc(default_val_buf_len + 1);
						memcpy(default_val_buf_copy, default_val_buf, default_val_buf_len);
						default_val_buf_copy[default_val_buf_len++] = '\n';
						default_val_buf = default_val_buf_copy;
					}
				// }
				struct gsf_reader default_val_reader = {0};
				default_val_reader.flags = article_flags;
				gsf_reader_create_buf(&default_val_reader, default_val_buf, default_val_buf_len);
				int result = gsf_txt_value_read(&default_val_reader, default_val, selected_type);
				gsf_reader_delete_buf(&default_val_reader);
				if (!result) {
					if (gsf_errno == gsf_etxtart) {
						gsf_errno = gsf_etxtsch;
					}
					return 0;
				}
				gsf_free(default_val_buf_copy);
				gsf_free(default_val_type);
			}
		}
	}

	gsf_delete_article(schema_article);
	return 1;
}

int gsf_txt_article_read_desc(struct gsf_reader* reader, gsf_descriptor* descriptor) {
	struct buf_set set = make_buf_set(reader->flags);

	reader->read(reader, SIZE_MAX);

	const char* buf = reader->ptr;
	const size_t buf_size = reader->end - reader->ptr;
	const char* ptr = buf;

	if (buf_size == 0) GSF_RAISE(gsf_etxtart, "textual articles must end with a line ending");
	ptr = buf + buf_size - 1;
	if (*ptr != '\0') {
		while (isblank(*ptr) && ptr != buf) ptr--;
		if (*ptr != '\n') GSF_RAISE(gsf_etxtart, "textual articles must end with a line ending");
	}
	ptr = buf;

	while (isspace(*ptr)) {
		if (IS_END(ptr, buf, buf_size)) {
			ptr--;
			break;
		}
		set.next(&ptr);
	}

	if (*ptr != '@') {
		// schemaless
		if (descriptor && !gsf_descriptor_init(descriptor, "", "", "")) return 0;
		return 1;
	}
	ptr++;
	const char* start = ptr;
	while (!set.is_le(ptr)) {
		set.next(&ptr);
	}
	if (descriptor && !gsf_descriptor_from_str_buf(descriptor, start, ptr - start)) return 0;

	set.next(&ptr);
	reader->ptr = ptr;
	return 1;
}


#define GSF_TOKENS_STEP 64
#if __STDC_VERSION__ >= 202300L
enum token_type : uint8_t {
#else
enum token_type {
#endif
	token_indentation,	// int; at the beginning of every line
	token_key,			// str; field key
	token_type_spec,	// str; field type spec
	token_string,		// str; a string literal
	token_integer,		// int; an integer scalar literal
	token_negative,		// int; a negative integer scalar literal
	token_float,		// float; a floating-point scalar literal
	token_special,		// int; sepcial symbol or code (value meanings below)
// 	token_special (-1):	unexpected/error (or part of a multi-line literal)
// 	token_special (1):	key-value separator (:)
//	token_special (2):	array member delimeter (;)
};
struct token {
#if __STDC_VERSION__ >= 202300L
	enum token_type type;
#else
	uint8_t type; // enum token_type (limit size to 1 byte)
#endif
	size_t offset;
	union {
		size_t data_str;
		uint64_t data_int;
		double data_float;
	};
};

static bool reserve_tokens(size_t count, struct token** array, struct token** current) {
	size_t tokens_left = GSF_TOKENS_STEP - ((*current - *array) % GSF_TOKENS_STEP);
	if (tokens_left < count) {
		struct token* new_tokens = gsf_realloc(*array, (*current - *array + tokens_left - 1 + GSF_TOKENS_STEP) * sizeof(struct token));
		if (!new_tokens) {
			gsf_free(*array);
			GSF_RAISE(gsf_emem);
		}
		*current = new_tokens + (*current - *array);
		*array = new_tokens;
	}
	return true;
}

static const char* tokenize(struct gsf_reader* reader, struct token** tokens_ptr, size_t* count_ptr, size_t* max_depth) {
	struct buf_set set = make_buf_set(reader->flags);
	reader->read(reader, SIZE_MAX);
	size_t buf_size = reader->end - reader->ptr;
	const char* buf = reader->ptr;
	const char* ptr = buf;

	if (buf_size == 0) GSF_RAISE(gsf_etxtart, "textual articles must end with a line ending");
	ptr = buf + buf_size - 1;
	if (*ptr != '\0') {
		while (isblank(*ptr) && ptr != buf) ptr--;
		if (*ptr != '\n') {
			GSF_RAISE(gsf_etxtart, "textual articles must end with a line ending");
		}
	}
	ptr = buf;

	const char* indent_example = 0;
	if ((reader->flags & GSF_TXT_READ_AUTO_INDENTS) == GSF_TXT_READ_AUTO_INDENTS) {
		indent_example = "";
	} else if (reader->flags & GSF_TXT_READ_FOUR_SPACE_INDENTS) {
		indent_example = "    ";
	} else if (reader->flags & GSF_TXT_READ_TAB_INDENTS) {
		indent_example = "\t";
	} else {
		indent_example = "  ";
	}
	int indent_example_len = strlen(indent_example);
	char* indent_example_auto = 0;

	struct token* tokens = gsf_alloc(GSF_TOKENS_STEP * sizeof(struct token));
	if (!tokens) GSF_RAISE(gsf_emem);
	struct token* token = tokens;
	*max_depth = 0;

	double real_scale = 0; // for floats

	while (!IS_END(ptr, buf, buf_size)) {
		// if (set.is_le(ptr)) goto next_line; (disabled because of string reconstruction)

		size_t tokens_left = GSF_TOKENS_STEP - ((token - tokens) % GSF_TOKENS_STEP);
		if (!reserve_tokens(6, &tokens, &token)) return 0;

		if (indent_example_len == 0 && isblank(*ptr)) {
			// detect indentation (first indented line should be singly-indented)
			const char* end = ptr;
			while (isblank(*end)) {
				end++;
			}
			indent_example_auto = gsf_alloc(end - ptr);
			if (!indent_example_auto) {
				gsf_free(tokens);
				GSF_RAISE(gsf_emem);
			}
			strncpy(indent_example_auto, ptr, end - ptr);
			indent_example = indent_example_auto;
			indent_example_len = strlen(indent_example);
		}

		// indentation
		token->type = token_indentation;
		token->offset = ptr - buf;
		token->data_int = 0;
		int comp_position = 0;
		while (!set.is_le(ptr)) {
			if (*ptr != indent_example[comp_position]) {
				// end of indent
				ptr -= comp_position; // revert started indent
				break;
			}
			if (++comp_position >= indent_example_len) {
				token->data_int++;
				comp_position = 0;
			}
			ptr++;
		}
		if (token->data_int > *max_depth) *max_depth = token->data_int;
		token++; // always add the indentation token
		if (set.is_le(ptr)) goto next_line;

		// field key
		token->type = token_key;
		token->offset = ptr - buf;
		while (!set.is_le(ptr) && *ptr != '.' && *ptr != ':') {
			ptr++;
		}
		token->data_str = ptr - buf;
		if (token->data_str != token->offset && !set.is_le(ptr)) {
			token++; // key is not empty – add the token
			token->offset = (token - 1)->offset; // for reanalyzing
		}

		if (*ptr == '.') {
			// field type
			ptr++;
			if (isdigit(*ptr)) { // types can't start with a number
				// reanalyze as literal
				ptr = buf + token->offset;
				goto literal;
			}
			token->type = token_type_spec;
			token->offset = ptr - buf;
			while (!set.is_le(ptr) && *ptr != ':') {
				ptr++;
			}
			token->data_str = ptr - buf;
			token++; // add the token
		} else if (set.is_le(ptr)) {
			// reanalyze as literal
			ptr = buf + token->offset;
			goto literal;
		}

		// key-value separator
		if (*ptr != ':') {
			// unexpected
			token->type = token_special;
			token->offset = ptr - buf;
			token->data_int = -1;
			token++; // add the token
			ptr = set.le(ptr);
			goto next_line;
		}
		token->type = token_special;
		token->offset = ptr - buf;
		token->data_int = 1;
		token++;
		ptr++; // skip ':' separator
		if (isblank(*ptr)) ptr++; // skip first space
		if (set.is_le(ptr)) goto next_line;

		literal:
		token->offset = ptr - buf;

		// scalar literals
		while (isblank(*ptr)) ptr++;
		token->type = token_integer; // start as integer
		token->data_int = 0;
		if (*ptr == '-') {
			token->type = token_negative;
			ptr++;
		}
		while (isdigit(*ptr)) {
			token->data_int *= 10;
			token->data_int += *ptr - '0';
			ptr++;
		}
		if (token->offset != ptr - buf) {
			// possible scalar (starts with digits)
			while (!set.is_le(ptr)) {
				if (*ptr == ';') {
					// add previous scalar token
					token++;

					if (!reserve_tokens(2, &tokens, &token)) return 0;
					// array member delimeter
					token->type = token_special;
					token->offset = ptr - buf;
					token->data_int = 2;
					ptr++;
					while (isblank(*ptr)) ptr++;
					if (set.is_le(ptr)) continue; // don't add another scalar if empty
					token++;

					// another possible scalar
					token->type = token_integer;
					token->offset = ptr - buf;
					token->data_int = 0;
					if (*ptr == '-') {
						token->type = token_negative;
						ptr++;
					}
					continue;
				} else if (*ptr == '.') {
					// float
					if (token->type == token_float) goto string;
					token->data_float = token->data_int;
					if (token->type == token_negative) token->data_float *= -1;
					token->type = token_float;
					real_scale = 0.1;
				} else if (isdigit(*ptr) /*&& *ptr != '\'' && *ptr != ','*/) {
					if (token->type != token_float) {
						token->data_int *= 10;
						token->data_int += *ptr - '0';
					} else {
						// FIXME this algorithm is inaccurate (does not even truncate before adding)
						token->data_float += (*ptr - '0') * real_scale;
						real_scale /= 10;
					}
				} else if (!isblank(*ptr)) {
					goto string;
				}
				ptr++;
			}
			// if not empty, add the scalar token (or last of the array)
			if (buf - ptr > token->offset) token++;
			goto next_line;
		}

		string:
		// string literal
		token->type = token_string;
		if (!set.is_le(ptr)) {
			ptr = set.le(ptr);
			if (*(ptr - 1) == ' ') ptr--; // remove trailing space (mainly for comments)
		}
		token->data_str = ptr - buf;
		token++;
		if (*ptr == ' ') ptr++; // skip previously removed trailing space

		next_line:
		if (*ptr != '\0') set.next(&ptr);
	}
	gsf_free(indent_example_auto);

	// additional indentation (for easier parsing)
	if (!reserve_tokens(1, &tokens, &token)) return 0;
	token->type = token_indentation;
	token->offset = ptr - buf;
	token->data_int = 0;
	token++;

	*tokens_ptr = tokens;
	*count_ptr = token - tokens;
	reader->ptr = ptr;
	return buf;
}

// returns error message
static inline const char* token_to_scalar(void* dst, struct token* token, const gsf_type* type) {
	uint8_t width = gsf_type_scalar_width(type);
	if (gsf_type_is_int(type)) {
		bool is_negative = token->type == token_negative;
		if (token->type != token_integer && !is_negative) {
			return "expected a signed integer literal";
		}
		if (width == 1) { // i8
			if (token->data_int > INT8_MAX + is_negative) {
				return "i8 literal exceeds its limits";
			}
			*((int8_t*) dst) = token->data_int;
			if (is_negative) *((int8_t*) dst) *= -1;
		} else if (width == 2) { // i16
			if (token->data_int > INT16_MAX + is_negative) {
				return "i16 literal exceeds its limits";
			}
			*((int16_t*) dst) = token->data_int;
			if (is_negative) *((int16_t*) dst) *= -1;
		} else if (width == 4) { // i32
			if (token->data_int > INT32_MAX + is_negative) {
				return "i32 literal exceeds its limits";
			}
			*((int32_t*) dst) = token->data_int;
			if (is_negative) *((int32_t*) dst) *= -1;
		} else if (width == 8) { // i64
			if (token->data_int > INT64_MAX + is_negative) {
				return "i64 literal exceeds its limits";
			}
			*((int64_t*) dst) = token->data_int;
			if (is_negative) *((int64_t*) dst) *= -1;
		}
	} else if (gsf_type_is_uint(type)) {
		if (token->type != token_integer) {
			return "expected an unsigned integer literal";
		}
		if (width == 1) { // u8
			if (token->data_int > UINT8_MAX) {
				return "u8 literal exceeds its limits";
			}
			*((uint8_t*) dst) = token->data_int;
		} else if (width == 2) { // u16
			if (token->data_int > UINT16_MAX) {
				return "u16 literal exceeds its limits";
			}
			*((uint16_t*) dst) = token->data_int;
		} else if (width == 4) { // u32
			if (token->data_int > UINT32_MAX) {
				return "u32 literal exceeds its limits";
			}
			*((uint32_t*) dst) = token->data_int;
		} else if (width == 8) { // u64
			*((uint64_t*) dst) = token->data_int;
		}
	} else if (gsf_type_is_float(type)) {
		if (token->type == token_integer || token->type == token_negative) {
			token->type = token_float;
			token->data_float = token->data_int;
			if (token->type == token_negative) token->data_float *= -1;
		}
		if (token->type != token_float) {
			return "expected a floating-point literal";
		}
		if (width == 4) {
			*((float*) dst) = token->data_float;
		} else if (width == 8) {
			*((double*) dst) = token->data_float;
		}
	}
	return 0;
}

static inline char* buf_seg(const char* buf, size_t buf_size, size_t pos, int flags) {
	if (pos > buf_size) {
		return 0;
	}
	const char* start = buf + pos;
	while (start != buf && *(start - 1) != '\n' && pos - (start - buf) < 32) {
		start--;
	}
	// TODO include previous line?
	const char* end = buf + pos;
	while (!IS_END(end, buf, buf_size) && *end != '\n' && (end - buf) - pos < 32) {
		end++;
	}
	char* segment = gsf_alloc((end - start) + (pos - (start - buf)) + 3);
	if (!segment) return 0;
	memcpy(segment, start, end - start);
	segment[end - start] = '\n';
	memset(segment + (end - start) + 1, '~', pos - (start - buf));
	segment[(end - start) + (pos - (start - buf)) + 1] = '^';
	segment[(end - start) + (pos - (start - buf)) + 2] = '\0';
	return segment;
}

int gsf_txt_article_read_proper(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema) {
	if (!gsf_article_init(article, schema)) {
		return 0;
	}
	// TODO type
	return gsf_txt_value_read(reader, gsf_article_access_root(article), 0);
}
int gsf_txt_value_read(struct gsf_reader* reader, gsf_value* value, const gsf_type* type) {
	struct token* tokens = 0;
	size_t token_count = 0;
	size_t max_depth = 0;
	const char* buf = tokenize(reader, &tokens, &token_count, &max_depth);
	if (!buf) return 0;
	size_t buf_size = (const char*) reader->ptr - buf;

	// analyze tokens
	max_depth++; // the value stack contains root as 0th depth
	struct {
		gsf_value* val;
		union {
			gsf_array* array;
			gsf_list* list;
			gsf_map* map;
			gsf_object* object;
		} casted;
		const gsf_type* type;
	} values[max_depth + 1];
	size_t depth = 0;
	struct token* token = tokens;
	GSF_ASSERT(token - tokens < token_count);

	const char* err_buf = 0;
	size_t temp_string_len = 0;
	char* temp_string = 0;
#define GSF_PARSE_CLEANUP()		\
	do {						\
		gsf_free(temp_string);	\
		gsf_free(tokens);		\
		gsf_value_unset(value);	\
	} while (0)
#define GSF_PARSE_RAISE(msg, pos)							\
	do {													\
		depth = pos; 										\
		GSF_PARSE_CLEANUP();								\
		temp_string = buf_seg(buf, buf_size, depth, reader->flags);	\
		if (temp_string) {									\
			GSF_RAISEF(										\
				gsf_etxtart,								\
				"%s\ntextual buffer fragment:\n%s\n",		\
				msg, temp_string							\
			);												\
		} else {											\
			GSF_RAISEF(										\
				gsf_etxtart,								\
				"%s\n(at the end of the buffer)\n", msg		\
			);												\
		}													\
		gsf_free(temp_string);								\
	} while (0)
#define GSF_TEMP_STR(len)									\
	do {													\
		if (temp_string_len < len) {						\
			char* str = gsf_realloc(temp_string, len + 1);	\
			if (!str) {										\
				GSF_PARSE_CLEANUP();						\
				return 0;									\
			}												\
			temp_string = str;								\
			temp_string_len = len;							\
		}													\
		temp_string[len] = '\0';							\
	} while (0)
#define GSF_TOKEN_STR(token)								\
	do {													\
		size_t len = token->data_str - token->offset;		\
		GSF_TEMP_STR(len);									\
		memcpy(temp_string, buf + token->offset, len);		\
	} while (0)

	struct token* key_token = 0;
	struct token* type_token = 0;
	values[0].val = value;
	const gsf_typing* typing = gsf_value_get_typing(value);

	// remove leading indents if root is a primitive
	if (
		type && gsf_type_is_primitive(type) ||
		!type && gsf_typing_get_default(typing) && gsf_type_is_primitive(gsf_typing_get_default(typing))
	) {
		while (token - tokens < token_count && token->type == token_indentation) token++;
		if (token - tokens >= token_count) token--; // at least keep the last indent
	}

	goto init_value;
	while (token - tokens < token_count) {
		GSF_ASSERT(token->type == token_indentation);
		token++;
		if (token - tokens >= token_count) break;
		if (!gsf_type_is_string(values[depth].type)) {
			if (token->type == token_indentation) continue;
			if ((token - 1)->data_int > depth) {
				GSF_PARSE_RAISE("bad indentation depth", token->offset);
			}
		}
		while (depth > (token - 1)->data_int) {
			if (gsf_type_is_object(values[depth].type)) {
				gsf_object_apply_defaults(values[depth].casted.object);
				if (gsf_object_count_unset(values[depth].casted.object) > 0) {
					GSF_PARSE_RAISE("object does not initialize all class values", (token - 1)->offset);
				}
			}
			depth--;
		}

		if (gsf_type_is_string(values[depth].type)) {
			// multi-line string literal
			size_t len = 0;
			if (gsf_value_decode_str(values[depth].val)[0] != '\0') {
				if (reader->flags & GSF_TXT_READ_ARTICLE_STRING_BLEND) {
					len = strlen(temp_string);
				} else {
					GSF_PARSE_RAISE(
						"with string blend mode disabled, one-line strings cannot continue to further lines",
						token->offset
					);
				}
			}
			token--; // back to indentation
			while (token - tokens < token_count - 1 && token->data_int >= depth) {
				if (token->data_int > depth) {
					// TODO parse additional indents as part of the string
				}
				token++;
				if (token->type == token_string) {
					size_t token_len = token->data_str - token->offset;
					GSF_TEMP_STR(len + token_len + 1);
					memcpy(temp_string + len, buf + token->offset, token_len);
					len += token_len + 1;
					temp_string[len - 1] = '\n';
					token++;
					GSF_ASSERT(token->type == token_indentation);
				} else {
					// assemble string from bad tokens
					while (token->type != token_indentation) {
						size_t token_len = (token + 1)->offset - token->offset;
						// if (token + 1) is an indentation token, line end is included
						// in the token_len so we don't need to add '\n' manually
						GSF_TEMP_STR(len + token_len);
						memcpy(temp_string + len, buf + token->offset, token_len);
						len += token_len;
						token++;
					}
				}
				if (!gsf_value_encode_str(values[depth].val, temp_string)) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
			}
			continue;
		} else if (gsf_type_is_primitive(values[depth].type)) {
			// primitives other than string are always one-liners
			GSF_PARSE_CLEANUP();
			GSF_RAISEF(gsf_eukntype, "unknown type '%s'", gsf_type_to_string(values[depth].type));
		}

		if (gsf_type_is_array(values[depth].type)) {
			// multi-line array
			const gsf_type* elem_type = gsf_type_get_element_type(values[depth].type);
			token--; // back to indentation
			uint8_t elem_width = gsf_type_scalar_width(elem_type);
			void* val_ptr = gsf_array_as_any(values[depth].casted.array) + gsf_array_get_size(values[depth].casted.array);
			while (token - tokens < token_count - 1 && token->data_int >= depth) {
				if (token->data_int > depth) {
					GSF_PARSE_RAISE("bad indentation depth", token->offset);
				}
				token++;
				while (token->type != token_indentation) {
					// TODO bulk array resize?
					size_t old_size = gsf_array_get_size(values[depth].casted.array);
					gsf_array_resize(values[depth].casted.array, old_size + 1);
					void* val_ptr = gsf_array_as_any(values[depth].casted.array) + old_size * elem_width;
					if ((err_buf = token_to_scalar(val_ptr, token, elem_type))) {
						GSF_PARSE_RAISE(err_buf, token->offset);
					}
					token++;
					if (token->type == token_indentation) break;
					if (token->type != token_special || token->data_int != 2) {
						GSF_PARSE_RAISE("excepted array delimeter (;) or line end", token->offset);
					}
					token++;
				}
			}
			continue;
		}

		// add to parent container
		if (token->type == token_key) {
			key_token = token;
			token++;
		} else {
			key_token = 0;
		}
		if (token->type == token_type_spec) {
			type_token = token;
			token++;
		} else {
			type_token = 0;
		}
		if (token->type != token_special || token->data_int != 1) {
			GSF_PARSE_RAISE("expected key-value separator (:)", token->offset);
		}
		token++;
		if (gsf_type_is_list(values[depth].type)) {
			if (key_token) GSF_PARSE_RAISE("list keys must be empty", key_token->offset);
			values[depth + 1].val = gsf_list_add_element(values[depth].casted.list);
			if (!values[depth + 1].val) {
				GSF_PARSE_CLEANUP();
				return 0;
			}
		} else if (gsf_type_is_map(values[depth].type)) {
			if (!key_token) GSF_PARSE_RAISE("map keys must not be empty", key_token->offset);
			GSF_TOKEN_STR(key_token);
			values[depth + 1].val = gsf_map_add_element(values[depth].casted.map, temp_string);
			if (!values[depth + 1].val) {
				GSF_PARSE_CLEANUP();
				return 0;
			}
		} else if (gsf_type_is_object(values[depth].type)) {
			if (key_token) {
				GSF_TOKEN_STR(key_token);
				values[depth + 1].val = gsf_object_access_at_key(values[depth].casted.object, temp_string);
				if (!values[depth + 1].val) {
					GSF_PARSE_RAISE("unknown label key", key_token->offset);
				}
				if (gsf_value_get_type(values[depth + 1].val)) {
					GSF_PARSE_RAISE("value at this label is already set", key_token->offset);
				}
			} else {
				values[depth + 1].val = gsf_object_access_next(values[depth].casted.object);
				if (!values[depth + 1].val) {
					GSF_PARSE_RAISE("no more labels (add key?)", token->offset);
				}
			}
		} else GSF_RAISE(gsf_eassert, "already checked type fail on second check");
		depth++;

		// encode the value
		init_value:
		typing = gsf_value_get_typing(values[depth].val);
		values[depth].type = gsf_typing_get_default(typing);
		if (type_token) {
			GSF_TOKEN_STR(type_token);
			int result = gsf_typing_match_type(typing, temp_string, &values[depth].type);
			if (result < 0) {
				// TODO add part of buffer to the error msg? (or add an error stack/cause system)
				GSF_PARSE_CLEANUP();
				return 0;
			} else if (result == 0) {
				GSF_PARSE_RAISE("selected type is not supported by value's typing", type_token->offset);
			}
		} else if (type) {
			// first value with type from func argument
			values[depth].type = type;
			type = 0;
		} else if (!values[depth].type) {
			GSF_PARSE_RAISE("no type was selected yet no default exists for that value", token->offset > 1 ? token->offset - 2 : token->offset);
		}
		if (gsf_type_is_null(values[depth].type)) {
			while (token->type != token_indentation) token++;
			gsf_value_encode_null(values[depth].val);
			if (depth > 0) depth--;
		} else if (gsf_type_is_primitive(values[depth].type)) {
			// one-line primitive
			bool keep_depth = false;
			if (gsf_type_is_string(values[depth].type)) {
				if (token->type == token_indentation) {
					GSF_TEMP_STR(0); // FIXME crashes on empty string??
					keep_depth = true;
				} else if (token->type == token_string) {
					GSF_TOKEN_STR(token);
					token++;
					GSF_ASSERT(token->type == token_indentation);
				} else {
					// assemble string from bad tokens
					size_t len = 0;
					while (token->type != token_indentation) {
						size_t token_len = (token + 1)->offset - token->offset;
						if (token_len == 0) continue;
						if (buf[token->offset + token_len - 1] == '\n') token_len--;
						GSF_TEMP_STR(len + token_len);
						memcpy(temp_string + len, buf + token->offset, token_len);
						len += token_len;
						token++;
					}
				}
				if (!gsf_value_encode_str(values[depth].val, temp_string)) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
			} else if (gsf_type_is_scalar(values[depth].type)) {
				if ((err_buf = token_to_scalar(gsf_value_encode_any(values[depth].val, values[depth].type), token, values[depth].type))) {
					GSF_PARSE_RAISE(err_buf, token->offset);
				}
				token++;
			} else if (gsf_type_is_enum(values[depth].type)) {
				if (token->type != token_string) {
					GSF_PARSE_RAISE("bad enum variant format", token->offset);
				}
				GSF_TOKEN_STR(token);
				if (!gsf_value_encode_enum_str(values[depth].val, gsf_type_get_enum(values[depth].type), temp_string)) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
				token++;
				GSF_ASSERT(token->type == token_indentation);
			} else {
				GSF_PARSE_CLEANUP();
				GSF_RAISEF(gsf_eukntype, "unknown type '%s'", gsf_type_to_string(values[depth].type));
			}
			if (!keep_depth && depth > 0) depth--;
		} else {
			// non-primitive
			if (gsf_type_is_array(values[depth].type)) {
				const gsf_type* elem_type = gsf_type_get_element_type(values[depth].type);
				values[depth].casted.array = gsf_value_encode_array(values[depth].val, elem_type, 0);
				if (!values[depth].casted.array) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
				uint8_t elem_width = gsf_type_scalar_width(elem_type);
				while (token->type != token_indentation) {
					size_t old_size = gsf_array_get_size(values[depth].casted.array);
					gsf_array_resize(values[depth].casted.array, old_size + 1);
					void* val_ptr = gsf_array_as_any(values[depth].casted.array) + old_size * elem_width;
					if ((err_buf = token_to_scalar(val_ptr, token, elem_type))) {
						GSF_PARSE_RAISE(err_buf, token->offset);
					}
					token++;
					if (token->type == token_indentation) break;
					if (token->type != token_special || token->data_int != 2) {
						GSF_PARSE_RAISE("excepted array delimeter (;) or line end", token->offset);
					}
					token++;
				}
			} else if (gsf_type_is_list(values[depth].type)) {
				values[depth].casted.list = gsf_value_encode_list(
					values[depth].val, gsf_type_get_element_typing(values[depth].type), 0
				);
				if (!values[depth].casted.list) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
			} else if (gsf_type_is_map(values[depth].type)) {
				values[depth].casted.map = gsf_value_encode_map(
					values[depth].val, gsf_type_get_element_typing(values[depth].type), 0
				);
				if (!values[depth].casted.map) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
			} else if (gsf_type_is_object(values[depth].type)) {
				values[depth].casted.object = gsf_value_encode_object(
					values[depth].val, gsf_type_get_object_class(values[depth].type)
				);
				if (!values[depth].casted.object) {
					GSF_PARSE_CLEANUP();
					return 0;
				}
			} else {
				GSF_PARSE_CLEANUP();
				GSF_RAISEF(gsf_eukntype, "unknown type '%s'", gsf_type_to_string(values[depth].type));
			}
			if (token->type != token_indentation) {
				GSF_PARSE_RAISE("containers other than array must start with a line end", token->offset);
			}
		}
	};
	depth++;
	while (depth-- > 0) {
		if (gsf_type_is_object(values[depth].type)) {
			gsf_object_apply_defaults(values[depth].casted.object);
			if (gsf_object_count_unset(values[depth].casted.object) > 0) {
				// FIXME not always root
				GSF_RAISE(gsf_etxtart, "root object does not initialize all class values");
			}
		}
	}

	gsf_free(temp_string);
	gsf_free(tokens);
	return 1;
}
