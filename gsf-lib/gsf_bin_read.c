#include "gsf_codec.h"

#include <stdint.h>
#include <string.h>
#include "gsf_types.h"
#include "gsf_itm.h"
#include "gsf_data.h"
#include "gsf_err.h"

#include "gsf_util.c"

extern void* gsf_value_encode_any(gsf_value* holder, const gsf_type* type);

// current position
#define PTR reader->ptr
// current space left
#define GSF_SL (reader->end - reader->ptr)
// require space
#define GSF_RS(space) do {								\
	if (space <= GSF_SL) break;							\
	int ret = reader->read(reader, 2048 + GSF_SL);		\
	if (ret <= 0) {										\
		if (!ret) GSF_RAISE(gsf_eshortbuf);				\
		return 0;										\
	}													\
	if (GSF_SL < space) GSF_RAISE(gsf_eshortbuf);		\
} while (0)

int gsf_bin_value_read(struct gsf_reader* reader, gsf_value* holder);
int gsf_bin_vsize_read(struct gsf_reader* reader, size_t* value);

#include <stdio.h>
int gsf_bin_article_read_desc(struct gsf_reader* reader, gsf_descriptor* descriptor) {
	// descriptor size
	size_t desc_size;
	if (!gsf_bin_vsize_read(reader, &desc_size)) return 0;
	if (desc_size == 0) {
		// schemaless
		if (descriptor && !gsf_descriptor_init(descriptor, "", "", "")) return 0;
		return 1;
	}
	// descriptor
	GSF_RS(desc_size);
	const void* start = PTR;
	const char* desc_strs[3] = {};
	for (size_t i = 0; i < 3; i++) {
		desc_strs[i] = (const char*) PTR;
		PTR = memchr(PTR, '\0', desc_size - (PTR - start));
		if (!PTR++) GSF_RAISE(gsf_ebinart);
	}
	if (PTR - start != desc_size) GSF_RAISE(gsf_ebinart);
	if (descriptor && !gsf_descriptor_init(descriptor, desc_strs[0], desc_strs[1], desc_strs[2])) return 0;
	return 1;
}

int gsf_bin_article_read_proper(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema) {
	if (!gsf_article_init(article, schema)) return 0;
	gsf_value* root = gsf_article_access_root(article);
	return gsf_bin_value_read(reader, root);
}

int gsf_bin_value_read(struct gsf_reader* reader, gsf_value* holder) {
	const gsf_typing* typing = gsf_value_get_typing(holder);
	const gsf_type* type = gsf_typing_get_default(typing);
	if (gsf_typing_is_any(typing)) {
		// free type choice
		// TODO use binary type format
		size_t type_len;
		if (!gsf_bin_vsize_read(reader, &type_len)) return 0;
		GSF_RS(type_len);
		type = gsf_itm_str_buf_to_type(gsf_typing_manager(typing), PTR, type_len);
		if (!type) return 0;
		PTR += type_len;
	} else if (gsf_typing_is_omnibus(typing)) {
		// type selection
		size_t choice;
		if (!gsf_bin_vsize_read(reader, &choice)) return 0;
		type = gsf_typing_get_type(typing, choice);
	}
	if (gsf_type_is_null(type)) {
	} else if (gsf_type_is_scalar(type)) {
		size_t width = gsf_type_scalar_width(type);
		GSF_RS(width);
		void* value = gsf_value_encode_any(holder, type);
		if (!value) return 0;
		// value
		memcpy(value, PTR, width);
		PTR += width;
	} else if (gsf_type_is_enum(type)) {
		size_t index;
		if (!gsf_bin_vsize_read(reader, &index)) return 0;
		if (!gsf_value_encode_enum_idx(holder, gsf_type_get_enum(type), index)) return 0;
	} else if (gsf_type_is_string(type)) {
		// size
		size_t count;
		if (!gsf_bin_vsize_read(reader, &count)) return 0;
		// chars
		GSF_RS(count);
		char* string = gsf_alloc(count + 1);
		memcpy(string, PTR, count);
		PTR += count;
		string[count] = '\0';
		gsf_value* result = gsf_value_encode_str(holder, string);
		gsf_free(string);
		if (!result) return 0;
	} else if (gsf_type_is_array(type)) {
		const gsf_type* elem_type = gsf_type_get_element_type(type);
		// count
		size_t count;
		if (!gsf_bin_vsize_read(reader, &count)) return 0;
		gsf_array* array = gsf_value_encode_array(holder, elem_type, count);
		if (!array) return 0;
		// values
		size_t width = gsf_type_scalar_width(elem_type);
		size_t space = count * width;
		GSF_RS(space);
		void* values = gsf_array_as_any(array);
		memcpy(values, PTR, space);
		PTR += space;
	} else if (gsf_type_is_object(type)) {
		// count
		size_t count;
		if (!gsf_bin_vsize_read(reader, &count)) return 0;
		// fields
		const gsf_class* class = gsf_type_get_object_class(type);
		gsf_object* object = gsf_value_encode_object(holder, class);
		if (!object) return 0;
		for (size_t i = 0; i < count; i++) {
			// index
			size_t index;
			if (!gsf_bin_vsize_read(reader, &index)) return 0;
			const gsf_label* label = gsf_class_get_label(class, index);
			// value
			gsf_value* value = gsf_object_access_at_label(object, label);
			if (!value) return 0;
			if (!gsf_bin_value_read(reader, value)) return 0;
		}
		gsf_object_apply_defaults(object);
	} else if (gsf_type_is_list(type)) {
		// count
		size_t count;
		if (!gsf_bin_vsize_read(reader, &count)) return 0;
		// values
		const gsf_typing* elem_typing = gsf_type_get_element_typing(type);
		gsf_list* list = gsf_value_encode_list(holder, elem_typing, count);
		if (!list) return 0;
		for (size_t i = 0; i < count; i++) {
			// value
			gsf_value* value = gsf_list_add_element(list);
			if (!value) return 0;
			if (!gsf_bin_value_read(reader, value)) return 0;
		}
	} else if (gsf_type_is_map(type)) {
		// count
		size_t count;
		if (!gsf_bin_vsize_read(reader, &count)) return 0;
		// values
		const gsf_typing* elem_typing = gsf_type_get_element_typing(type);
		gsf_map* map = gsf_value_encode_map(holder, elem_typing, count);
		if (!map) return 0;
		for (size_t i = 0; i < count; i++) {
			// key size
			size_t key_size;
			if (!gsf_bin_vsize_read(reader, &key_size)) return 0;
			// key
			GSF_RS(key_size);
			char* key = gsf_alloc(key_size + 1);
			memcpy(key, PTR, key_size);
			key[key_size] = '\0';
			PTR += key_size;
			// value
			gsf_value* value = gsf_map_add_element(map, key);
			gsf_free(key);
			if (!value) return 0;
			if (!gsf_bin_value_read(reader, value)) return 0;
		}
	} else GSF_RAISE(gsf_ewrongtype);
	return 1;
}

int gsf_bin_vsize_read(struct gsf_reader* reader, size_t* value) {
	// read value
	uint8_t bytes[10] = {0};
	uint8_t i = 1;
	// first byte is 0 for easier reading
	do {
		GSF_RS(1);
		bytes[i] = *(uint8_t*) PTR++;
	} while (bytes[i++] & 0x01);

	// convert from variable length
	*value = 0;
	uint8_t byte = i - 1;
	i = 0;
	do {
		*value <<= 8;
		uint8_t value_byte = 0;
		value_byte |= (bytes[i] & ~0x01) << (8 - byte - 1);
		i++;
		value_byte |= (bytes[i]) >> byte;
		*value |= value_byte;
		byte--;
	} while (bytes[i] & 0x01);

	return 1;
}
