#include <stddef.h>
#include "gsf_err.h"

#ifdef GSF_STOP_ON_RAISE
	#include "signal.h"
	#define GSF_RAISE_STOP do {raise(SIGSTOP);} while (0)
#else
	#define GSF_RAISE_STOP do {} while (0)
#endif // GSF_STOP_ON_RAISE

#define GSF_RAISEF(err, msg, ...) do {		\
	gsf_errno_setf(err, msg, __VA_ARGS__);	\
	GSF_RAISE_STOP; 						\
	return 0;								\
} while (0)

#define GSF_RAISE_2(err, msg) do {	\
	gsf_errno_set(err, msg); 		\
	GSF_RAISE_STOP; 				\
	return 0;						\
} while (0)
#define GSF_RAISE_1(err) GSF_RAISE_2(err, 0)

#define GSF_RAISE_X(x, err, msg, macro, ...) macro
#define GSF_RAISE(...) GSF_RAISE_X(, ##__VA_ARGS__, GSF_RAISE_2(__VA_ARGS__), GSF_RAISE_1(__VA_ARGS__))

#define GSF_ARRAY_LEN(arr) sizeof(arr) / sizeof(*arr);

#define GSF_ASSERT(x) do { if (!(x)) GSF_RAISE(gsf_eassert, #x); } while (0)

extern void* (*gsf_alloc)(size_t);
extern void* (*gsf_calloc)(size_t);
extern void* (*gsf_realloc)(void*, size_t);
extern void* (*gsf_crealloc)(void*, size_t, size_t);
extern void (*gsf_free)(void*);

typedef struct gsf_schema gsf_schema;
extern gsf_schema* gsf_get_master_schema();
