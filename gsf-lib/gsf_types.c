#include "gsf_itm.h"
#include "gsf_types.h"

#include <ctype.h>
#include <memory.h>
#include <string.h>
#include "gsf_util.c"
#include "gsf_data.h"

#define GSF_ITM_TYPES_CHUNK 64
#define GSF_ITM_TYPES_BUCKETS 128
#define GSF_ITM_TYPINGS_CHUNK 64
#define GSF_ITM_TYPINGS_BUCKETS 192
#define GSF_ITM_ALIASES_CHUNK 16

// Austin Appleby's 32-bit MurmurHash3 from Wikipedia
static inline uint32_t murmur_32_scramble(uint32_t k) {
	k *= 0xcc9e2d51;
	k = (k << 15) | (k >> 17);
	k *= 0x1b873593;
	return k;
}
uint32_t murmur3_32(const uint8_t* key, size_t len, uint32_t seed)
{
	uint32_t h = seed;
	uint32_t k;
	/* Read in groups of 4. */
	for (size_t i = len >> 2; i; i--) {
		// Here is a source of differing results across endiannesses.
		// A swap here has no effects on hash properties though.
		memcpy(&k, key, sizeof(uint32_t));
		key += sizeof(uint32_t);
		h ^= murmur_32_scramble(k);
		h = (h << 13) | (h >> 19);
		h = h * 5 + 0xe6546b64;
	}
	/* Read the rest. */
	k = 0;
	for (size_t i = len & 3; i; i--) {
		k <<= 8;
		k |= key[i - 1];
	}
	// A swap is *not* necessary here because the preceding loop already
	// places the low bytes in the low places according to whatever endianness
	// we use. Swaps only apply when the memory is copied in a chunk.
	h ^= murmur_32_scramble(k);
	/* Finalize. */
	h ^= len;
	h ^= h >> 16;
	h *= 0x85ebca6b;
	h ^= h >> 13;
	h *= 0xc2b2ae35;
	h ^= h >> 16;
	return h;
}


enum gsf_category {
	none, // uninitialized
	nul, // null
	pri, // primitive
	col, // collection
	enu, // enum
	obj, // object
};

enum gsf_collection_type {
	arr, // array
	lst, // list
	map, // map
};

static const char* gsf_collection_codenames[] = {
	[arr] = "arr",
	[lst] = "lst",
	[map] = "map",
};

enum gsf_primitive_type {
	// integer types
	i8, i16, i32, i64,
	// unsigned integer types
	u8, u16, u32, u64,
	// floating point types
	f32, f64,
	// string type
	str
};

static const char* gsf_primitive_codenames[] = {
	[i8] = "i8",
	[i16] = "i16",
	[i32] = "i32",
	[i64] = "i64",
	[u8] = "u8",
	[u16] = "u16",
	[u32] = "u32",
	[u64] = "u64",
	[f32] = "f32",
	[f64] = "f64",
	[str] = "str",
};

struct gsf_type {
	__attribute__((unused)) unsigned char null_tag; // always 0 to distinguish from string
	gsf_itm* manager;
	uint32_t hash;
	char* textual;
	enum gsf_category category;
	union {
		enum gsf_primitive_type pri;
		struct {
			enum gsf_collection_type collection;
			union {
				const gsf_typing* ing;
				const gsf_type* e;
			} typ;
		} col;
		const gsf_class* obj;
		const gsf_enum* enu;
	} spec;
	gsf_type* next;
};

struct gsf_typing {
	__attribute__((unused)) unsigned char null_tag; // always 0 to distinguish from string
	gsf_itm* manager;
	uint32_t hash;
	char* textual;
	size_t types_size;
	size_t types_count;
	const gsf_type** types;
	const gsf_type* default_type;
	gsf_typing* next;
};

// note: type and typing construction functions are at the bottom of the file

char* gsf_typing_to_string_buf(const gsf_typing* self, char* buf, size_t buf_size) {
	return gsf_itm_typing_to_str_buf(self->manager, self, buf, buf_size);
}
const char* gsf_typing_to_string(const gsf_typing* self) {
	return gsf_itm_typing_to_str(self->manager, self);
}

int gsf_typing_is_any(const gsf_typing* self) {
	return self->types_count == 0;
}
size_t gsf_typing_type_count(const gsf_typing* self) {
	return self->types_count;
}
const gsf_type* gsf_typing_get_type(const gsf_typing* self, size_t index) {
	if (index >= self->types_count) return 0;
	return self->types[index];
}
const gsf_type* gsf_typing_get_default(const gsf_typing* self) {
	return self->default_type;
}

int gsf_typing_match_type(const gsf_typing* self, const gsf_type_or_char* preset, const gsf_type** out) {
	if (gsf_typing_is_any(self)) {
		const gsf_type* type = gsf_itm_get_type(self->manager, preset);
		if (!type) return -1;
		if (out) *out = type;
		return 1;
	}
	for (size_t i = 0; i < self->types_count; i++) {
		int result = gsf_type_match(self->types[i], preset);
		if (result != 0) {
			if (out) *out = self->types[i];
			return result;
		}
	}
	return 0;
}
int gsf_typing_match(const gsf_typing* self, const gsf_typing_or_char* preset) {
	const gsf_typing* other = gsf_itm_get_typing(self->manager, preset);
	if (!other) return -1;
	if (self->types_count > other->types_count) return 0;
	for (size_t i = 0; i < self->types_count; i++) {
		int result = gsf_typing_match_type(other, self->types[i], 0);
		if (result != 0) return result;
	}
	return 1;
}
int gsf_typing_match_strict(const gsf_typing* self, const gsf_typing_or_char* preset) {
	const gsf_typing* other = gsf_itm_get_typing(self->manager, preset);
	if (!other) return -1;
	return self == other;
}

int gsf_typing_is_omnibus(const gsf_typing* self) {
	return self->types_count != 1;
}

int gsf_typing_find(const gsf_typing* self, const gsf_type_or_char* preset, size_t* out) {
	const gsf_type* type = gsf_itm_get_type(self->manager, preset);
	if (!type) return -1;
	for (size_t i = 0; i < self->types_count; i++) {
		if (self->types[i] == type) {
			*out = i;
			return 1;
		}
	}
	return 0;
}


char* gsf_type_to_string_buf(const gsf_type* self, char* buf, size_t buf_size) {
	return gsf_itm_type_to_str_buf(self->manager, self, buf, buf_size);
}
const char* gsf_type_to_string(const gsf_type* self) {
	return gsf_itm_type_to_str(self->manager, self);
}

int gsf_type_match(const gsf_type* self, const gsf_type_or_char* preset) {
	const gsf_type* other = gsf_itm_get_type(self->manager, preset);
	if (!other) return -1;
	if (self == other) return 1;
	if (self->category != col || other->category != col) {
		return 0;
	} else {
		if (self->spec.col.collection != other->spec.col.collection) return 0;
		if (self->spec.col.collection == arr) {
			const gsf_type* self_elem = gsf_type_get_element_type(self);
			const gsf_type* other_elem = gsf_type_get_element_type(other);
			return gsf_type_match(self_elem, other_elem);
		} else {
			const gsf_typing* self_elem = gsf_type_get_element_typing(self);
			const gsf_typing* other_elem = gsf_type_get_element_typing(other);
			return gsf_typing_match(self_elem, other_elem);
		}
	}
}
int gsf_type_match_strict(const gsf_type* self, const gsf_type_or_char* preset) {
	const gsf_type* other = gsf_itm_get_type(self->manager, preset);
	if (!other) return -1;
	return self == other;
}

uint8_t gsf_type_scalar_width(const gsf_type* type) {
	enum gsf_primitive_type adequate_signed;
	if (type->spec.pri >= f32) {
		adequate_signed = type->spec.pri - 6;
	} else if (type->spec.pri >= u8) {
		adequate_signed = type->spec.pri - 4;
	} else {
		adequate_signed = type->spec.pri;
	}
	unsigned char bytes = 1;
	while (adequate_signed > i8) {
		bytes *= 2;
		adequate_signed--;
	}
	return bytes;
}
int gsf_type_is_null(const gsf_type* self) {
	return self->category == nul;
}
int gsf_type_is_primitive(const gsf_type* self) {
	return self->category == nul || self->category == pri || self->category == enu;
}
int gsf_type_is_string(const gsf_type* self) {
	return self->category == pri && self->spec.pri == str;
}
int gsf_type_is_scalar(const gsf_type* self) {
	return self->category == pri && self->spec.pri != str;
}
int gsf_type_is_int(const gsf_type* self) {
	return self->category == pri && self->spec.pri >= i8 && self->spec.pri <= i64;
}
int gsf_type_is_uint(const gsf_type* self) {
	return self->category == pri && self->spec.pri >= u8 && self->spec.pri <= u64;
}
int gsf_type_is_float(const gsf_type* self) {
	return self->category == pri && self->spec.pri >= f32 && self->spec.pri <= f64;
}
int gsf_type_is_container(const gsf_type* self) {
	return self->category == col || self->category == obj;
}
int gsf_type_is_collection(const gsf_type* self) {
	return self->category == col;
}
int gsf_type_is_array(const gsf_type* self) {
	return self->category == col && self->spec.col.collection == arr;
}
int gsf_type_is_list(const gsf_type* self) {
	return self->category == col && self->spec.col.collection == lst;
}
int gsf_type_is_map(const gsf_type* self) {
	return self->category == col && self->spec.col.collection == map;
}
int gsf_type_is_object(const gsf_type* self) {
	return self->category == obj;
}
int gsf_type_is_enum(const gsf_type* self) {
	return self->category == enu;
}

const gsf_type* gsf_type_get_element_type(const gsf_type* self) {
	if (!gsf_type_is_array(self)) GSF_RAISE(gsf_ewrongtype);
	return self->spec.col.typ.e;
}
const gsf_typing* gsf_type_get_element_typing(const gsf_type* self) {
	if (!gsf_type_is_list(self) && !gsf_type_is_map(self)) GSF_RAISE(gsf_ewrongtype);
	return self->spec.col.typ.ing;
}
const gsf_class* gsf_type_get_object_class(const gsf_type* self) {
	if (!gsf_type_is_object(self)) GSF_RAISE(gsf_ewrongtype);
	return self->spec.obj;
}
const gsf_enum* gsf_type_get_enum(const gsf_type* self) {
	if (!gsf_type_is_enum(self)) GSF_RAISE(gsf_ewrongtype);
	return self->spec.enu;
}
gsf_itm* gsf_type_manager(const gsf_type* self) {
	return self->manager;
}
gsf_itm* gsf_typing_manager(const gsf_typing* self) {
	return self->manager;
}

struct type_map {
	size_t space; // amount of arrays
	size_t count; // amount of added types
	gsf_type** arrays;
	gsf_type* buckets[GSF_ITM_TYPES_BUCKETS];
};

struct typing_map {
	size_t space; // amount of arrays
	size_t count; // amount of added typings
	gsf_typing** arrays;
	gsf_typing* buckets[GSF_ITM_TYPINGS_BUCKETS];
};

struct hash_alias {
	uint32_t a;
	uint32_t b;
};

struct alias_array {
	size_t count;
	size_t size;
	struct hash_alias* array;
};

struct gsf_itm {
	unsigned char initialized;
	struct type_map types;
	struct typing_map typings;
	struct alias_array aliases;
};

int gsf_itm_hash_name(const gsf_itm* self, const char* data, size_t length, uint32_t* out) {
	*out = murmur3_32((const uint8_t*) data, length, 84712398);
	for (size_t i = 0; i < self->aliases.count; i++) {
		if (self->aliases.array[i].b == *out) {
			*out = self->aliases.array[i].a;
			break;
		}
	}
	return 1;
}

int gsf_itm_hash_notation(const gsf_itm* self, const char* data, size_t length, uint32_t* out) {
	const char* ptr = data;
	size_t depth = 0;
	size_t max_depth = 0;
	while (ptr - data < length) {
		if (isalnum(*ptr) || isblank(*ptr) || *ptr == '!' || *ptr == '|' || *ptr == '?') {
			ptr++;
			continue;
		}
		if (*ptr == '[') {
			depth++;
			if (depth > max_depth) max_depth = depth;
		} else if (*ptr == ']') {
			if (depth == 0) GSF_RAISE(gsf_etypingfmt, "Notation's ']' does not match a '['.");
			depth--;
		} else {
			GSF_RAISE(gsf_etypingfmt, "Bad character in a type or typing notation.");
		}
		ptr++;
	}
	if (depth > 0) GSF_RAISE(gsf_etypingfmt, "Notation's '[' deos not match a ']'.");

	const char* start = 0;
	const char* end = 0;
	uint32_t hash[(max_depth + 1) * 2];
	hash[0] = 0;
	hash[1] = 0;
	depth = 0;
	ptr = data;
	while (ptr - data < length) {
		if (isblank(*ptr)) {
			if (end) GSF_RAISE(gsf_etypingfmt, "Type name contains blank characters.");
			if (start) end = ptr;
		} else if (isalnum(*ptr) || *ptr == '!' || *ptr == '?') {
			// hashing '!' with the type name is the easiest way to hash the defaults
			if (!start) start = ptr;
		} else {
			if (!start && *ptr == '[') GSF_RAISE(gsf_etypingfmt, "Expected type name.");
			if (!end) end = ptr;
			uint32_t hashed = 0;
			if (start && !gsf_itm_hash_name(self, start, end - start, &hashed)) return 0;
			if (*ptr == '[') {
				depth++;
				hash[depth * 2] = hashed; // will be needed to hash with contents
				hash[depth * 2 + 1] = 0;
			} else if (*ptr == ']') {
				hash[depth * 2 + 1] += hashed;
				hash[depth * 2] = murmur3_32((uint8_t*) &hash[depth * 2], 2 * sizeof(uint32_t), 374453424);
				depth--;
				hash[depth * 2 + 1] += hash[(depth + 1) * 2];
			} else { // '|'
				hash[depth * 2 + 1] += hashed;
			}
			start = 0;
			end = 0;
		}
		ptr++;
	}
	if (start) {
		if (!end) end = ptr;
		uint32_t hashed;
		if (!gsf_itm_hash_name(self, start, end - start, &hashed)) return 0;
		hash[1] += hashed;
	}
	*out = murmur3_32((uint8_t*) &hash[0], 2 * sizeof(uint32_t), 374453424);
	return 1;
}

int gsf_itm_add_alias_bufs(gsf_itm* self, const char* a, size_t a_len, const char* b, size_t b_len) {
	if (self->aliases.count >= self->aliases.size) {
		self->aliases.size += GSF_ITM_ALIASES_CHUNK;
		struct hash_alias* new_array = gsf_realloc(self->aliases.array, self->aliases.size);
		if (!new_array) GSF_RAISE(gsf_emem);
		self->aliases.array = new_array;
	}
	self->aliases.array[self->aliases.count].a = murmur3_32((const uint8_t*) a, a_len, 84712398);
	self->aliases.array[self->aliases.count].b = murmur3_32((const uint8_t*) b, b_len, 84712398);
	self->aliases.count++;
	return 1;
}
int gsf_itm_add_alias(gsf_itm* self, const char* a, const char* b) {
	return gsf_itm_add_alias_bufs(self, a, strlen(a), b, strlen(b));
}

gsf_itm* gsf_create_itm(void) {
	gsf_itm* itm = gsf_alloc(sizeof(gsf_itm));
	itm->initialized = 0;
	return itm;
}
void gsf_delete_itm(gsf_itm* manager) {
	if (!manager) return;
	gsf_itm_destroy(manager);
	gsf_free(manager);
}

gsf_itm* gsf_itm_init(gsf_itm* self) {
	self->initialized = 1;
	self->types.count = 0;
	self->types.space = 1;
	self->types.arrays = gsf_alloc(sizeof(void*));
	if (!self->types.arrays) GSF_RAISE(gsf_emem);
	self->types.arrays[0] = gsf_calloc(GSF_ITM_TYPES_CHUNK * sizeof(gsf_type));
	if (!self->types.arrays[0]) GSF_RAISE(gsf_emem);
	memset(self->types.buckets, 0, sizeof(self->types.buckets));

	self->typings.count = 0;
	self->typings.space = 1;
	self->typings.arrays = gsf_alloc(sizeof(void*));
	if (!self->typings.arrays) GSF_RAISE(gsf_emem);
	self->typings.arrays[0] = gsf_calloc(GSF_ITM_TYPINGS_CHUNK * sizeof(gsf_typing));
	if (!self->typings.arrays[0]) GSF_RAISE(gsf_emem);
	memset(self->typings.buckets, 0, sizeof(self->typings.buckets));

	self->aliases.count = 0;
	self->aliases.size = GSF_ITM_ALIASES_CHUNK;
	self->aliases.array = gsf_alloc(GSF_ITM_ALIASES_CHUNK * sizeof(struct hash_alias));
	gsf_itm_add_alias(self, "i8", "i08");
	gsf_itm_add_alias(self, "u8", "u08");
	gsf_itm_add_alias(self, "str", "string");
	gsf_itm_add_alias(self, "arr", "array");
	gsf_itm_add_alias(self, "lst", "list");
	gsf_itm_add_alias(self, "nul", "null");

	return self;
}
void gsf_itm_destroy(gsf_itm* self) {
	if (!self->initialized) return;
	for (size_t i = 0; i < self->types.count; i++) {
		gsf_type* type = self->types.arrays[i / GSF_ITM_TYPES_CHUNK] + i % GSF_ITM_TYPES_CHUNK;
		gsf_free(type->textual);
	}
	for (size_t i = 0; i < self->types.space; i++) {
		gsf_free(self->types.arrays[i]);
	}
	gsf_free(self->types.arrays);
	for (size_t i = 0; i < self->typings.count; i++) {
		gsf_typing* typing = self->typings.arrays[i / GSF_ITM_TYPINGS_CHUNK] + i % GSF_ITM_TYPINGS_CHUNK;
		gsf_free(typing->textual);
		gsf_free(typing->types);
	}
	for (size_t i = 0; i < self->typings.space; i++) {
		gsf_free(self->typings.arrays[i]);
	}
	gsf_free(self->typings.arrays);
	gsf_free(self->aliases.array);
}

size_t gsf_itm_reserve_types(gsf_itm* self, size_t amount) {
	if (amount == 0) amount++;
	if (amount % GSF_ITM_TYPES_CHUNK > GSF_ITM_TYPES_CHUNK - self->types.count % GSF_ITM_TYPES_CHUNK) {
		// add another array - last array's remaining space is not sufficient for the remainder
		amount += GSF_ITM_TYPES_CHUNK;
	}
	amount /= GSF_ITM_TYPES_CHUNK; // count in chunks
	self->types.arrays = gsf_realloc(self->types.arrays, (self->types.space + amount) * sizeof(void*));
	size_t space = 0;
	for (space = self->types.space; space < self->types.space + amount; space++) {
		self->types.arrays[space] = gsf_calloc(GSF_ITM_TYPES_CHUNK * sizeof(gsf_typing));
		if (!self->types.arrays[space]) {
			self->types.space = space + 1;
			GSF_RAISE(gsf_emem);
		}
	}
	self->types.space = space;
	return self->types.space * GSF_ITM_TYPES_CHUNK - self->types.count;
}
size_t gsf_itm_reserve_typings(gsf_itm* self, size_t amount) {
	if (amount == 0) amount++;
	if (amount % GSF_ITM_TYPINGS_CHUNK > GSF_ITM_TYPINGS_CHUNK - self->typings.count % GSF_ITM_TYPINGS_CHUNK) {
		// add another array - last array's remaining space is not sufficient for the remainder
		amount += GSF_ITM_TYPINGS_CHUNK;
	}
	amount /= GSF_ITM_TYPINGS_CHUNK; // count in chunks
	self->typings.arrays = gsf_realloc(self->typings.arrays, (self->typings.space + amount) * sizeof(void*));
	size_t space = 0;
	for (space = self->typings.space; space < self->typings.space + amount; space++) {
		self->typings.arrays[space] = gsf_calloc(GSF_ITM_TYPINGS_CHUNK * sizeof(gsf_typing));
		if (!self->typings.arrays[space]) {
			self->typings.space = space + 1;
			GSF_RAISE(gsf_emem);
		}
	}
	self->typings.space = space;
	return self->typings.space * GSF_ITM_TYPINGS_CHUNK - self->typings.count;
}
static gsf_type* gsf_itm_reserve_type(gsf_itm* self) {
	if (!gsf_itm_reserve_types(self, 1)) return 0;
	return self->types.arrays[self->types.count / GSF_ITM_TYPINGS_CHUNK] + self->types.count % GSF_ITM_TYPINGS_CHUNK;
}
static gsf_typing* gsf_itm_reserve_typing(gsf_itm* self) {
	if (!gsf_itm_reserve_typings(self, 1)) return 0;
	return self->typings.arrays[self->typings.count / GSF_ITM_TYPINGS_CHUNK] + self->typings.count % GSF_ITM_TYPINGS_CHUNK;
}
static gsf_type* gsf_itm_put_type(gsf_itm* self, const char* name, size_t name_len) {
	uint32_t hash;
	if (!gsf_itm_hash_notation(self, name, name_len, &hash)) return 0;
	uint8_t bucket = hash % GSF_ARRAY_LEN(self->types.buckets);
	gsf_type* li = self->types.buckets[bucket];
	while (li) {
		if (li->hash == hash) GSF_RAISE(gsf_ekeytaken);
		li = li->next;
	}
	char* name_buf = gsf_alloc(name_len + 1);
	if (!name_buf) return 0;
	memcpy(name_buf, name, name_len);
	name_buf[name_len] = '\0';
	gsf_type* type = gsf_itm_reserve_type(self);
	if (!type) {
		gsf_free(name_buf);
		return 0;
	}
	self->types.count++;
	type->null_tag = 0;
	type->manager = self;
	type->hash = hash;
	type->textual = name_buf;
	li = self->types.buckets[bucket];
	if (!li) {
		self->types.buckets[bucket] = type;
	} else {
		while (li->next) li = li->next;
		li->next = type;
	}
	return type;
}
static gsf_typing* gsf_itm_put_typing(gsf_itm* self, const char* name, size_t name_len) {
	uint32_t hash;
	if (!gsf_itm_hash_notation(self, name, name_len, &hash)) return 0;
	uint8_t bucket = hash % GSF_ARRAY_LEN(self->typings.buckets);
	gsf_typing* li = self->typings.buckets[bucket];
	while (li) {
		if (li->hash == hash) GSF_RAISE(gsf_ekeytaken);
		li = li->next;
	}
	char* name_buf = gsf_alloc(name_len + 1);
	if (!name_buf) return 0;
	memcpy(name_buf, name, name_len);
	name_buf[name_len] = '\0';
	gsf_typing* typing = gsf_itm_reserve_typing(self);
	if (!typing) {
		gsf_free(name_buf);
		return 0;
	}
	self->typings.count++;
	typing->null_tag = 0;
	typing->manager = self;
	typing->hash = hash;
	typing->textual = name_buf;
	li = self->typings.buckets[bucket];
	if (!li) {
		self->typings.buckets[bucket] = typing;
	} else {
		while (li->next) li = li->next;
		li->next = typing;
	}
	return typing;
}


gsf_type* gsf_itm_add_class(gsf_itm* self, const gsf_class* class) {
	const char* name = gsf_class_get_name(class);
	if (islower(name[0])) {
		// TODO include name in error
		GSF_RAISE(gsf_ebadname, "Class names must start with an uppercase letter.");
	}
	gsf_type* type = gsf_itm_put_type(self, name, strlen(name));
	if (!type) return 0;
	type->category = obj;
	type->spec.obj = class;
	return type;
}
gsf_type* gsf_itm_add_enum(gsf_itm* self, const gsf_enum* enum_) {
	const char* name = gsf_enum_get_name(enum_);
	if (islower(name[0])) {
		// TODO include name in error
		GSF_RAISE(gsf_ebadname, "Enum names must start with an uppercase letter.");
	}
	gsf_type* type = gsf_itm_put_type(self, name, strlen(name));
	if (!type) return 0;
	type->category = enu;
	type->spec.enu = enum_;
	return type;
}


static const gsf_type* gsf_itm_find_type_buf(gsf_itm* self, const char* buf, size_t buf_size) {
	uint32_t hash;
	if (!gsf_itm_hash_notation(self, buf, buf_size, &hash)) return 0;
	uint8_t bucket = hash % GSF_ARRAY_LEN(self->types.buckets);
	gsf_type* li = self->types.buckets[bucket];
	while (li) {
		if (li->hash == hash) break;
		li = li->next;
	}
	return li;
}
static const gsf_type* gsf_itm_find_type(gsf_itm* self, const char* name) {
	return gsf_itm_find_type_buf(self, name, strlen(name));
}
const gsf_type* gsf_itm_cast_type(gsf_itm* self, const gsf_type* type) {
	const gsf_type* found = gsf_itm_find_type(self, type->textual);
	if (found) {
		// TODO check if class/enum matches (otherwise error – different class with same name)
		return found;
	}
	gsf_type* result;
	if (type->category == obj || type->category == enu) GSF_RAISE(gsf_eusupptype, type->textual);
	if (type->category != col) {
		result = gsf_itm_put_type(self, type->textual, strlen(type->textual));
		if (!result) return 0;
		result->category = result->category;
		result->spec = type->spec;
	} else if (type->spec.col.collection == arr) {
		const gsf_type* elem_type = gsf_itm_cast_type(self, type->spec.col.typ.e);
		if (!elem_type) return 0;
		result = gsf_itm_put_type(self, type->textual, strlen(type->textual));
		if (!result) return 0;
		result->category = col;
		result->spec.col.collection = type->spec.col.collection;
		result->spec.col.typ.e = elem_type;
	} else { // list or map
		const gsf_typing* elem_typing = gsf_itm_cast_typing(self, type->spec.col.typ.ing);
		if (!elem_typing) return 0;
		result = gsf_itm_put_type(self, type->textual, strlen(type->textual));
		if (!result) return 0;
		result->category = col;
		result->spec.col.collection = type->spec.col.collection;
		result->spec.col.typ.ing = elem_typing;
	}
	return result;
}
const gsf_type* gsf_itm_get_type(gsf_itm* self, const gsf_type_or_char* spec) {
	if (((const char*) spec)[0] == 0) { // is a type
		return gsf_itm_cast_type(self, (const gsf_type*) spec);
	} else { // is a string
		return gsf_itm_str_to_type(self, (const char*) spec);
	}
}
static const gsf_typing* gsf_itm_find_typing_buf(gsf_itm* self, const char* buf, size_t buf_size) {
	uint32_t hash;
	if (!gsf_itm_hash_notation(self, buf, buf_size, &hash)) return 0;
	uint8_t bucket = hash % GSF_ARRAY_LEN(self->typings.buckets);
	gsf_typing* li = self->typings.buckets[bucket];
	while (li) {
		if (li->hash == hash) break;
		li = li->next;
	}
	return li;
}
static const gsf_typing* gsf_itm_find_typing(gsf_itm* self, const char* name) {
	return gsf_itm_find_typing_buf(self, name, strlen(name));
}
const gsf_typing* gsf_itm_cast_typing(gsf_itm* self, const gsf_typing* typing) {
	const gsf_typing* found = gsf_itm_find_typing(self, typing->textual);
	if (found) {
		// TODO check if classes and enums match
		return found;
	}

	if (gsf_typing_is_any(typing)) {
		gsf_typing* result = gsf_itm_put_typing(self, "?", 1);
		if (!result) return 0;
		result->types_size = 0;
		result->types_count = 0;
		result->types = 0;
		result->default_type = 0;
		return result;
	}

	const gsf_type** types = gsf_alloc(typing->types_count * sizeof(void*));
	if (!types) return 0;
	const gsf_type* default_type = 0;
	for (size_t i = 0; i < typing->types_count; i++) {
		const gsf_type* type = gsf_itm_cast_type(self, typing->types[i]);
		if (!type) {
			gsf_free(types);
			return 0;
		}
		if (typing->types[i] == typing->default_type) {
			default_type = type;
		}
		types[i] = type;
	}

	gsf_typing* result = gsf_itm_put_typing(self, typing->textual, strlen(typing->textual));
	if (!result) {
		gsf_free(types);
		return 0;
	}
	result->types_size = typing->types_count;
	result->types_count = typing->types_count;
	result->types = types;
	result->default_type = default_type;
	return result;
}
const gsf_typing* gsf_itm_get_typing(gsf_itm* self, const gsf_typing_or_char* spec) {
	if (((const char*) spec)[0] == 0) { // is a typing
		return gsf_itm_cast_typing(self, (const gsf_typing*) spec);
	} else { // is a string
		return gsf_itm_str_to_typing(self, (const char*) spec);
	}
}

static int strntcmp(const char* a, const char* b, size_t n) {
	size_t bn = strlen(b);
	return strncmp(a, b, n < bn ? n : bn);
}

const gsf_type* gsf_itm_str_buf_to_type(gsf_itm* self, const char* buf, size_t buf_size) {
	const gsf_type* type = gsf_itm_find_type_buf(self, buf, buf_size);
	if (type) return type;

	while (isblank(buf[0])) {
		buf++;
		buf_size--;
	}
	while (isblank(buf[buf_size - 1])) {
		buf_size--;
	}

	gsf_type tmp;
	#define GSF_RAISE_FMT() GSF_RAISEF(gsf_etypefmt, "%.*s", buf_size, buf)
	if (buf_size == 0) GSF_RAISE(gsf_etypefmt, "(empty type)");
	if (isupper(buf[0])) GSF_RAISEF(gsf_eusupptype, "%.*s", buf_size, buf);
	if (buf[0] == 'n') { // null
		tmp.category = nul;
		if (
			buf_size < 3 || buf_size > 4 ||
			strntcmp(buf, "nul", buf_size) != 0 && strntcmp(buf, "null", buf_size) != 0
		) GSF_RAISE_FMT();
	} else if (buf[0] == 'i') { // ints
		tmp.category = pri;
		if (buf_size == 2) {
			if (buf[1] == '8') tmp.spec.pri = i8;
			else GSF_RAISE_FMT();
		} else if (buf_size == 3) {
			if (buf[1] == '0' && buf[2] == '8') tmp.spec.pri = i8;
			else if (buf[1] == '1' && buf[2] == '6') tmp.spec.pri = i16;
			else if (buf[1] == '3' && buf[2] == '2') tmp.spec.pri = i32;
			else if (buf[1] == '6' && buf[2] == '4') tmp.spec.pri = i64;
			else GSF_RAISE_FMT();
		} else GSF_RAISE_FMT();
	} else if (buf[0] == 'u') { // uints
		tmp.category = pri;
		if (buf_size == 2) {
			if (buf[1] == '8') tmp.spec.pri = u8;
			else GSF_RAISE_FMT();
		} else if (buf_size == 3) {
			if (buf[1] == '0' && buf[2] == '8') tmp.spec.pri = u8;
			else if (buf[1] == '1' && buf[2] == '6') tmp.spec.pri = u16;
			else if (buf[1] == '3' && buf[2] == '2') tmp.spec.pri = u32;
			else if (buf[1] == '6' && buf[2] == '4') tmp.spec.pri = u64;
			else GSF_RAISE_FMT();
		} else GSF_RAISE_FMT();
	} else if (buf[0] == 'f') { // floats
		tmp.category = pri;
		if (buf_size != 3) GSF_RAISE_FMT();
		if (buf[1] == '3' && buf[2] == '2') tmp.spec.pri = f32;
		else if (buf[1] == '6' && buf[2] == '4') tmp.spec.pri = f64;
	} else if (buf[0] == 's') { // string
		tmp.category = pri;
		tmp.spec.pri = str;
		if (buf_size != 3 || strntcmp(buf, "str", buf_size) != 0) GSF_RAISE_FMT();
	} else { // collection
		tmp.category = col;
		const char* ptr = buf;
		if (strntcmp(ptr, "arr", buf_size) == 0) {
			ptr += 3;
		} else if (strntcmp(ptr, "array", buf_size) == 0) {
			ptr += 5;
		} else if (strntcmp(ptr, "lst", buf_size) == 0) {
			ptr += 3;
		} else if (strntcmp(ptr, "list", buf_size) == 0) {
			ptr += 4;
		} else if (strntcmp(ptr, "map", buf_size) == 0) {
			ptr += 3;
		}
		if (buf_size - (ptr - buf) < 1) GSF_RAISE(gsf_eshortbuf);
		while (isblank(*ptr)) {
			ptr++;
			if (buf_size - (ptr - buf) < 1) GSF_RAISE(gsf_eshortbuf);
		}
		if (*ptr++ != '[') GSF_RAISE_FMT(/*"Collection type '%.*s' does not open element sepecification."*/);
		const char* end = buf + buf_size - 1;
		if (*end != ']') GSF_RAISE_FMT(/*"Collection type '%.*s' does not close element sepecification."*/);
		if (buf[0] == 'a') {
			tmp.spec.col.collection = arr;
			const gsf_type* element = gsf_itm_str_buf_to_type(self, ptr, end - ptr);
			if (!element) return 0;
			tmp.spec.col.typ.e = element;
		} else if (buf[0] == 'l') { // list
			tmp.spec.col.collection = lst;
			const gsf_typing* element = gsf_itm_str_buf_to_typing(self, ptr, end - ptr);
			if (!element) return 0;
			tmp.spec.col.typ.ing = element;
		} else if (buf[0] == 'm') { // map
			tmp.spec.col.collection = map;
			const gsf_typing* element = gsf_itm_str_buf_to_typing(self, ptr, end - ptr);
			if (!element) return 0;
			tmp.spec.col.typ.ing = element;
		} else GSF_RAISE_FMT();
	}
	#undef GSF_RAISE_FMT

	gsf_type* result = gsf_itm_put_type(self, buf, buf_size);
	result->category = tmp.category;
	result->spec = tmp.spec;
	return result;
}
const gsf_type* gsf_itm_str_to_type(gsf_itm* self, const char* string) {
	return gsf_itm_str_buf_to_type(self, string, strlen(string));
}
const gsf_typing* gsf_itm_str_buf_to_typing(gsf_itm* self, const char* buf, size_t buf_size) {
	gsf_errno_reset();
	const gsf_typing* found = gsf_itm_find_typing_buf(self, buf, buf_size);
	if (gsf_errno) return 0;
	if (found) return found;

	while (isblank(buf[0])) {
		buf++;
		buf_size--;
	}
	while (isblank(buf[buf_size - 1])) {
		buf_size--;
	}

	if (buf_size == 1 && *buf == '?') {
		gsf_typing* result = gsf_itm_put_typing(self, buf, buf_size);
		if (!result) return 0;
		result->types_size = 0;
		result->types_count = 0;
		result->types = 0;
		result->default_type = 0;
		return result;
	}

	size_t count = 1, depth = 0;
	const char* ptr = buf;
	while (ptr - buf < buf_size) {
		if (*ptr == '[') {
			depth++;
		} else if (*ptr == ']') {
			depth--;
		} else if (depth == 0 && *ptr == '|') {
			count++;
		}
		ptr++;
	}

	const gsf_type** types = gsf_alloc(count * sizeof(void*));
	if (!types) GSF_RAISE(gsf_emem);
	size_t default_idx = -1;
	count = 0;
	ptr = buf;
	const char* start = ptr;
	while (ptr - buf < buf_size) {
		if (*ptr == '[') {
			depth++;
		} else if (*ptr == ']') {
			depth--;
		} else if (depth == 0 && *ptr == '|') {
			if (*start == '!') {
				default_idx = count;
				start++;
			}
			types[count] = gsf_itm_str_buf_to_type(self, start, ptr - start);
			if (!types[count]) {
				gsf_free(types);
				return 0;
			}
			count++;
			start = ptr + 1;
		}
		ptr++;
	}
	if (*start == '!') {
		default_idx = count;
		start++;
	}
	types[count] = gsf_itm_str_buf_to_type(self, start, ptr - start);
	if (!types[count]) {
		gsf_free(types);
		return 0;
	}
	count++;
	if (count == 1) default_idx = 0;

	gsf_typing* typing = gsf_itm_put_typing(self, buf, buf_size);
	if (!typing) {
		gsf_free(types);
		return 0;
	}
	typing->types_size = count;
	typing->types_count = count;
	typing->types = types;
	if (default_idx != (size_t) -1) {
		typing->default_type = types[default_idx];
	} else {
		typing->default_type = 0;
	}
	return typing;
}
const gsf_typing* gsf_itm_str_to_typing(gsf_itm* self, const char* string) {
	return gsf_itm_str_buf_to_typing(self, string, strlen(string));
}

char* gsf_itm_type_to_str_buf(gsf_itm* self, const gsf_type* type, char* buf, size_t buf_size) {
	size_t len = strlen(type->textual);
	if (len > buf_size) GSF_RAISE(gsf_eshortbuf);
	memcpy(buf, type->textual, len);
	return buf + len;
}
const char* gsf_itm_type_to_str(gsf_itm* self, const gsf_type* type) {
	return type->textual;
}
char* gsf_itm_typing_to_str_buf(gsf_itm* self, const gsf_typing* typing, char* buf, size_t buf_size) {
	size_t len = strlen(typing->textual);
	if (len > buf_size) GSF_RAISE(gsf_eshortbuf);
	memcpy(buf, typing->textual, len);
	return buf + len;
}
const char* gsf_itm_typing_to_str(gsf_itm* self, const gsf_typing* typing) {
	return typing->textual;
}


const gsf_typing* gsf_typing_make(const gsf_type* type) {
	// textual representation of a one-type typing is the same as of its type
	return gsf_itm_get_typing(type->manager, type->textual);
}
const gsf_type* gsf_type_make_array(const gsf_type* elems) {
	char* textual = gsf_alloc(strlen(elems->textual) + 5 + 1);
	if (!textual) GSF_RAISE(gsf_emem);
	strcpy(textual, "arr[");
	strcpy(textual + 4, elems->textual);
	strcpy(textual + 4 + strlen(elems->textual), "]");
	const gsf_type* result = gsf_itm_str_to_type(elems->manager, textual);
	gsf_free(textual);
	return result;
}
const gsf_type* gsf_type_make_list(const gsf_typing* elems) {
	char* textual = gsf_alloc(strlen(elems->textual) + 5 + 1);
	if (!textual) GSF_RAISE(gsf_emem);
	strcpy(textual, "lst[");
	strcpy(textual + 4, elems->textual);
	strcpy(textual + 4 + strlen(elems->textual), "]");
	const gsf_type* result = gsf_itm_str_to_type(elems->manager, textual);
	gsf_free(textual);
	return result;
}
const gsf_type* gsf_type_make_map(const gsf_typing* elems) {
	char* textual = gsf_alloc(strlen(elems->textual) + 5 + 1);
	if (!textual) GSF_RAISE(gsf_emem);
	strcpy(textual, "map[");
	strcpy(textual + 4, elems->textual);
	strcpy(textual + 4 + strlen(elems->textual), "]");
	const gsf_type* result = gsf_itm_str_to_type(elems->manager, textual);
	gsf_free(textual);
	return result;
}

const gsf_typing* gsf_itm_typing_any(gsf_itm* self) {
	return gsf_itm_str_to_typing(self, "?");
}
const gsf_typing* gsf_itm_typing_make(gsf_itm* self, const gsf_type_or_char* type) {
	type = gsf_itm_get_type(self, type);
	if (!type) return 0;
	return gsf_typing_make(type);
}
const gsf_type* gsf_itm_type_make_array(gsf_itm* self, const gsf_type_or_char* elems) {
	elems = gsf_itm_get_type(self, elems);
	if (!elems) return 0;
	return gsf_type_make_array(elems);
}
const gsf_type* gsf_itm_type_make_list(gsf_itm* self, const gsf_typing_or_char* elems) {
	elems = gsf_itm_get_typing(self, elems);
	if (!elems) return 0;
	return gsf_type_make_list(elems);
}
const gsf_type* gsf_itm_type_make_map(gsf_itm* self, const gsf_typing_or_char* elems) {
	elems = gsf_itm_get_typing(self, elems);
	if (!elems) return 0;
	return gsf_type_make_map(elems);
}

const gsf_typing* gsf_typing_with_type(const gsf_typing* self,
		const gsf_type_or_char* type_spec, int is_default) {
	if (gsf_typing_is_any(self)) GSF_RAISE(gsf_ewrongtype, "Type can only be added to typings other than '?' (any).");

	if (is_default) is_default = 1; // used for calculations
	if (is_default) self = gsf_typing_without_default(self);

	size_t self_len = strlen(self->textual) - 1;
	if (self->textual[self_len - 1] != ']') {
		GSF_RAISEF(gsf_eassert, "Typing's last char is not ']' (string: %s).", self->textual);
	}
	const gsf_type* type = gsf_itm_get_type(self->manager, type_spec);
	size_t len = self_len + strlen(type->textual) + 1 + is_default; // + '|' + '!'

	char* textual = gsf_alloc(len);
	if (!textual) GSF_RAISE(gsf_emem);
	memcpy(textual, self->textual, self_len - 1);
	textual[self_len - 1] = '|';
	if (is_default) textual[self_len] = '!';
	strcpy(textual + self_len + is_default, type->textual);
	textual[len - 1] = ']';
	const gsf_typing* result = gsf_itm_str_buf_to_typing(self->manager, textual, len);
	gsf_free(textual);
	return result;
}
// TODO any with default
const gsf_typing* gsf_typing_with_default(const gsf_typing* self, size_t index) {
	size_t len = strlen(self->textual);
	if (self->textual[len - 1] != ']') {
		GSF_RAISE(gsf_eassert, "Typing's last char is not ']'.");
	}

	if (!self->default_type) len += 1; // + '!'
	char* textual = gsf_alloc(len + 1); // + '\0'
	if (!textual) return 0;
	char* ptr = textual;
	for (size_t i = 0; i < self->types_count; i++) {
		if (i == index) *ptr++ = '!';
		size_t type_len = strlen(self->types[i]->textual);
		memcpy(ptr, self->types[i]->textual, type_len);
		ptr += type_len;
		*ptr++ = '|';
	}
	*--ptr = '\0'; // swap last '|' for a '\0'
	return gsf_itm_str_to_typing(self->manager, textual);
}
const gsf_typing* gsf_typing_without_default(const gsf_typing* self) {
	if (!self->default_type) return self;
	size_t len = strlen(self->textual);
	if (self->textual[len - 1] != ']') {
		GSF_RAISE(gsf_eassert, "Typing's last char is not ']'.");
	}

	len -= 1; // - '!'
	char* textual = gsf_alloc(len);
	if (!textual) return 0;
	const char* src_ptr = self->textual;
	char* dst_ptr = textual;
	size_t depth = 0;
	while (*src_ptr != '\0') {
		if (*src_ptr == '[') depth++;
		else if (*src_ptr == ']') depth--;
		else if (depth == 0 && *src_ptr == '!') continue;
		*dst_ptr++ = *src_ptr++;
	}
	const gsf_typing* result = gsf_itm_str_buf_to_typing(self->manager, textual, len);
	gsf_free(textual);
	return result;
}
