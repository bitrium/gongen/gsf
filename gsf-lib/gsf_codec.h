#pragma once

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

// blended strings - a feature primarly meant for schemas (not recommended elsewhere):
// this representation is a mixture of one-line and multi-line string representations
// (first line is right after value, rest is a regular string block)

#define GSF_TXT_READ_COMMON_BYTES				0b000011110000
#define GSF_TXT_READ_NO_COMMENTS				0b000000100000
#define GSF_TXT_READ_TWO_SPACE_INDENTS			0b000000000000 // default
#define GSF_TXT_READ_FOUR_SPACE_INDENTS			0b000001000000
#define GSF_TXT_READ_TAB_INDENTS				0b000010000000
#define GSF_TXT_READ_AUTO_INDENTS				0b000011000000

#define GSF_TXT_READ_SCHEMA_BYTES				0b000000000000

#define GSF_TXT_READ_ARTICLE_BYTES				0b000000001111
#define GSF_TXT_READ_ARTICLE_STRING_BLEND		0b000000000001

#define GSF_TXT_READ_BYTES (		\
	GSF_TXT_READ_COMMON_BYTES |		\
	GSF_TXT_READ_SCHEMA_BYTES |		\
	GSF_TXT_READ_ARTICLE_BYTES		\
)

#define GSF_TXT_WRITE_COMMON_BYTES				0b001111000000
#define GSF_TXT_WRITE_SIGNATURE					0b000001000000
#define GSF_TXT_WRITE_COMMENTS					0b000010000000
#define GSF_TXT_WRITE_NO_HEADER_NEWLINE			0b000100000000
#define GSF_TXT_WRITE_LEADING_ZERO				0b001000000000

#define GSF_TXT_WRITE_SCHEMA_BYTES				0b110000000000
#define GSF_TXT_WRITE_SCHEMA_NO_LABEL_DOC		0b010000000000

#define GSF_TXT_WRITE_ARTICLE_BYTES				0b000000111111
#define GSF_TXT_WRITE_ARTICLE_STRING_BLEND		0b000000000001
#define GSF_TXT_WRITE_ARTICLE_NO_DESCRIPTOR		0b000000000010
#define GSF_TXT_WRITE_ARTICLE_STRONG_TYPES_ALL	0b000000001100
#define GSF_TXT_WRITE_ARTICLE_STRONG_ROOT		0b000000000100
#define GSF_TXT_WRITE_ARTICLE_STRONG_TYPES		0b000000001000
// TODO flag for skipping values that match the default

#define GSF_TXT_WRITE_BYTES (		\
	GSF_TXT_WRITE_COMMON_BYTES |	\
	GSF_TXT_WRITE_SCHEMA_BYTES |	\
	GSF_TXT_WRITE_ARTICLE_BYTES		\
)

typedef struct gsf_context gsf_context;
typedef struct gsf_descriptor gsf_descriptor;
typedef struct gsf_schema gsf_schema;
typedef struct gsf_article gsf_article;

enum gsf_codec_format {
	GSF_CODEC_TEXTUAL,
	GSF_CODEC_BINARY,
};

struct gsf_reader {
	/**
	 * @brief Format of GSF data to decode.
	 */
	enum gsf_codec_format format;
	/**
	 * @brief Read function flags.
	 */
	int flags;

	/**
	 * @brief Make bytes after ptr available for reading.
	 * @note May change ptr but keeps it in the same position of read data.
	 * @param self The reader.
	 * @param count Requested of amount of bytes after ptr to be readable.
	 * @return Zero on error; negative on EOF / no more bytes; positive on success.
	 */
	int (*read)(struct gsf_reader* self, size_t count);

	/**
	 * @brief Reader's user data.
	 */
	void* user;
	/**
	 * @brief Current position. Can be freely advanced.
	 */
	const void* ptr;
	/**
	 * @brief End of currently available space.
	 */
	const void* end;
};
struct gsf_writer {
	/**
	 * @brief Format of GSF data to encode.
	 */
	enum gsf_codec_format format;
	/**
	 * @brief Write function flags.
	 */
	int flags;

	/**
	 * @brief Make bytes after ptr available for writing.
	 * @note May change ptr but keeps it in the same position of written data.
	 * @param self The writer.
	 * @param count Requested of amount of bytes after ptr to be writeable.
	 * @return Zero on error; negative on EOF / no more bytes; positive on success.
	 */
	int (*write)(struct gsf_writer* self, size_t count);

	/**
	 * @brief Writer's user data. Should be left untouched outside of its implementation.
	 */
	void* user;
	/**
	 * @brief Current position. Can be freely advanced.
	 */
	void* ptr;
	/**
	 * @brief End of currently available space.
	 */
	const void* end;
};

int gsf_reader_create_buf(struct gsf_reader* reader, const void* buf, size_t size);
void gsf_reader_delete_buf(struct gsf_reader* reader);
int gsf_writer_create_buf(struct gsf_writer* writer, void* buf, size_t size);
void gsf_writer_delete_buf(struct gsf_writer* writer);
int gsf_writer_create_growing_buf(struct gsf_writer* writer, void** buf, size_t initial_size);
void gsf_writer_delete_growing_buf(struct gsf_writer* writer);

int gsf_read_schema(struct gsf_reader* reader, gsf_schema* schema, gsf_descriptor* desc);
int gsf_read_schema_to_ctx(struct gsf_reader* reader, gsf_context* ctx);
int gsf_read_article_desc(struct gsf_reader* reader, gsf_descriptor* desc); // only desc
int gsf_read_article_proper(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema); // no desc
int gsf_read_article(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema); // desc + article
int gsf_read_article_ctx(struct gsf_reader* reader, gsf_article* article, const gsf_context* ctx); // desc + article

int gsf_write_schema(struct gsf_writer* writer, const gsf_schema* schema);
int gsf_write_article(struct gsf_writer* writer, const gsf_article* article);

#ifdef __cplusplus
} // extern "C"
#endif
