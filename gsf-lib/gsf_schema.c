#include "gsf_data.h"

#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include "gsf_err.h"
#include "gsf_context.h"
#include "gsf_itm.h"
#include "gsf_util.c"

extern gsf_value* gsf_create_value(const gsf_typing* typing);
extern void gsf_destroy_value(gsf_value* value);


struct gsf_label {
	// const gsf_context* context;
	gsf_value* default_val;
	char* key;
	const gsf_typing* typing;
	char* doc;
};
void gsf_destroy_label(gsf_label* label);

struct gsf_class {
	const gsf_type* type;
	char* name;
	size_t labels_size;
	size_t labels_count;
	gsf_label* labels;
};
void gsf_destroy_class(gsf_class* class);

struct gsf_variant {
	char* name;
	char* doc;
};
void gsf_destroy_variant(gsf_variant* variant);

struct gsf_enum {
	const gsf_type* type;
	char* name;
	size_t variants_size;
	size_t variants_count;
	gsf_variant* variants;
};
void gsf_destroy_enum(gsf_enum* self);

struct gsf_schema {
	const gsf_descriptor* descriptor;
	bool is_single;
	union gsf_context_or_itm {
		gsf_context* full;
		gsf_itm* single;
	} ctx;
	gsf_itm* itm;
	const char* root_typing_draft;
	const gsf_typing* root_typing;
	size_t classes_size;
	size_t classes_count;
	gsf_class* classes;
	size_t enums_count;
	size_t enums_size;
	gsf_enum* enums;
};

gsf_schema* gsf_create_schema(void) {
	gsf_schema* schema = gsf_alloc(sizeof(gsf_schema));
	// amount > len if uninitialized
	schema->classes_count = -1;
	schema->classes_size = 0;
	if (!schema) GSF_RAISE(gsf_emem);
	return schema;
}

void gsf_delete_schema(gsf_schema* schema) {
	if (!schema) return;
	gsf_schema_destroy(schema);
	gsf_free(schema);
}

static gsf_schema* gsf_schema_init_generic(gsf_schema* schema,
		const gsf_descriptor* descriptor, const gsf_typing_or_char* root,
		size_t classes_count, bool single, gsf_itm* itm, union gsf_context_or_itm ctx) {
	if (!schema) GSF_RAISE(gsf_enullptr);

	gsf_class* classes = gsf_calloc(classes_count * sizeof(gsf_class));
	if (!classes) GSF_RAISE(gsf_emem);

	const gsf_typing* root_typing = 0;
	if (root) {
		gsf_errno_reset();
		root_typing = gsf_itm_get_typing(itm, root);
		if (!root_typing) {
			if (gsf_errno == gsf_eusupptype) {
				// class or enum fail - ignore and keep root as draft
				gsf_errno_reset();
			} else {
				// other error - rethrow
				gsf_free(classes);
				return 0;
			}
		}
	}
	if (root_typing) root = 0;

	schema->descriptor = descriptor;
	schema->is_single = single;
	schema->ctx = ctx;
	schema->itm = itm;
	schema->root_typing_draft = root;
	schema->root_typing = root_typing;
	schema->classes_size = classes_count;
	schema->classes_count = 0;
	schema->classes = classes;
	schema->enums_size = 0;
	schema->enums_count = 0;
	schema->enums = 0;
	return schema;
}
gsf_schema* gsf_schema_init_ctx(gsf_schema* schema, const gsf_descriptor* descriptor,
		const gsf_typing_or_char* root, size_t classes_count,
		gsf_context* context, gsf_itm* itm) {
	union gsf_context_or_itm ctx = { .full = context };
	return gsf_schema_init_generic(schema, descriptor, root, classes_count, false, itm, ctx);
}
gsf_schema* gsf_schema_init(gsf_schema* schema, const gsf_descriptor* descriptor,
		const gsf_typing_or_char* root, size_t classes_count) {
	union gsf_context_or_itm ctx = { .single = gsf_create_itm() };
	if (!ctx.single || !gsf_itm_init(ctx.single)) {
		gsf_delete_itm(ctx.single);
		return 0;
	}
	if (!gsf_schema_init_generic(schema, descriptor, root, classes_count, true, ctx.single, ctx)) {
		gsf_delete_itm(ctx.single);
		return 0;
	}
	return schema;
}
void gsf_schema_destroy(gsf_schema* schema) {
	if (!schema) return;
	if (schema->classes_count > schema->classes_size) return;
	for (gsf_class* class = schema->classes;
			class - schema->classes < schema->classes_count; class++) {
		gsf_destroy_class(class);
	}
	gsf_free(schema->classes);
	schema->classes_count = -1;
	schema->classes_size = 0;
	for (gsf_enum* enum_ = schema->enums;
			enum_ - schema->enums < schema->enums_count; enum_++) {
		gsf_destroy_enum(enum_);
	}
	gsf_free(schema->enums);
	schema->enums_count = -1;
	schema->enums_size = 0;
	if (schema->is_single) {
		gsf_delete_itm(schema->itm);
	}
}

gsf_schema* gsf_schema_new(const gsf_descriptor* descriptor,
						   const gsf_typing_or_char* root, size_t classes_count) {
	gsf_schema* schema = gsf_create_schema();
	if (schema) gsf_schema_init(schema, descriptor, root, classes_count);
	if (!schema) gsf_delete_schema(schema);
	return schema;
}

gsf_schema* (gsf_schema_verify)(gsf_schema* schema) {
	// TODO schema verification
	return schema;
}

const gsf_descriptor* gsf_schema_get_descriptor(const gsf_schema* schema) {
	return schema->descriptor;
}

void gsf_schema_set_descriptor(gsf_schema* schema, const gsf_descriptor* descriptor) {
	schema->descriptor = descriptor;
}

const gsf_context* gsf_schema_get_context(const gsf_schema* schema) {
	if (schema->is_single) return 0;
	return schema->ctx.full;
}

void gsf_schema_update_root_draft(gsf_schema* schema) {
	if (!schema->root_typing_draft) return;
	schema->root_typing = gsf_itm_str_to_typing(schema->itm, schema->root_typing_draft);
	if (schema->root_typing) schema->root_typing_draft = 0;
	gsf_errno_reset();
}

const gsf_typing* gsf_schema_get_root(const gsf_schema* schema) {
	return schema->root_typing;
}

gsf_schema* gsf_schema_set_root(gsf_schema* schema, const gsf_typing_or_char* typing) {
	schema->root_typing = gsf_itm_get_typing(schema->itm, typing);
	if (!schema->root_typing) return 0;
	schema->root_typing_draft = 0;
	return schema;
}

size_t gsf_schema_class_count(const gsf_schema* schema) {
	return schema->classes_count;
}

gsf_class* gsf_schema_add_class(gsf_schema* schema, const char* name,
								size_t labels_count) {
	if (schema->classes_count == schema->classes_size) {
		schema->classes_size++;
		gsf_class* classes = gsf_realloc(
			schema->classes, sizeof(gsf_class) * schema->classes_size
		);
		if (!classes) {
			schema->classes_size--;
			GSF_RAISE(gsf_emem);
		}
		schema->classes = classes;
	}

	gsf_class* class = schema->classes + schema->classes_count;
	class->name = gsf_alloc(strlen(name) + 1);
	class->labels = gsf_calloc(labels_count * sizeof(gsf_label));
	if (!class->labels || !class->name) {
		gsf_free(class->name);
		gsf_free(class->labels);
		GSF_RAISE(gsf_emem);
	}
	strcpy(class->name, name);
	class->type = gsf_itm_add_class(schema->itm, class);
	if (!class->type) {
		gsf_free(class->name);
		gsf_free(class->labels);
		return 0;
	}

	class->labels_count = 0;
	class->labels_size = labels_count;
	schema->classes_count++;
	gsf_schema_update_root_draft(schema);
	return class;
}

gsf_class* (gsf_schema_get_class)(const gsf_schema* schema, size_t index) {
	if (index >= schema->classes_count) return 0;
	return schema->classes + index;
}

// void gsf_schema_remove_class(gsf_schema* schema, size_t index) {
// 	if (index >= schema->classes_count) return;
// 	gsf_class* class = schema->classes + index;
// 	gsf_destroy_class(class);
// 	schema->classes_count--;
// 	memmove(
// 		class, class + 1,
// 		sizeof(gsf_class) * (schema->classes_count - index)
// 	);
// 	// TODO remove from context?
// }

size_t gsf_schema_enum_count(const gsf_schema* schema) {
	return schema->enums_count;
}
gsf_enum* gsf_schema_add_enum(gsf_schema* schema, const char* name, size_t variants_count) {
	if (schema->enums_size == 0) {
		schema->enums = gsf_alloc(sizeof(gsf_enum));
		if (!schema->enums) GSF_RAISE(gsf_emem);
		schema->enums_size = 1;
	} else if (schema->enums_count == schema->enums_size) {
		gsf_enum* enums = gsf_realloc(schema->enums, (schema->enums_size + 1) * sizeof(gsf_enum));
		if (!enums) GSF_RAISE(gsf_emem);
		schema->enums = enums;
		schema->enums_size++;
	}

	gsf_enum* enum_ = schema->enums + schema->enums_count++;
	enum_->variants = gsf_alloc(variants_count * sizeof(gsf_variant));
	enum_->name = gsf_alloc(strlen(name) + 1);
	if (!enum_->variants || !enum_->name) {
		gsf_free(enum_->variants);
		gsf_free(enum_->name);
		GSF_RAISE(gsf_emem);
	}
	strcpy(enum_->name, name);
	enum_->type = gsf_itm_add_enum(schema->itm, enum_);
	if (!enum_->type) {
		gsf_free(enum_->variants);
		gsf_free(enum_->name);
		return 0;
	}

	enum_->variants_size = variants_count;
	enum_->variants_count = 0;
	gsf_schema_update_root_draft(schema);
	return enum_;
}
gsf_enum* (gsf_schema_get_enum)(const gsf_schema* schema, size_t index) {
	if (index >= schema->enums_count) return 0;
	return schema->enums + index;
}
// void gsf_schema_remove_enum(gsf_schema* schema, size_t index) {
// 	if (index >= schema->enums_count) return;
// 	gsf_enum* enum_ = schema->enums + index;
// 	gsf_destroy_enum(enum_);
// 	schema->enums_count--;
// 	memmove(enum_, enum_ + 1, (schema->enums_count - index) * sizeof(gsf_enum));
// 	// TODO remove from context
// }

void gsf_destroy_class(gsf_class* class) {
	for (gsf_label* label = class->labels;
			label - class->labels < class->labels_count; label++) {
		gsf_destroy_label(label);
	}
	gsf_free(class->name);
	gsf_free(class->labels);
}

const gsf_type* gsf_class_as_type(const gsf_class* class) {
	return class->type;
}

const char* gsf_class_get_name(const gsf_class* class) {
	return class->name;
}

gsf_class* gsf_class_set_name(gsf_class* class, const char* name) {
	char* new_name = gsf_realloc(class->name, strlen(name) + 1);
	if (!new_name) GSF_RAISE(gsf_emem);
	strcpy(new_name, name);
	class->name = new_name;
	return class;
}

size_t gsf_class_label_count(const gsf_class* class) {
	return class->labels_count;
}

gsf_label* gsf_class_add_label(gsf_class* class, const char* key,
							   const gsf_typing_or_char* typing) {
	const gsf_typing* label_typing = gsf_itm_get_typing(gsf_type_manager(class->type), typing);
	if (!label_typing) return 0;

	size_t key_len = strlen(key);
	if (key_len == 0) GSF_RAISE(gsf_ekeyempty);
	char* key_buf = gsf_alloc(key_len + 1);
	if (!key_buf) GSF_RAISE(gsf_emem);
	if (class->labels_count == class->labels_size) {
		class->labels_size++;
		gsf_label* labels = gsf_realloc(
				class->labels, class->labels_size * sizeof(gsf_label)
		);
		if (!labels) {
			class->labels_size--;
			gsf_free(key_buf);
			GSF_RAISE(gsf_emem);
		}
		class->labels = labels;
	}

	gsf_label* label = class->labels + class->labels_count;
	strcpy(key_buf, key);
	label->default_val = 0;
	label->key = key_buf;
	label->typing = label_typing;
	label->doc = 0;
	class->labels_count++;
	return label;
}

gsf_label* (gsf_class_get_label)(const gsf_class* class, size_t index) {
	if (index >= class->labels_count) return 0;
	return class->labels + index;
}

gsf_label* (gsf_class_get_label_at_key)(const gsf_class* class, const char* key) {
	size_t index = gsf_class_index_at_key(class, key);
	if (!index--) return 0;
	return class->labels + index;
}

size_t gsf_class_index_at_label(const gsf_class* class, const gsf_label* label) {
	ptrdiff_t diff = label - class->labels;
	if (diff < 0 || diff >= class->labels_count) return 0;
	return diff + 1;
}

size_t gsf_class_index_at_key(const gsf_class* class, const char* key) {
	for (size_t i = 0; i < class->labels_count; i++) {
		if (strcmp(class->labels[i].key, key) == 0) {
			return i + 1;
		}
	}
	return 0;
}

void gsf_class_remove_label(gsf_class* class, size_t index) {
	if (index >= class->labels_count) return;
	gsf_label* label = class->labels + index;
	gsf_destroy_label(label);
	class->labels_count--;
	memmove(
			label, label + 1,
			sizeof(gsf_label) * (class->labels_count - index)
	);
}

void gsf_destroy_label(gsf_label* label) {
	if (!label) return;
	gsf_free(label->key);
	gsf_destroy_value(label->default_val);
	gsf_free(label->default_val);
	gsf_free(label->doc);
}

const char* gsf_label_get_key(const gsf_label* label) {
	return label->key;
}

gsf_label* gsf_label_set_key(gsf_label* label, const char* key) {
	char* new_key = gsf_realloc(label->key, strlen(key) + 1);
	if (!new_key) GSF_RAISE(gsf_emem);
	strcpy(new_key, key);
	label->key = new_key;
	return label;
}

const gsf_typing* gsf_label_get_typing(const gsf_label* label) {
	return label->typing;
}

gsf_label* gsf_label_set_typing(gsf_label* label, const gsf_typing_or_char* typing) {
	const gsf_typing* new_typing = gsf_itm_get_typing(gsf_typing_manager(label->typing), typing);
	if (!new_typing) return 0;
	label->typing = new_typing;
	return label;
}

const gsf_value* gsf_label_get_default(const gsf_label* label) {
	if (!label->default_val) return 0;
	if (gsf_value_get_type(label->default_val) == 0) return 0;
	return label->default_val;
}

gsf_value* gsf_label_access_default(gsf_label* label) {
	if (!label->default_val) label->default_val = gsf_create_value(label->typing);
	return label->default_val;
}

const char* gsf_label_get_doc(const gsf_label* label) {
	return label->doc;
}

gsf_label* gsf_label_set_doc(gsf_label* label, const char* doc) {
	char* new_doc = gsf_realloc(label->doc, strlen(doc) + 1);
	if (!new_doc) GSF_RAISE(gsf_emem);
	strcpy(new_doc, doc);
	label->doc = new_doc;
	return label;
}

void gsf_destroy_enum(gsf_enum* self) {
	for (size_t i = 0; i < self->variants_count; i++) {
		gsf_destroy_variant(self->variants + i);
	}
	gsf_free(self->variants);
	gsf_free(self->name);
}

const gsf_type* gsf_enum_as_type(const gsf_enum* self) {
	return self->type;
}

const char* gsf_enum_get_name(const gsf_enum* self) {
	return self->name;
}

size_t gsf_enum_variant_count(const gsf_enum* self) {
	return self->variants_count;
}

gsf_variant* gsf_enum_add_variant(gsf_enum* self, const char* name) {
	if (self->variants_count == self->variants_size) {
		gsf_variant* variants = gsf_realloc(self->variants, (self->variants_size + 1) * sizeof(gsf_variant));
		if (!variants) GSF_RAISE(gsf_emem);
		self->variants = variants;
		self->variants_size++;
	}
	gsf_variant* variant = self->variants + self->variants_count;
	variant->name = gsf_alloc(strlen(name) + 1);
	if (!variant->name) GSF_RAISE(gsf_emem);
	strcpy(variant->name, name);
	variant->doc = 0;
	self->variants_count++;
	return variant;
}

void gsf_enum_remove_variant(gsf_enum* self, size_t index) {
	if (index >= self->variants_count) return;
	gsf_variant* ptr = self->variants + index;
	gsf_destroy_variant(ptr);
	self->variants_count--;
	memmove(ptr, ptr + 1, (self->variants_count - index) * sizeof(gsf_variant));
}

int gsf_enum_index_of(const gsf_enum* self, const gsf_variant* variant, size_t* index) {
	if (variant < self->variants || variant >= self->variants + self->variants_count) GSF_RAISE(gsf_euknenuvar);
	*index = variant - self->variants;
	return 1;
}

gsf_variant* gsf_enum_get_variant(const gsf_enum* self, size_t index) {
	if (index >= self->variants_count) return 0;
	return self->variants + index;
}

gsf_variant* gsf_enum_get_variant_by_name(const gsf_enum* self, const char* name) {
	for (size_t i = 0; i < self->variants_count; i++) {
		if (strcmp(self->variants[i].name, name) == 0) return self->variants + i;
	}
	return 0;
}

void gsf_destroy_variant(gsf_variant* self) {
	gsf_free(self->name);
	gsf_free(self->doc);
}

gsf_variant* gsf_variant_matches(gsf_variant* self, const char* string) {
	if (strcmp(string, self->name) == 0) return self;
	return 0;
}

const char* gsf_variant_get_name(const gsf_variant* self) {
	return self->name;
}

gsf_variant* gsf_variant_set_doc(gsf_variant* self, const char* doc) {
	gsf_free(self->doc);
	if (!doc) return self;
	self->doc = gsf_alloc(strlen(doc) + 1);
	if (!self->doc) GSF_RAISE(gsf_emem);
	strcpy(self->doc, doc);
	return self;
}

const char* gsf_variant_get_doc(const gsf_variant* self) {
	return self->doc;
}
