#include "gsf_context.h"

#include <memory.h>
#include <string.h>
#include "gsf_err.h"
#include "gsf_itm.h"
#include "gsf_data.h"
#include "gsf_util.c"

extern gsf_schema* gsf_schema_init_ctx(gsf_schema* schema, const gsf_descriptor* descriptor,
		const gsf_typing_or_char* root, size_t classes_count, gsf_context* context, gsf_itm* itm);


#define GSF_CTX_CHUNK 4

struct gsf_ctx_entry {
	gsf_descriptor* desc;
	gsf_schema* schema;
};

struct gsf_context {
	gsf_itm* types;
	size_t entries_size;
	size_t entries_count;
	struct gsf_ctx_entry* entries;
};

gsf_context* gsf_create_context(void) {
	gsf_context* ctx = gsf_alloc(sizeof(gsf_context));
	if (!ctx) GSF_RAISE(gsf_emem);
	ctx->types = 0;
	return ctx;
}

void gsf_delete_context(gsf_context* ctx) {
	gsf_context_destroy(ctx);
	gsf_free(ctx);
}

gsf_context* gsf_context_new(void) {
	gsf_context* context = gsf_create_context();
	if (context) gsf_context_init(context);
	if (!context) {
		gsf_delete_context(context);
		return 0;
	}
	return context;
}
gsf_context* gsf_context_init(gsf_context* self) {
	self->types = gsf_create_itm();
	if (!self->types) return 0;
	if (!gsf_itm_init(self->types)) {
		self->types = 0;
		return 0;
	}
	self->entries_count = 0;
	self->entries_size = GSF_CTX_CHUNK;
	self->entries = gsf_alloc(GSF_CTX_CHUNK * sizeof(struct gsf_ctx_entry));
	if (!self->entries) {
		self->types = 0;
		gsf_delete_itm(self->types);
		GSF_RAISE(gsf_emem);
	}
	return self;
}
void gsf_context_destroy(gsf_context* self) {
	if (!self->types) return;
	gsf_itm_destroy(self->types);
	for (size_t i = 0; i < self->entries_count; i++) {
		gsf_delete_schema(self->entries[i].schema);
		gsf_delete_descriptor(self->entries[i].desc);
	}
	gsf_free(self->entries);
}

int gsf_context_add_entry(gsf_context* self, gsf_schema** schema_out,
		gsf_descriptor** desc_out) {
	size_t new_size = (self->entries_count / GSF_CTX_CHUNK + 1) * GSF_CTX_CHUNK;
	if (new_size > self->entries_size) {
		struct gsf_ctx_entry* entries = gsf_realloc(self->entries, new_size * sizeof(struct gsf_ctx_entry));
		if (!entries) GSF_RAISE(gsf_emem);
		self->entries_size = new_size;
		self->entries = entries;
	}
	struct gsf_ctx_entry* entry = self->entries + self->entries_count;
	entry->desc = gsf_create_descriptor();
	if (!entry->desc) return 0;
	entry->schema = gsf_create_schema();
	if (!entry->desc) {
		gsf_delete_descriptor(entry->desc);
		return 0;
	}
	if (desc_out) *desc_out = entry->desc;
	if (schema_out) *schema_out = entry->schema;
	self->entries_count++;
	return 1;
}
gsf_schema* gsf_context_new_schema(gsf_context* self, const gsf_descriptor* desc_original,
		const gsf_typing_or_char* root, size_t class_amount) {
	gsf_schema* schema;
	gsf_descriptor* desc;
	if (!gsf_context_add_entry(self, &schema, &desc)) return 0;
	if (!gsf_descriptor_init(
		desc,
		gsf_descriptor_get_group(desc_original),
		gsf_descriptor_get_name(desc_original),
		gsf_descriptor_get_version(desc_original)
	)) return 0;
	if (!desc) return 0;
	if (!gsf_schema_init_ctx(schema, desc, root, class_amount, self, self->types)) {
		gsf_descriptor_destroy(desc);
		return 0;
	}
	return schema;
}
gsf_schema* gsf_context_new_schema_name(gsf_context* self, const char* desc_name,
		const gsf_typing_or_char* root, size_t class_amount) {
	gsf_descriptor* desc = gsf_create_descriptor();
	if (!desc || !gsf_descriptor_from_str(desc, desc_name)) return 0;
	gsf_schema* result = gsf_context_new_schema(self, desc, root, class_amount);
	gsf_delete_descriptor(desc);
	return result;
}
size_t gsf_context_schema_count(const gsf_context* self) {
	return self->entries_count;
}
gsf_schema* (gsf_context_get_schema_by_idx)(const gsf_context* self, size_t index) {
	if (index >= self->entries_count) return 0;
	if (!gsf_descriptor_get_name(self->entries[index].desc)) return 0; // desc not initialized
	return self->entries[index].schema;
}
gsf_schema* (gsf_context_get_schema)(const gsf_context* self, const gsf_descriptor* desc) {
	for (size_t i = 0; i < self->entries_count; i++) {
		if (!gsf_descriptor_get_name(self->entries[i].desc)) continue; // desc not initialized
		if (gsf_descriptor_match(self->entries[i].desc, desc)) return self->entries[i].schema;
	}
	return 0;
}
