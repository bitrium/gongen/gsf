/**
 * @file gsf_com.h
 * @brief Interfaces common to all other GSF headers.
 *
 * This header is included in every other GSF header and therefore there is no need to add
 * it manually.
 *
 * Adds necessary functions that allocate and free structures used in the library.
 *
 * Also adds a function for changing the allocator used by the library.
 */
#pragma once

#ifndef __cplusplus
#include "stdlib.h"
#else
#include <cstdlib>

extern "C" {
#endif

#define GSF_VERSION "dev"

typedef struct gsf_context gsf_context;
typedef struct gsf_type gsf_type;
typedef struct gsf_typing gsf_typing;
typedef struct gsf_descriptor gsf_descriptor;
typedef struct gsf_schema gsf_schema;
typedef struct gsf_article gsf_article;

/**
 * @brief Changes the allocation functions used internally by the GSF library.
 *
 * After changing the allocator every function of the library will use the new functions
 * for allocating, reallocating and deallocating any objects including the ones that are
 * not exposed from any functions.
 *
 * By default standard `malloc`, `realloc` and `free` are used.
 *
 * If your allocator only changes part of these functions, pass null pointers for the
 * remaining arguments.
 *
 * @param fun_alloc New regular allocation function or null pointer.
 * @param fun_realloc New reallocation function or null pointer.
 * @param fun_free New deallocation function or null pointer.
 */
void gsf_set_allocator(void* fun_alloc(size_t), void* fun_realloc(void*, size_t),
					   void fun_free(void*));


// TODO doc
gsf_context* gsf_create_context(void);

void gsf_delete_context(gsf_context* context);

/**
 * Allocates a Descriptor object.
 *
 * The Descriptor has to be freed later with @link gsf_delete_descriptor @endlink later.
 * For use in most functions it should be initialized first with either @link
 * gsf_descriptor_init @endlink) or some copying/parsing function.
 *
 * @return Created Descriptor.
 */
gsf_descriptor* gsf_create_descriptor(void);

/**
 * @brief Destroys and deletes a Schema Descriptor.
 *
 * Frees a Descriptor or does nothing on null pointer.
 *
 * @param descriptor Deleted Descriptor.
 */
void gsf_delete_descriptor(gsf_descriptor* descriptor);

/**
 * Allocates a Schema object.
 *
 * The Schema has to be freed later with @link gsf_delete_schema @endlink later.
 * For use in most functions it should be initialized first with either @link
 * gsf_schema_init @endlink) or some copying/parsing function.
 *
 * @return Created Schema.
 */
gsf_schema* gsf_create_schema(void);

/**
 * @brief Destroys and deletes a Schema.
 *
 * Frees a Schema or does nothing on null pointer.
 *
 * @param schema Deleted Schema.
 */
void gsf_delete_schema(gsf_schema* schema);

/**
 * Allocates an article object.
 *
 * The article has to be freed later with @link gsf_delete_article @endlink later.
 * For use in most functions it should be initialized first with either @link
 * gsf_article_init @endlink) or some copying/parsing function.
 *
 * @return Created article.
 */
gsf_article* gsf_create_article(void);

/**
 * @brief Destroys and deletes an article.
 *
 * Frees an article or does nothing on null pointer.
 *
 * @param article Deleted article.
 */
void gsf_delete_article(gsf_article* article);

#ifdef __cplusplus
} // extern "C"
#endif
