/**
 * @file gsf_data.h
 * @brief Interface for manipulating GSF data.
 *
 * Identifiers and documentation use terminology introduced in the GSF specification.
 */
#pragma once

#include "gsf_com.h"

#ifndef __cplusplus
#include <stdint.h>
#else
#include <cstdint>

#define class class_ // C++ reserves 'class'
extern "C" {
#endif

typedef void gsf_typing_or_char;
typedef void gsf_type_or_char;

typedef struct gsf_class gsf_class;
typedef struct gsf_label gsf_label;
typedef struct gsf_enum gsf_enum;
typedef struct gsf_variant gsf_variant;

typedef struct gsf_value gsf_value;
typedef struct gsf_object gsf_object;
typedef struct gsf_array gsf_array;
typedef struct gsf_list gsf_list;
typedef struct gsf_map gsf_map;

// TODO fix 'new' functions

/**
 * @brief Initializes a schema Descriptor.
 *
 * The Descriptor must be destroyed with @link gsf_descriptor_destroy @endlink or
 * @link gsf_delete_descriptor @endlink.
 *
 * All of the @c char* arguments are null-terminated strings. The contents of these
 * strings are copied to the Descriptor.
 *
 * @param descriptor Created Descriptor.
 * @param group Null-terminated group string.
 * @param name Null-terminated name string.
 * @param version Null-terminated version string.
 * @return The @a Descriptor or null pointer on error.
 */
gsf_descriptor* gsf_descriptor_init(gsf_descriptor* descriptor, const char* group,
									const char* name, const char* version);

/**
 * @brief Creates and initializes a schema Descriptor.
 *
 * The Descriptor must be freed with @link gsf_delete_descriptor @endlink.
 *
 * All of the @c char* arguments are null-terminated strings. The contents of these
 * strings are copied to the Descriptor.
 *
 * @param group Null-terminated group string.
 * @param name Null-terminated name string.
 * @param version Null-terminated version string.
 * @return Initialized Descriptor or null pointer on error.
 */
gsf_descriptor* gsf_descriptor_new(const char* group, const char* name,
								   const char* version);

/**
 * @brief Destroys a Descriptor.
 *
 * This only destroys the Descriptor internally. It still needs to be freed with @link
 * gsf_delete_descriptor @endlink.
 *
 * @param descriptor Destroyed Descriptor.
 */
void gsf_descriptor_destroy(gsf_descriptor* descriptor);

/**
 * @brief Initializes a descriptor by parsing its string representation.
 * @param self Uninitialized descriptor.
 * @param buf Buffer containing the string representation.
 * @param len Lenght of the string representation.
 * @return Positive on success, zero on error.
 */
int gsf_descriptor_from_str_buf(gsf_descriptor* self, const char* buf, size_t len);

/**
 * @brief Initializes a descriptor by parsing its string representation.
 * @param self Uninitialized descriptor.
 * @param string Null-terminated string representation.
 * @return Positive on success, zero on error.
 */
int gsf_descriptor_from_str(gsf_descriptor* self, const char* string);

char* gsf_descriptor_to_str_buf(const gsf_descriptor* self, char* buf, size_t len);
const char* gsf_descriptor_to_str(const gsf_descriptor* self);

/**
 * @brief Changes the schema Descriptor's properties.
 *
 * Modifies any to all of the Descriptor's segments. If any of the string arguments is a
 * null pointer, the certain segment will not be changed.
 *
 * On error, returns null pointer and the Descriptor remains untouched. Otherwise returns
 * the Descriptor (the pointer is equal to the argument).
 *
 * @param descriptor Edited Descriptor.
 * @param group New null-terminated group string or null pointer for no change.
 * @param name New null-terminated name string or null pointer for no change.
 * @param version New null-terminated version string or null pointer for no change.
 * @return Edited Descriptor on success or null pointer on fail.
 */
gsf_descriptor* gsf_descriptor_edit(gsf_descriptor* descriptor, const char* group,
									const char* name, const char* version);

// TODO doc
const char* gsf_descriptor_get_group(const gsf_descriptor* descriptor);
const char* gsf_descriptor_get_name(const gsf_descriptor* descriptor);
const char* gsf_descriptor_get_version(const gsf_descriptor* descriptor);

int gsf_descriptor_match(const gsf_descriptor* self, const gsf_descriptor* preset);


/**
 * @brief Initializes a schema.
 *
 * The schema must be destroyed with @link gsf_schema_destroy @endlink or
 * @link gsf_delete_schema @endlink.
 *
 * The @p root argument must not be an empty string.
 *
 * @param schema Created Schema.
 * @param descriptor Descriptor for the schema.
 * @param root Root Typing or non-empty null-terminated string with its representation or
 * null pointer if Typing is set later.
 * @param class_count Initial amount of classes. Can be expanded later.
 * @return The @a schema or null pointer on error.
 */
gsf_schema* gsf_schema_init(gsf_schema* schema, const gsf_descriptor* descriptor,
							const gsf_typing_or_char* root, size_t class_count);

/**
 * @brief Creates and initializes a schema.
 *
 * The schema must be freed with @link gsf_delete_schema @endlink.
 *
 * The @p root argument must not be an empty string.
 *
 * @param descriptor Descriptor for the schema.
 * @param root Root Typing or non-empty null-terminated string with its representation or
 * null pointer if Typing is set later.
 * @param class_count Initial amount of classes. Can be expanded later.
 * @return Initialized Schema or null pointer on error.
 */
gsf_schema* gsf_schema_new(const gsf_descriptor* descriptor,
						   const gsf_typing_or_char* root, size_t class_count);

/**
 * @brief Destroys a schema.
 *
 * This only destroys the schema internally. It still needs to be freed with @link
 * gsf_delete_schema @endlink.
 *
 * @param schema Destroyed Schema.
 */
void gsf_schema_destroy(gsf_schema* schema);

/**
 * @brief Verifies validity of the schema.
 *
 * TODO more info
 *
 * @param schema Verified Schema.
 * @return Verified Schema or null pointer on error.
 */
gsf_schema* gsf_schema_verify(gsf_schema* schema);

#define gsf_schema_verify(schema)															\
_Generic((schema),																			\
	const gsf_schema*:	(const gsf_schema*) gsf_schema_verify((const gsf_schema*) schema),	\
	default:			gsf_schema_verify(schema)											\
)

/**
 * @brief Gets the schema's Descriptor.
 * @param schema The schema.
 * @return Descriptor.
 */
const gsf_descriptor* gsf_schema_get_descriptor(const gsf_schema* schema);

/**
 * @brief Sets the schema's Descriptor.
 * @param schema The schema.
 * @param descriptor New Descriptor.
 */
void gsf_schema_set_descriptor(gsf_schema* schema, const gsf_descriptor* descriptor);

// TODO doc
const gsf_context* gsf_schema_get_context(const gsf_schema* schema);

/**
 * @brief Gets the schema's root Typing.
 * @param schema The schema.
 * @return Typing of the root value.
 */
const gsf_typing* gsf_schema_get_root(const gsf_schema* schema);

/**
 * @brief Sets the schema's root Typing.
 *
 * If a Typing object is provided, it will be (deep) copied into the schema and therefore
 * can be freed right after calling the function.
 *
 * The @p Typing argument must not be an empty string.
 *
 * @param schema The schema.
 * @param typing New Typing or non-empty null-terminated string with its representation.
 * @return The schema (pointer is the same as the input) or null pointer on error.
 */
gsf_schema* gsf_schema_set_root(gsf_schema* schema, const gsf_typing_or_char* typing);

// TODO gsf_schema_add_to_root (for types instead of typings; maybe without "to")
// TODO gsf_schema_add_root_class (gsf_schema_add_class but also adds the class to root)

/**
 * @brief Gets the current amount of classes of a schema.
 *
 * If the schema was initialized with a bigger amount of classes and only part was added
 * so far, only the latter count.
 *
 * @param schema The schema.
 * @return Amount of its classes.
 */
size_t gsf_schema_class_count(const gsf_schema* schema);

/**
 * @brief Adds a class to the schema.
 *
 * In case the amount of classes would exceed the initially assumed amount, the object
 * will be reallocated to fit one more class.
 *
 * The class will be freed with the schema deletion and should not be deleted manually.
 *
 * @param schema The schema.
 * @param name Null-terminated string name of the class.
 * @param label_count Initial amount of labels in the class. Can be expanded later.
 * @return The new class object.
 */
gsf_class* gsf_schema_add_class(gsf_schema* schema, const char* name,
								size_t label_count);

/**
 * @brief Gets a class of a schema.
 *
 * Gets a class of a schema at the given index. In case the index is too large, a null
 * pointer is returned.
 *
 * @param schema The schema.
 * @param index Classes' index in the schema.
 * @return Found class or null pointer.
 */
gsf_class* gsf_schema_get_class(const gsf_schema* schema, size_t index);

#define gsf_schema_get_class(S, I) _Generic(							\
	1 ? (S) : (gsf_schema*) 0,											\
	const gsf_schema*: (const gsf_class*) gsf_schema_get_class(S, I),	\
	default: gsf_schema_get_class(S, I)									\
)
// TODO get_class_by_name, get_enum_by_name

// /**
//  * @brief Removes a class of a schema.
//  *
//  * Destroys the class if found. If index is out of bounds, nothing happens.
//  *
//  * @param schema The schema.
//  * @param index Classes' index in the schema.
//  */
// void gsf_schema_remove_class(gsf_schema* schema, size_t index);

/**
 * @brief Gets the current amount of enums of a schema.
 * @param schema The schema.
 * @return Amount of its enums.
 */
size_t gsf_schema_enum_count(const gsf_schema* schema);

/**
 * @brief Adds an enum to the schema.
 *
 * The enum will be freed with the schema deletion and should not be deleted manually.
 *
 * @param schema The schema.
 * @param name Null-terminated string name of the enum.
 * @param variant_count Initial amount of variants in the enum. Can be expanded later.
 * @return The new enum object.
 */
gsf_enum* gsf_schema_add_enum(gsf_schema* schema, const char* name, size_t variant_count);

/**
 * @brief Gets an enum of a schema.
 *
 * Gets an enum of a schema at the given index. In case the index is too large, a null
 * pointer is returned.
 *
 * @param schema The schema.
 * @param index Enum's index in the schema.
 * @return Found enum or null pointer.
 */
gsf_enum* gsf_schema_get_enum(const gsf_schema* schema, size_t index);

#define gsf_schema_get_enum(S, I) _Generic(							\
	1 ? (S) : (gsf_schema*) 0,											\
	const gsf_schema*: (const gsf_enum*) gsf_schema_get_class(S, I),	\
	default: gsf_schema_get_class(S, I)									\
)

/**
 * @brief Removes an enum of a schema.
 *
 * Destroys the enum if found. If index is out of bounds, nothing happens.
 *
 * @param schema The schema.
 * @param index Enum's index in the schema.
 */
// FIXME
// void gsf_schema_remove_enum(gsf_schema* schema, size_t index);

/**
 * @brief Gets the type specification representing objects of this class.
 * @return The adequate type.
 */
const gsf_type* gsf_class_as_type(const gsf_class* class);

/**
 * @brief Gets the name of a class.
 * @param class The class.
 * @return Null-terminated string name of the class.
 */
const char* gsf_class_get_name(const gsf_class* class);

/**
 * @brief Sets the name of a class.
 * @param class The class.
 * @param name New null-terminated string name of the class.
 * @return The class (pointer is the same as the input) or null pointer on error.
 */
gsf_class* gsf_class_set_name(gsf_class* class, const char* name);

/**
 * @brief Gets the current amount of labels of a class.
 *
 * If the class was initialized with a bigger amount of labels and only part was added
 * so far, only the latter count.
 *
 * @param class The class.
 * @return Amount of its labels.
 */
size_t gsf_class_label_count(const gsf_class* class);

// TODO error if key exists!
/**
 * @brief Adds a label to a class.
 *
 * In case the amount of labels would exceed the initially assumed amount, the object
 * will be reallocated to fit one more label.
 *
 * The label will be freed with the schema deletion and should not be deleted manually.
 *
 * If a Typing object is provided, it will be (deep) copied into the class and therefore
 * can be freed right after calling the function.
 *
 * The @p Typing argument must not be an empty string.
 *
 * @param class The class.
 * @param key Null-terminated string key of the label.
 * @param typing Typing or non-empty null-terminated string with its representation.
 * @return The new label object.
 */
gsf_label* gsf_class_add_label(gsf_class* class, const char* key,
							   const gsf_typing_or_char* typing);

/**
 * @brief Gets a label of a class at a given index.
 *
 * Gets a label of a class at the given index. In case the index is too large, a null
 * pointer is returned.
 *
 * @param class The class.
 * @param index Label's index in the class.
 * @return Found class or null pointer.
 */
gsf_label* gsf_class_get_label(const gsf_class* class, size_t index);

#define gsf_class_get_label(C, I) _Generic(								\
	1 ? (C) : (gsf_class*) 0,											\
	const gsf_class*: (const gsf_label*) gsf_class_get_label(C, I),		\
	default: gsf_class_get_label(C, I)									\
)

/**
 * @brief Gets a label of a class at a given key.
 *
 * Finds a label of a class that is of a matching key. In case the key is not recognized,
 * a null pointer is returned.
 *
 * @param class The class.
 * @param key Null-terminated string key of the label.
 * @return Found class or null pointer.
 */
gsf_label* gsf_class_get_label_at_key(const gsf_class* class, const char* key);

#define gsf_class_get_label_at_key(C, K) _Generic(								\
	1 ? (C) : (gsf_class*) 0,													\
	const gsf_class*: (const gsf_label*) gsf_class_get_label_at_key(C, K),		\
	default: gsf_class_get_label_at_key(C, K)									\
)

/**
 * @brief Gets index of a label in a class at a given label.
 *
 * Gets index of a label in a class incremented by 1. In case the label does not exist in
 * the class, returns 0.
 *
 * @param class The class.
 * @param label Label of the class.
 * @return Index of the label incremented by 1 or the value 0.
 */
size_t gsf_class_index_at_label(const gsf_class* class, const gsf_label* label);

/**
 * @brief Gets a label in a class at a given key.
 *
 * Gets index of a label in a class incremented by 1. In case the key is not found in the
 * class, returns 0.
 *
 * @param class The class.
 * @param key Null-terminated string key of the label.
 * @return Index of the found label incremented by 1 or the value 0.
 */
size_t gsf_class_index_at_key(const gsf_class* class, const char* key);

/**
 * @brief Removes a label of a class.
 *
 * Deletes and destroys the label if found. If index is out of bounds, nothing happens.
 *
 * @param class The class.
 * @param index Label's index in the class.
 */
void gsf_class_remove_label(gsf_class* class, size_t index);

/**
 * @brief Gets the key of a label.
 * @param label The label.
 * @return Null-terminated string key.
 */
const char* gsf_label_get_key(const gsf_label* label);

/**
 * @brief Sets the key of a label.
 * @param label The label.
 * @param key New null-terminated string key.
 * @return The label (pointer is the same as the input) or null pointer on error.
 */
gsf_label* gsf_label_set_key(gsf_label* label, const char* key);

/**
 * @brief Gets the Typing of a label.
 * @param label The label.
 * @return Typing of the label.
 */
const gsf_typing* gsf_label_get_typing(const gsf_label* label);

/**
 * @brief Sets the Typing of a label.
 *
 * If a Typing object is provided, it will be (deep) copied into the class and therefore
 * can be freed right after calling the function.
 *
 * The @p Typing argument must not be an empty string.
 *
 * @param label The label.
 * @param typing New Typing or non-empty null-terminated string with its representation.
 * @return The label (pointer is the same as the input) or null pointer on error.
 */
gsf_label* gsf_label_set_typing(gsf_label* label, const gsf_typing_or_char* typing);


/**
 * @brief Gets the value holder for the default value of a label.
 *
 * This method is intended for getting the default. Use @link gsf_label_access_default
 * @endlink to set the default value.
 *
 * @param label The label.
 * @return gsf_value* Value holder of the default or null pointer if not set.
 */
const gsf_value* gsf_label_get_default(const gsf_label* label);

/**
 * @brief Gets or creates the value holder for the default value of a label.
 *
 * This function will create the default value holder if it does not exist. A created
 * but unset value is safe, but to avoid redurantly creating a value, use the constant
 * variant of the function: @link gsf_label_get_default @endlink.
 *
 * The default values are only applied when @link gsf_object_apply_defaults @endlink is
 * called. It is a good practise to set the defaults after encoding non-default to avoid
 * redurant default value copying.
 *
 * @param label The label.
 * @return gsf_value* Value holder of the default or null pointer on error.
 */
gsf_value* gsf_label_access_default(gsf_label* label);


/**
 * @brief Gets the docstring of a label
 * @param label The label.
 * @return Null-terminated docstring of the label.
 */
const char* gsf_label_get_doc(const gsf_label* label);

/**
 * @brief Sets the docstring of a label
 * @param label The label.
 * @param doc New null-terminated docstring of the label.
 * @return The label (pointer is the same as the input) or null pointer on error.
 */
gsf_label* gsf_label_set_doc(gsf_label* label, const char* doc);

// TODO enum const correctness

/**
 * @brief Gets the type specification representing instances of this enum.
 * @return The adequate type.
 */
const gsf_type* gsf_enum_as_type(const gsf_enum* self);

const char* gsf_enum_get_name(const gsf_enum* self);

size_t gsf_enum_variant_count(const gsf_enum* self);

gsf_variant* gsf_enum_add_variant(gsf_enum* self, const char* name);

void gsf_enum_remove_variant(gsf_enum* self, size_t index);

int gsf_enum_index_of(const gsf_enum* self, const gsf_variant* variant, size_t* index);

gsf_variant* gsf_enum_get_variant(const gsf_enum* self, size_t index);

gsf_variant* gsf_enum_get_variant_by_name(const gsf_enum* self, const char* name);

gsf_variant* gsf_variant_matches(gsf_variant* self, const char* string);

const char* gsf_variant_get_name(const gsf_variant* self);

gsf_variant* gsf_variant_set_doc(gsf_variant* self, const char* doc);

const char* gsf_variant_get_doc(const gsf_variant* self);


/**
 * @brief Initializes an article.
 *
 * The article must be destroyed with @link gsf_article_destroy @endlink or
 * @link gsf_delete_article @endlink.
 *
 * @param schema Schema for the article or null pointer for schemaless mode ('any' typing as root).
 * @return The @a article or null pointer on error.
 */
gsf_article* gsf_article_init(gsf_article* article, const gsf_schema* schema);

/**
 * @brief Creates and initializes an article.
 *
 * The article must be freed with @link gsf_delete_article @endlink.
 *
 * @param schema Schema for the article or null pointer for schemaless mode ('any' typing as root).
 * @return Initialized article or null pointer on error.
 */
gsf_article* gsf_article_new(gsf_schema* schema);

/**
 * @brief Destroys an article.
 *
 * This only destroys the article internally. It still needs to be freed with @link
 * gsf_delete_article @endlink.
 *
 * @param article Destroyed article.
 */
void gsf_article_destroy(gsf_article* article);

/**
 * @breif Gets the article's schema.
 * @param article The article.
 * @return The schema or null pointer in schemaless mode.
 */
const gsf_schema* gsf_article_get_schema(const gsf_article* article);

// TODO const-correctness
/**
 * @brief Gets the value holder (do not free) for the root value of an article.
 * @param article The article.
 * @return The value holder.
 */
gsf_value* gsf_article_access_root(const gsf_article* article);

#define gsf_article_access_root(A) _Generic(							\
	1 ? (A) : (gsf_article*) 0,											\
	const gsf_article*: (const gsf_value*) gsf_article_access_root(A),	\
	default: gsf_article_access_root(A)									\
)

// TODO doc
const gsf_class* gsf_object_get_class(const gsf_object* object);

/**
 * @brief Gets the value holder (do not free) for the field at a given label.
 *
 * If label is outside of the object's class, returns null pointer.
 *
 * @param object The object.
 * @param label Pointing label.
 * @return Found value holder or null pointer.
 */
gsf_value* gsf_object_access_at_label(const gsf_object* object, const gsf_label* label);

#define gsf_object_access_at_label(O, L) _Generic(							\
	1 ? (O) : (gsf_object*) 0,												\
	const gsf_object*: (const gsf_value*) gsf_object_access_at_label(O, L),	\
	default: gsf_object_access_at_label(O, L)								\
)

/**
 * @brief Gets the value holder (do not free) for the field at a given key.
 *
 * If the object/class doesn't contain a label with such key, returns null pointer.
 *
 * @param object The object.
 * @param key Pointing null-terminated string key.
 * @return Found value holder or null pointer.
 */
gsf_value* gsf_object_access_at_key(const gsf_object* object, const char* key);

#define gsf_object_access_at_key(O, K) _Generic(							\
	1 ? (O) : (gsf_object*) 0,												\
	const gsf_object*: (const gsf_value*) gsf_object_access_at_key(O, K),	\
	default: gsf_object_access_at_key(O, K)									\
)


/**
 * @brief Resets next position to the first field.
 * @see gsf_object_access_next
 * @param object The object.
 */
void gsf_object_access_iter(const gsf_object* object);

/**
 * @brief Gets the value holder for the value after the previously obtained value.
 *
 * Gets the value holder for the field after the previousely accessed field.
 * Can be used for iteration (use @link gsf_object_access_first @endlink first).
 *
 * @param object The object.
 * @return gsf_value* Next value holder or null pointer if no more are available.
 */
gsf_value* gsf_object_access_next(const gsf_object* object);

#define gsf_object_access_next(O) _Generic(							\
	1 ? (O) : (gsf_object*) 0,											\
	const gsf_object*: (const gsf_value*) gsf_object_access_next(O),	\
	default: gsf_object_access_next(O)									\
)

/**
 * @brief Applies the default values of a label to unset members.
 *
 * Does not overwrite any values set previousely. Only affects unset values.
 * Empty values at labels without a default value will remain unset.
 *
 * @param object The object.
 * @return Amount of affected values.
 */
size_t gsf_object_apply_defaults(gsf_object* object);

/**
 * @brief Counts fields that are left to set. Includes unset fields with default values.
 * @param object The object.
 * @return size_t Amount of unset values.
 */
size_t gsf_object_count_unset(const gsf_object* object);

/**
 * @brief Gets the amount of possible elements of a list.
 *
 * Gets the currently allocated size of the list.
 *
 * @param list The list.
 * @return Amount of its elements.
 */
size_t gsf_list_get_size(const gsf_list* list);

/**
 * @brief Changes the amount of possible elements of a list.
 *
 * If the size shrinks, leading elements will be dropped.
 * If it extends, new elements will be set to neutral/zero values.
 *
 * @param list The list.
 * @param size New amount of its elements.
 * @returns The list or null pointer on error.
 */
gsf_list* gsf_list_resize(gsf_list* list, size_t size);

/**
 * @brief Gets the amount of elements of a list.
 *
 * Gets the current amount of elements in list. This value is also the index of the next
 * element added by @link gsf_list_add_element @endlink.
 *
 * @param list The list.
 * @return Amount of its elements.
 */
size_t gsf_list_element_count(const gsf_list* list);

/**
 * @brief Gets a value holder for the element of a list at a given index.
 *
 * If index exceeds the bounds of the list, null pointer is returned.
 * Do not free the value holder.
 *
 * @param list The list.
 * @param index Index of the element.
 * @return Found value holder or null pointer.
 */
gsf_value* gsf_list_get_element(const gsf_list* list, size_t index);

#define gsf_list_get_element(L, I) _Generic(							\
	1 ? (L) : (gsf_list*) 0,											\
	const gsf_schema*: (const gsf_value*) gsf_list_get_element(L, I),	\
	default: gsf_list_get_element(L, I)									\
)

/**
 * @brief Adds a value if needed and gets a value holder after the last gotten index.
 *
 * Starts from the index 0 or index set by @link gsf_list_get_element @endlink
 * and advances by 1 with every call.
 *
 * If the next index is out of bounds of the list it extends the list by one.
 *
 * @param list The list.
 * @return Value holder of the new element or null pointer on error.
 */
gsf_value* gsf_list_add_element(gsf_list* list);

/**
 * @brief Removes an element from a list.
 * @param map The list.
 * @param index Index of removed element.
 * @return Positive on success, zero on missing element (no error).
 */
int gsf_list_remove_element(gsf_list* list, size_t index);

/**
 * @brief Gets the Typing specification of elements of a list.
 * @param list The list.
 * @return The Typing.
 */
const gsf_typing* gsf_list_get_typing(const gsf_list* list);

/**
 * @brief Gets the amount of possible elements of a map.
 *
 * Gets the currently allocated size of the map.
 *
 * @param map The map.
 * @return Amount of its elements.
 */
size_t gsf_map_get_size(const gsf_map* map);

/**
 * @brief Changes the amount of possible elements of a map.
 *
 * If the size shrinks, leading elements will be dropped.
 * If it extends, new elements will be set to neutral/zero values.
 *
 * @param map The map.
 * @param size New amount of its elements.
 * @returns The map or null pointer on error.
 */
gsf_map* gsf_map_resize(gsf_map* map, size_t size);

/**
 * @brief Gets the amount of elements of a map.
 *
 * Gets the current amount of elements in map. This value is also the index of the next
 * element added by @link gsf_map_add_element @endlink.
 *
 * @param map The map.
 * @return Amount of its elements.
 */
size_t gsf_map_element_count(const gsf_map* map);

/**
 * @brief Gets the key for the element of a map at a given index.
 *
 * The returned char pointer is a null-terminated string.
 * If index exceeds the bounds of the map, null pointer is returned.
 *
 * @param map The map.
 * @param index Index of the element.
 * @return Key of found element or null pointer.
 */
const char* gsf_map_get_key(const gsf_map* map, size_t index);

/**
 * @brief Gets a value holder for the element of a map at a given index.
 *
 * If index exceeds the bounds of the map, null pointer is returned.
 * Do not free the value holder.
 *
 * @param map The map.
 * @param index Index of the element.
 * @return Value holder of found element or null pointer.
 */
gsf_value* gsf_map_get_value(const gsf_map* map, size_t index);

#define gsf_map_get_value(M, I) _Generic(								\
	1 ? (M) : (gsf_map*) 0,												\
	const gsf_schema*: (const gsf_value*) gsf_map_get_value(M, I),		\
	default: gsf_map_get_value(M, I)									\
)

/**
 * @brief Gets a value holder for the element of a map at a given key.
 *
 * If no element matches the given key, null pointer is returned.
 * Do not free the value holder.
 *
 * @param map The map.
 * @param key Null-terminated string key of the element.
 * @return Found value holder or null pointer.
 */
gsf_value* gsf_map_get_value_at_key(const gsf_map* map, const char* key);

#define gsf_map_get_value_at_key(M, K) _Generic(							\
	1 ? (M) : (gsf_map*) 0,													\
	const gsf_schema*: (const gsf_value*) gsf_map_get_value_at_key(M, K),	\
	default: gsf_map_get_value_at_key(M, K)									\
)

/**
 * @brief Adds a value if needed and gets a value holder after the last gotten index.
 *
 * If the map already contains that key, an error will occur.
 *
 * Starts from the index 0 or index set by @link gsf_map_get_element @endlink
 * and advances by 1 with every call.
 *
 * If the next index is out of bounds of the map it extends the map by one.
 *
 * @param map The map.
 * @param key Null-terminated string key of the added element.
 * @return Value holder of the new element or null pointer on error.
 */
gsf_value* gsf_map_add_element(gsf_map* map, const char* key);

/**
 * @brief Removes an element from a map.
 * @param map The map.
 * @param index Index of removed element.
 * @return Positive on success, zero on missing element (no error).
 */
int gsf_map_remove_element(gsf_map* map, size_t index);

/**
 * @brief Removes an element from a map by key.
 * @param map The map.
 * @param key Key of element for lookup.
 * @return Positive on success, zero on missing element (no error).
 */
int gsf_map_remove_at_key(gsf_map* map, const char* key);

/**
 * @brief Gets the Typing specification of elements of a map.
 * @param map The map.
 * @return The Typing.
 */
const gsf_typing* gsf_map_get_typing(const gsf_map* map);

/**
 * @brief Gets the amount of elements of an array.
 * @param array The array.
 * @return Amount of elements of an array.
 */
size_t gsf_array_get_size(const gsf_array* array);

/**
 * @brief Resizes the array
 *
 * Simply reallocates the array. On expansion, sets the values of new elements to 0.
 *
 * @param array The array.
 * @param size New size.
 * @return The array (input pointer) or null pointer on error.
 */
gsf_array* gsf_array_resize(gsf_array* array, size_t size);

/**
 * @brief Gets the type specification of elements of an array.
 * @param array The array.
 * @return The type.
 */
const gsf_type* gsf_array_get_type(const gsf_array* array);

// TODO const-correctness of the array functions below

// TODO doc
void* gsf_array_as_any(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 8-bit signed integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
int8_t* gsf_array_as_i8(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 16-bit signed integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
int16_t* gsf_array_as_i16(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 32-bit signed integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
int32_t* gsf_array_as_i32(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 64-bit signed integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
int64_t* gsf_array_as_i64(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 8-bit unsigned integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
uint8_t* gsf_array_as_u8(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 16-bit unsigned integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
uint16_t* gsf_array_as_u16(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 32-bit unsigned integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
uint32_t* gsf_array_as_u32(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 64-bit unsigned integers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
uint64_t* gsf_array_as_u64(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 32-bit floating-point numbers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
float* gsf_array_as_f32(const gsf_array* array);

/**
 * @brief Gets direct pointer to values of an array of 64-bit floating-point numbers.
 * @param array The array.
 * @return Pointer to the first value or null if wrong type.
 */
double* gsf_array_as_f64(const gsf_array* array);

// TODO 2d arrays?

/**
 * @brief Deeply copies a value.
 *
 * The destination's current value will be destroyed while keeping its typing. If sources
 * type does not match the destination's typing, an error will be raised.
 *
 * @param dst Destination value holder.
 * @param src Source value holder.
 * @return The destination value or null pointer on error.
 */
gsf_value* gsf_value_copy(gsf_value* dst, const gsf_value* src);

/**
 * @brief Gets the Typing specification of a value holder.
 *
 * The Typing specification contains all types that this value is allowed to be set to.
 *
 * @param value The value holder.
 * @return The Typing specification.
 */
const gsf_typing* gsf_value_get_typing(const gsf_value* value);

/**
 * @brief Gets the current type specification of a value holder.
 *
 * The type changes when different values are encoded in a value holder.
 *
 * @param value The value holder.
 * @return The current type specification or null pointer if no value encoded.
 */
const gsf_type* gsf_value_get_type(const gsf_value* value);

/**
 * @brief Removes the encoded value from a value holder or does nothing if already unset.
 * @param value The value holder.
 */
void gsf_value_unset(gsf_value* value);

/**
 * @brief Reads a value as a null.
 *
 * Can be used to generate erorrs for the assertion of null pointer on a value.
 *
 * @param holder The value holder.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_null(const gsf_value* holder);

/**
 * @brief Writes a value as a null.
 * @param holder The value holder.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_null(gsf_value* holder);

/**
 * @brief Reads a value as an object.
 * @param holder The value holder.
 * @return Object at the value holder.
 */
const gsf_object* gsf_value_decode_object(const gsf_value* holder);

/**
 * @brief Writes a value as an object.
 * @param holder The value holder.
 * @param type Class of the written object or null pointer for default (if available).
 * @return Mutable object at the value holder.
 */
gsf_object* gsf_value_encode_object(gsf_value* holder, const gsf_class* type);

/**
 * @brief Reads or writes (depending on whether it is set) a value as an object.
 * @param holder The value holder.
 * @param type Class to set (or match) or null pointer for default (if available).
 * @return Mutable object at the value holder.
 */
gsf_object* gsf_value_as_object(gsf_value* holder, const gsf_class* type);

/**
 * @brief Reads a value as a map.
 * @param holder The value holder.
 * @return Map at the value holder.
 */
const gsf_map* gsf_value_decode_map(const gsf_value* holder);

/**
 * @brief Writes a value as a map.
 * @param holder The value holder.
 * @param typing Typing of the map element values or non-empty null-terminated string
 * with its representation or null pointer for default (if available).
 * @param size Initial amount of elements in the map. Can be expanded later.
 * @return Mutable map at the value holder.
 */
gsf_map* gsf_value_encode_map(gsf_value* holder, const gsf_typing_or_char* typing,
							  size_t size);

/**
 * @brief Reads or writes (depending on whether it is set) a value as a map.
 * @see gsf_value_decode_map
 * @see gsf_value_encode_map
 * @param holder The value holder.
 * @param typing Typing of the map element values to set (or match) or non-empty null
 * -terminated string with its representation or null pointer for default (if available).
 * @param size Initial amount of elements in the map (in case of writing). If a map is
 * already encoded, but of smaller size, it will be resized to match it.
 * @return Mutable map at the value holder.
 */
gsf_map* gsf_value_as_map(gsf_value* holder, const gsf_typing_or_char* typing,
						  size_t size);

/**
 * @brief Reads a value as a list.
 * @param holder The value holder.
 * @return List at the value holder.
 */
const gsf_list* gsf_value_decode_list(const gsf_value* holder);

/**
 * @brief Writes a value as a list.
 *
 * If a typing object is provided, it will be (deep) copied into the list and therefore
 * can be freed right after calling the function.
 *
 * The @p Typing argument must not be an empty string.
 *
 * @param holder The value holder.
 * @param typing Typing of the list elements or non-empty null-terminated string with its
 * representation or null pointer for default (if available).
 * @param size Initial amount of elements in the list. Can be expanded later.
 * @return Mutable list at the value holder.
 */
gsf_list* gsf_value_encode_list(gsf_value* holder, const gsf_typing_or_char* typing,
								size_t size);

/**
 * @brief Reads or writes (depending on whether it is set) a value as a list.
 * @see gsf_value_decode_list
 * @see gsf_value_encode_list
 * @param holder The value holder.
 * @param typing Typing of the list elements to set (or match) or non-empty null
 * -terminated string with its representation or null pointer for default (if available).
 * @param size Initial amount of elements in the list (in case of writing). If a list is
 * already encoded, but of smaller size, it will be resized to match it.
 * @return Mutable list at the value holder.
 */
gsf_list* gsf_value_as_list(gsf_value* holder, const gsf_typing_or_char* typing,
							size_t size);

/**
 * @brief Reads a value as an array.
 * @param holder The value holder.
 * @return Array at the value holder.
 */
const gsf_array* gsf_value_decode_array(const gsf_value* holder);

/**
 * @brief Writes a value as an array.
 *
 * If a type object is provided, it will be (deep) copied into the array and therefore
 * can be freed right after calling the function.
 *
 * The @p type argument must not be an empty string.
 *
 * @param holder The value holder.
 * @param type Type of the array elements or non-empty null-terminated string with its
 * representation or null pointer for default (if available).
 * @param size Initial amount of elements in the array. Can be expanded later.
 * @return Mutable array at the value holder.
 */
gsf_array* gsf_value_encode_array(gsf_value* holder, const gsf_type_or_char* type,
								  size_t size);

/**
 * @brief Reads or writes (depending on whether it is set) a value as an array.
 * @see gsf_value_decode_array
 * @see gsf_value_encode_array
 * @param holder The value holder.
 * @param type (Type of the array elements to set (or match) or non-empty null-terminated
 * string with its representation or null pointer for default (if available).
 * @param size Initial amount of elements in the array (in case of writing). If an array
 * is already encoded, but of smaller size, it will be resized to match it.
 * @return Mutable array at the value holder.
 */
gsf_array* gsf_value_as_array(gsf_value* holder, const gsf_type_or_char* type,
							  size_t size);

/**
 * @brief Reads a value as an enum variant.
 * @param holder The value holder.
 * @return The current enum variant or null pointer on error.
 */
const gsf_variant* gsf_value_decode_enum(const gsf_value* holder);

/**
 * @brief Writes a value as an enum variant.
 * @param holder The value holder.
 * @param type The encoded enum type or null-pointer for default (if available).
 * @param variant Variant of the enum.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_encode_enum(gsf_value* holder, const gsf_enum* type,
								 const gsf_variant* variant);

/**
 * @brief Reads a value as an enum variant.
 * @param holder The value holder.
 * @return Name of the selected enum variant or null pointer on error.
 */
const char* gsf_value_decode_enum_str(const gsf_value* holder);

/**
 * @brief Writes a value as an enum variant.
 * @param holder The value holder.
 * @param type The encoded enum type or null-pointer for default (if available).
 * @param variant Name of the enum variant.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_encode_enum_str(gsf_value* holder, const gsf_enum* type,
							  const char* variant);

/**
 * @brief Reads a value as an enum variant.
 * @param holder The value holder.
 * @param out Output index of selected enum variant.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_enum_idx(const gsf_value* holder, size_t* out);

/**
 * @brief Writes a value as an enum variant.
 * @param holder The value holder.
 * @param type The encoded enum type or null-pointer for default (if available).
 * @param variant Index of the enum variant.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_encode_enum_idx(gsf_value* holder, const gsf_enum* type,
							  size_t variant);

/**
 * @brief Reads a value as an 8-bit signed integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_i8(const gsf_value* holder, int8_t* out);

/**
 * @brief Writes a value as an 8-bit signed integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_i8(gsf_value* holder, int8_t value);

/**
 * @brief Reads a value as an 16-bit signed integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_i16(const gsf_value* holder, int16_t* out);

/**
 * @brief Writes a value as an 16-bit signed integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_i16(gsf_value* holder, int16_t value);

/**
 * @brief Reads a value as an 32-bit signed integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_i32(const gsf_value* holder, int32_t* out);

/**
 * @brief Writes a value as an 32-bit signed integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_i32(gsf_value* holder, int32_t value);

/**
 * @brief Reads a value as an 64-bit signed integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_i64(const gsf_value* holder, int64_t* out);

/**
 * @brief Writes a value as an 64-bit signed integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_i64(gsf_value* holder, int64_t value);

/**
 * @brief Reads a value as an 8-bit unsigned integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_u8(const gsf_value* holder, uint8_t* out);

/**
 * @brief Writes a value as an 8-bit unsigned integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_u8(gsf_value* holder, uint8_t value);

/**
 * @brief Reads a value as an 16-bit unsigned integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_u16(const gsf_value* holder, uint16_t* out);

/**
 * @brief Writes a value as an 16-bit unsigned integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_u16(gsf_value* holder, uint16_t value);

/**
 * @brief Reads a value as an 32-bit unsigned integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_u32(const gsf_value* holder, uint32_t* out);

/**
 * @brief Writes a value as an 32-bit unsigned integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_u32(gsf_value* holder, uint32_t value);

/**
 * @brief Reads a value as an 64-bit unsigned integer.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_u64(const gsf_value* holder, uint64_t* out);

/**
 * @brief Writes a value as an 64-bit unsigned integer.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_u64(gsf_value* holder, uint64_t value);

/**
 * @brief Reads a value as an 32-bit floating-point number.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_f32(const gsf_value* holder, float* out);

/**
 * @brief Writes a value as an 32-bit floating-point number.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_f32(gsf_value* holder, float value);

/**
 * @brief Reads a value as an 64-bit floating-point number.
 * @param holder The value holder.
 * @param out Output value.
 * @return Non-zero on success, zero on error.
 */
int gsf_value_decode_f64(const gsf_value* holder, double* out);

/**
 * @brief Writes a value as an 64-bit floating-point number.
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_f64(gsf_value* holder, double value);

/**
 * @brief Reads a value as a string.
 * @param holder The value holder.
 * @return The value or null pointer on error.
 */
const char* gsf_value_decode_str(const gsf_value* holder);

/**
 * @brief Writes a value as a string.
 *
 * The string will be copied to the holder.
 *
 * @param holder The value holder.
 * @param value The value.
 * @return The value holder or null pointer on error.
 */
gsf_value* gsf_value_encode_str(gsf_value* holder, const char* value);

#ifdef __cplusplus
} // extern "C"
#undef class
#endif
