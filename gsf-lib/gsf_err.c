#include "gsf_err.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>

extern void* (*gsf_alloc)(size_t);

#if !(!defined(__STDC_NO_THREADS) || __STDC_NO_THREADS__)
	#include <threads.h>
#endif

#undef gsf_errno
#undef gsf_errno_msg

#ifndef thread_local
	#define thread_local _Thread_local
#endif

thread_local int gsf_errno = gsf_enone;

int* gsf_errno_location(void) {
	return &gsf_errno;
}

thread_local char* gsf_errno_msg = 0;

char* gsf_errno_msg_location(void) {
	if (!gsf_errno_msg) {
		gsf_errno_msg = gsf_alloc(GSF_ERRNO_MSG_SIZE);
		gsf_errno_msg[0] = '\0';
	}
	return gsf_errno_msg;
}

#define gsf_errno_msg (gsf_errno_msg_location())

const char* gsf_errno_desc(int error_number) {
	switch (error_number) {
		default:
			return "unknown error number";
		case gsf_enone:
			return "no error";
		case gsf_estd:
			return "unhandled error caused by libc";
		case gsf_eassert:
			return "implementation-based assertion failed";
		case gsf_emem:
			return "failed to allocate memory";
		case gsf_enullptr:
			return "unexpected null pointer";
		case gsf_eshortbuf:
			return "buffer size too small";
		case gsf_evalunset:
			return "value not set";
		case gsf_ekeytaken:
			return "key already exits";
		case gsf_ekeyempty:
			return "key is empty";
		case gsf_ewrongtype:
			return "bad type";
		case gsf_eforbtype:
			return "forbidden type or typing";
		case gsf_ebadname:
			return "illegal name format error";
		case gsf_etypefmt:
			return "type parsing error";
		case gsf_etypingfmt:
			return "typing parsing error";
		case gsf_eusupptype:
			return "unknown type name (missing class or enum?)";
		case gsf_eukntype:
			return "function does not handle the used type";
		case gsf_euknenuvar:
			return "unknown enum variant";
		case gsf_euknschema:
			return "unknown schema (descriptor)";
		case gsf_ecfmtunsup:
			return "codec format unsupported by this function";
		case gsf_etxtart:
			return "textual article parsing error";
		case gsf_etxtsch:
			return "textual schema parsing error";
		case gsf_ebinart:
			return "binary article parsing error";
		case gsf_ebinsch:
			return "binary schema parsing error";
	}
}

void gsf_errno_set(int error_number, const char* message) {
	gsf_errno = error_number;
	gsf_errno_msg_location();
	if (message) {
		size_t len = strlen(message);
		if (len < GSF_ERRNO_MSG_SIZE) {
			memcpy(gsf_errno_msg, message, len + 1);
		} else {
			memcpy(gsf_errno_msg, message, GSF_ERRNO_MSG_SIZE - 1);
			gsf_errno_msg[GSF_ERRNO_MSG_SIZE - 1] = '\0';
		}
	} else {
		gsf_errno_msg[0] = '\0';
	}
}
void gsf_errno_setf(int error_number, const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	gsf_errno_setfv(error_number, fmt, va);
	va_end(va);
}
void gsf_errno_setfv(int error_number, const char* fmt, va_list va) {
	gsf_errno = error_number;
	gsf_errno_msg_location();
	vsnprintf(gsf_errno_msg, GSF_ERRNO_MSG_SIZE, fmt, va);
}

void gsf_errno_reset() {
	gsf_errno = gsf_enone;
	gsf_errno_msg[0] = '\0';
}

void gsf_errno_print_keep() {
	printf("%s", gsf_errno_desc(gsf_errno));
	if (strlen(gsf_errno_msg_location()) == 0) return;
	printf(": %s", gsf_errno_msg);
}

void gsf_errno_string_keep(char* out) {
	const char* desc = gsf_errno_desc(gsf_errno);
	size_t desc_len = strlen(desc);
	memcpy(out, desc, desc_len + 1);
	size_t msg_len = strlen(gsf_errno_msg_location());
	if (msg_len == 0) return;
	strcpy(out + desc_len, ": ");
	desc_len += 2;
	if (desc_len + msg_len + 1 > GSF_ERRNO_MSG_SIZE) {
		msg_len = GSF_ERRNO_MSG_SIZE - desc_len - 1;
	}
	memcpy(out + desc_len, gsf_errno_msg, msg_len);
	out[desc_len + msg_len] = '\n';
}

void gsf_errno_print() {
	gsf_errno_print_keep();
	gsf_errno_reset();
}
void gsf_errno_display_keep() {
	gsf_errno_print_keep();
	puts("");
}
void gsf_errno_display() {
	gsf_errno_print();
	puts("");
}

void gsf_errno_string(char* out) {
	gsf_errno_string_keep(out);
	gsf_errno_reset();
}
