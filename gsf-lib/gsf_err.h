#pragma once

#ifndef __cplusplus
#include <stdarg.h>
#else
#include <cstdarg>
extern "C" {
#endif

#define GSF_ERRNO_MSG_SIZE 2048

/**
 * @brief Gets the location of the thread-local errno.
 * @return Pointer to the current errno value.
 */
extern int* gsf_errno_location(void);

/**
 * @brief Gets the location of the thread-local errno message.
 * @return Pointer to the current null-terminated erorr message.
 */
extern char* gsf_errno_msg_location(void);

/**
 * @brief The current GSF error number.
 *
 * By default the value is 0 and should be reset to 0 after error handling using @link
 * gsf_errno_reset @endlink.
 *
 * This value is set by various functions of the library. To get a description on any
 * error number, use @link gsf_errno_desc @endlink. Some errors also provide an
 * additional message that can be retrieved with @link gsf_errno_msg @endlink.
 */
#define gsf_errno (*gsf_errno_location())

/**
 * @brief The current GSF error message.
 *
 * By default the string is empty and can be reset with @link gsf_errno_reset @endlink.
 *
 * This never results in a null pointer. On first usage, the message is allocated with
 * size of @link GSF_ERRNO_MSG_SIZE @endlink bytes.
 */
#define gsf_errno_msg (gsf_errno_msg_location())

enum gsf_errno_codes {
	gsf_enone = 0,	/// No error
	gsf_estd,		/// Unhandled error caused by libc - see standard errno.
	gsf_eassert,	/// Wrongful implementation assertion.
	gsf_emem,		/// Failed to allocate memory.
	gsf_enullptr,	/// Unexpectedly received null pointer as an argument.
	gsf_eshortbuf,	/// Buffer too short.
	gsf_evalunset,	/// Unset value.
	gsf_ekeytaken,	/// Label, element, class or enum with that key/name already exists.
	gsf_ekeyempty,	/// Label key may not be empty.
	gsf_ewrongtype,	/// Function only allowed for type(s) other than provided.
	gsf_eforbtype,	/// Type or typing is unsupported by schema or parent.
	gsf_ebadname,	/// Illegal name format.
	gsf_etypefmt,	/// Type parsing error.
	gsf_etypingfmt,	/// Typing parsing error.
	gsf_eusupptype,	/// Type manager does not support a type (casting error).
	gsf_eukntype,	/// Type not known. // FIXME merge with gsf_ewrongtype?
	gsf_euknenuvar,	/// Unknown enum variant.
	gsf_euknschema, /// Unknown schema.
	gsf_ecfmtunsup,	/// Unsupported codec format.
	gsf_etxtart,	/// Text article parsing error.
	gsf_etxtsch,	/// Text schema parsing error.
	gsf_ebinart,	/// Binary article parsing error.
	gsf_ebinsch,	/// Binary schema parsing error.
};

/**
 * @brief Gets a string description of an error.
 * @param error_number The error number; usually @link gsf_errno @endlink.
 * @return Null-terminated string containing literal explanation of the error.
 */
const char* gsf_errno_desc(int error_number);

/**
 * @brief Sets the current error.
 *
 * Clears the previous error and sets the new one.
 *
 * @param error_number The error code.
 * @param message Null-terminated additional message or null pointer for no message.
 */
void gsf_errno_set(int error_number, const char* message);
/**
 * @brief Sets the current error to a formatted string.
 * @see gsf_errno_set
 * @param error_number The error code.
 * @param fmt Null-terminated additional message format.
 * @param ... Arguments to fill the message according to the format.
 */
void gsf_errno_setf(int error_number, const char* fmt, ...);
/**
 * @see gsf_errno_setf
 */
void gsf_errno_setfv(int error_number, const char* fmt, va_list va);

/**
 * @brief Resets the current error number and message.
 */
void gsf_errno_reset();

/**
 * @brief Formats and prints the current error and message to stderr.
 * The printed string is in the same format as @link gsf_errno_string @endlink.
 */
void gsf_errno_print_keep();
/**
 * @brief Formats and prints the current error and message to stderr and resets it.
 * The printed string is in the same format as @link gsf_errno_string @endlink.
 */
void gsf_errno_print();
/**
 * @brief Formats and prints the current error and message to stderr.
 * Also adds a line end character.
 * The printed string is in the same format as @link gsf_errno_string @endlink.
 */
void gsf_errno_display_keep();
/**
 * @brief Formats and prints the current error and message to stderr and resets it.
 * Also adds a line end character.
 * The printed string is in the same format as @link gsf_errno_string @endlink.
 */
void gsf_errno_display();

#ifndef __cplusplus

/**
 * @brief Copies the current error and message formatted to a string.
 * @param out The output string buffer. Will be null-terminated.
 */
void gsf_errno_string_keep(char out[static GSF_ERRNO_MSG_SIZE]);

/**
 * @brief Copies the current error and message formatted to a string and resets it.
 * @param out The output string buffer. Will be null-terminated.
 */
void gsf_errno_string(char out[static GSF_ERRNO_MSG_SIZE]);

#else

// c++ does not allow array size notation
void gsf_errno_string_keep(char* out);
void gsf_errno_string(char* out);

#endif

#ifdef __cplusplus
} // extern "C"
#endif
