#include "gsf_codec.h"

#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdbool.h>

#include "gsf_err.h"
#include "gsf_types.h"
#include "gsf_data.h"

#include "gsf_util.c"

extern const void* gsf_value_decode_any(const gsf_value* holder);

// current position
#define PTR (char*) writer->ptr
// uncasted current position
#define VPTR writer->ptr
// space left
#define GSF_SL (writer->end - writer->ptr)
// require space
#define GSF_RS(space) do {									\
	if (GSF_SL < space) {									\
		int ret = writer->write(writer, 4096 + GSF_SL);		\
		if (ret <= 0) {										\
			if (ret == 0) GSF_RAISE(gsf_eshortbuf);			\
			return 0;										\
		}													\
		if (GSF_SL < space) GSF_RAISE(gsf_eshortbuf);		\
	}														\
} while (0)
// require space for type or typing
#define GSF_RS_TYPE() GSF_RS(256) // TODO better solution

int gsf_txt_value_write(struct gsf_writer* writer, const gsf_value* value, size_t depth);
int gsf_txt_scalar_write(struct gsf_writer* writer, const gsf_value* value);
int gsf_txt_string_write(struct gsf_writer* writer, const gsf_value* value, size_t depth);
int gsf_txt_enum_write(struct gsf_writer* writer, const gsf_value* value, size_t depth);
int gsf_txt_array_write(struct gsf_writer* writer, const gsf_value* value, size_t depth);
int gsf_txt_container_write(struct gsf_writer* writer, const gsf_value* value, size_t depth);
const gsf_type* gsf_txt_list_get_element(const char** key_out, const void* list, size_t index, int flags);
const gsf_type* gsf_txt_map_get_element(const char** key_out, const void* map, size_t index, int flags);
const gsf_type* gsf_txt_object_get_element(const char** key_out, const void* object, size_t index, int flags);
int gsf_txt_list_write_element(struct gsf_writer* writer, const void* list, size_t index, size_t depth);
int gsf_txt_map_write_element(struct gsf_writer* writer, const void* map, size_t index, size_t depth);
int gsf_txt_object_write_element(struct gsf_writer* writer, const void* object, size_t index, size_t depth);

static int write_i8(struct gsf_writer* writer, const int8_t* value);
static int write_i16(struct gsf_writer* writer, const int16_t* value);
static int write_i32(struct gsf_writer* writer, const int32_t* value);
static int write_i64(struct gsf_writer* writer, const int64_t* value);
static int write_u8(struct gsf_writer* writer, const uint8_t* value);
static int write_u16(struct gsf_writer* writer, const uint16_t* value);
static int write_u32(struct gsf_writer* writer, const uint32_t* value);
static int write_u64(struct gsf_writer* writer, const uint64_t* value);
static int write_f32(struct gsf_writer* writer, const float* value);
static int write_f64(struct gsf_writer* writer, const double* value);
enum scalar_type {
	i8, i16, i32, i64,
	u8, u16, u32, u64,
	f32, f64
};
static enum scalar_type get_scalar_type(const gsf_type* type);
#define write_fn(S) [S] = ((int(*)(struct gsf_writer*, const void*)) &write_##S)
int(*write_scalar[10])(struct gsf_writer*, const void*) = {
		write_fn(i8), write_fn(i16), write_fn(i32), write_fn(i64),
		write_fn(u8), write_fn(u16), write_fn(u32), write_fn(u64),
		write_fn(f32), write_fn(f64),
};
#undef write_fn


int write_signature(struct gsf_writer* writer, int flag_range, int flags) {
	static const char *format_left = "# written on %Y-%m-%d %H:%M:%S with flags ";
	size_t format_left_len = strlen(format_left);
	static const char *format_right = " using gsf-lib " GSF_VERSION;
	size_t format_right_len = strlen(format_right);
	int flags_start = 0;
	int flags_len = 0;
	while (flag_range % 2 != 1) {
		flags_start++;
		flag_range >>= 1;
		flags >>= 1;
	}
	while (flag_range) {
		flags_len++;
		flag_range >>= 1;
	}
	size_t format_len = format_left_len + flags_len + format_right_len;

	char* format = gsf_alloc(format_len + 1);
	char* format_ptr = format;
	memcpy(format_ptr, format_left, format_left_len);
	format_ptr += format_left_len;
	for (int i = flags_len - 1; i >= 0; i--) {
		if ((flags >> i) & 1)	*format_ptr++ = '1';
		else					*format_ptr++ = '0';
	}
	memcpy(format_ptr, format_right, format_right_len);
	format_ptr += format_right_len;
	*format_ptr++ = '\0';
	time_t raw_time;
	time(&raw_time);
	struct tm* time_info = localtime(&raw_time);
	// 3 = 2 for year + 1 for '\0' (later swapped for '\n')
	GSF_RS(format_len + 3);
	VPTR += strftime(PTR, GSF_SL, format, time_info);
	*PTR++ = '\n';
	gsf_free(format);
	return 1;
}

/*
// TODO error handling
char* gsf_txt_schema_write(const gsf_schema* schema, char* buf, size_t buf_size, int flags) {
	gsf_schema* master_schema = gsf_get_master_schema();
	if (!master_schema) GSF_RAISE(gsf_emem);
	gsf_article* schema_article = gsf_article_new(master_schema);
	if (!schema_article) GSF_RAISE(gsf_emem);
	gsf_object* root = gsf_value_encode_object(gsf_article_access_root(schema_article), 0);
	if (!root) GSF_RAISE(gsf_emem);
	const gsf_descriptor* descriptor = gsf_schema_get_descriptor(schema);
	gsf_value_encode_str(gsf_object_access_at_key(root, "group"),
						 gsf_descriptor_get_group(descriptor));
	gsf_value_encode_str(gsf_object_access_at_key(root, "name"),
						 gsf_descriptor_get_name(descriptor));
	gsf_value_encode_str(gsf_object_access_at_key(root, "version"),
						 gsf_descriptor_get_version(descriptor));
	const char* root_str = gsf_typing_to_string(gsf_schema_get_root(schema));
	gsf_value_encode_str(gsf_object_access_at_key(root, "root"), root_str);

	size_t label_str_len = 0;
	char* label_str = 0;
	size_t class_count = gsf_schema_class_count(schema);
	gsf_map* classes = gsf_value_encode_map(gsf_object_access_at_key(root, "classes"), 0, class_count);
	for (size_t class_index = 0; class_index < class_count; class_index++) {
		const gsf_class* class = gsf_schema_get_class(schema, class_index);
		gsf_value* class_value = gsf_map_add_element(classes, gsf_class_get_name(class));
		size_t label_count = gsf_class_label_count(class);
		gsf_map* labels = gsf_value_encode_map(class_value, 0, label_count);
		for (size_t label_index = 0; label_index < label_count; label_index++) {
			const gsf_label* label = gsf_class_get_label(class, label_index);
			gsf_value* label_value = gsf_map_add_element(labels, gsf_label_get_key(label));
			const gsf_typing* typing = gsf_label_get_typing(label);
			const char* label_str = gsf_typing_to_string(typing);
			size_t label_len = strlen(label_str);
			const gsf_value* default_val = gsf_label_get_default(label);
			if (default_val) { // FIXME old default type / default value style
				const gsf_type* default_val_type = gsf_value_get_type(default_val);
				bool write_type = !gsf_type_match(default_val_type, gsf_typing_get_default(typing));
				size_t new_label_len = label_len + 3 + (write_type ? 256 : 0);
				char* new_label_str = gsf_realloc(label_str, new_label_len);
				if (!new_label_str) {
					gsf_free(label_str);
					GSF_RAISE(gsf_emem);
				}
				label_str = new_label_str;
				strcpy(label_str + label_len, write_type ? " = " : " =");
				if (write_type) {
					gsf_type_to_string_buf(label_str + label_len + 3, 256, default_val_type);
					new_label_len = strlen(label_str);
				}
				label_str[new_label_len] = '\0';
				label_len = new_label_len;
			}
			const char* doc = gsf_label_get_doc(label);
			if (doc && !(flags & GSF_TXT_WRITE_SCHEMA_NO_LABEL_DOC)) {
				size_t doc_len = strlen(doc);
				size_t new_label_len = label_len + 3 + doc_len;
				char* new_label_str = gsf_realloc(label_str, new_label_len + 1);
				if (!new_label_str) {
					gsf_free(label_str);
					GSF_RAISE(gsf_emem);
				}
				label_str = new_label_str;
				strcpy(label_str + label_len, " / ");
				memcpy(label_str + label_len + 3, doc, doc_len);
				label_str[new_label_len] = '\0';
				label_len = new_label_len;
			} else {
				char* new_label_str = gsf_realloc(label_str, label_len + 1);
				if (!new_label_str) {
					gsf_free(label_str);
					GSF_RAISE(gsf_emem);
				}
			}
			// TODO add default value itself
			gsf_value_encode_str(label_value, label_str);
			gsf_free(label_str);
		}
	}


	char* ptr = buf;

	if (flags & GSF_TXT_WRITE_SIGNATURE) {
		ptr = write_signature(
			ptr, GSF_SL, GSF_TXT_WRITE_COMMON_BYTES | GSF_TXT_WRITE_SCHEMA_BYTES, flags
		);
		if (!ptr) return 0;
		if (!(flags & GSF_TXT_WRITE_NO_HEADER_NEWLINE)) {
			// article won't add header so newline would be skipped
			GSF_RS(1);
			*ptr++ = '\n';
		}
	}

	// copy article flags, remove 'signature' flag and add 'no descriptor' flag
	int article_flags = (flags & GSF_TXT_WRITE_COMMON_BYTES)
			& ~GSF_TXT_WRITE_SIGNATURE | GSF_TXT_WRITE_ARTICLE_NO_DESCRIPTOR
			| GSF_TXT_WRITE_ARTICLE_STRING_BLEND;
	return gsf_txt_article_write(schema_article, ptr, GSF_SL, article_flags);
}*/

int gsf_txt_article_write(struct gsf_writer* writer, const gsf_article* article) {
	GSF_RS(1);
	bool has_header = false;
	if (writer->flags & GSF_TXT_WRITE_SIGNATURE) {
		if (!write_signature(
			writer, GSF_TXT_WRITE_COMMON_BYTES | GSF_TXT_WRITE_ARTICLE_BYTES, writer->flags
		)) return 0;
		has_header = true;
	}

	if (!(writer->flags & GSF_TXT_WRITE_ARTICLE_NO_DESCRIPTOR) && gsf_article_get_schema(article)) {
		const gsf_descriptor* desc =
				gsf_schema_get_descriptor(gsf_article_get_schema(article));
		const char* group = gsf_descriptor_get_group(desc);
		size_t group_len = strlen(group);
		const char* name = gsf_descriptor_get_name(desc);
		size_t name_len = strlen(name);
		const char* version = gsf_descriptor_get_version(desc);
		size_t version_len = strlen(version);
		GSF_RS(4 + group_len + name_len + version_len);
		*PTR++ = '@';
		VPTR = strcpy(PTR, group) + group_len;
		*PTR++ = ':';
		VPTR = strcpy(PTR, name) + name_len;
		*PTR++ = ':';
		VPTR = strcpy(PTR, version) + version_len;
		*PTR++ = '\n';
		has_header = true;
	}

	const gsf_value* root = gsf_article_access_root(article);
	const gsf_type* default_root = gsf_typing_get_default(gsf_value_get_typing(root));
	const gsf_type* root_type = gsf_value_get_type(root);
	int match_result = default_root ? gsf_type_match(root_type, default_root) : 0;
	if (match_result < 0) return 0;
	if (writer->flags & GSF_TXT_WRITE_ARTICLE_STRONG_ROOT || !match_result) {
		GSF_RS_TYPE();
		*PTR++ = '!';
		VPTR = gsf_type_to_string_buf(root_type, PTR, GSF_SL);
		if (!PTR) return 0;
		GSF_RS(1);
		*PTR++ = '\n';
		has_header = true;
	}

	if (!has_header) writer->flags |= GSF_TXT_WRITE_NO_HEADER_NEWLINE;
	if (!gsf_txt_value_write(writer, root, 0)) return 0;
	if (PTR && writer->flags & GSF_TXT_WRITE_LEADING_ZERO) {
		GSF_RS(1);
		*PTR++ = '\0';
	}
	return 1;
}

int gsf_txt_value_write(struct gsf_writer* writer, const gsf_value* value, size_t depth) {
	const gsf_type* type = gsf_value_get_type(value);
	if (!type) GSF_RAISE(gsf_evalunset);
	if (gsf_type_is_null(type)) {
		GSF_RS(1);
		*PTR++ = '\n';
		return 1;
	} else if (gsf_type_is_scalar(type)) {
		return gsf_txt_scalar_write(writer, value);
	} else if (gsf_type_is_string(type)) {
		return gsf_txt_string_write(writer, value, depth);
	} else if (gsf_type_is_array(type)) {
		return gsf_txt_array_write(writer, value, depth);
	} else if (gsf_type_is_container(type)) {
		return gsf_txt_container_write(writer, value, depth);
	} else if (gsf_type_is_enum(type)) {
		return gsf_txt_enum_write(writer, value, depth);
	} else GSF_RAISE(gsf_eukntype);
}

int gsf_txt_scalar_write(struct gsf_writer* writer, const gsf_value* value) {
	GSF_RS(1);
	*PTR++ = ' ';

	const gsf_type* type = gsf_value_get_type(value);
	enum scalar_type scalar_type = get_scalar_type(type);
	if (scalar_type == -1) GSF_RAISE(gsf_ewrongtype);
	if (!write_scalar[scalar_type](writer, gsf_value_decode_any(value))) return 0;

	GSF_RS(1);
	*PTR++ = '\n';
	return 1;
}

// TODO support strings with no last endline somehow (here and in read)
int gsf_txt_string_write(struct gsf_writer* writer, const gsf_value* value, size_t depth) {
	const char* string = gsf_value_decode_str(value);
	if (!strchr(string, '\n')) {
		// one liner
		GSF_RS(1);
		*PTR++ = ' ';
		size_t string_len = strlen(string);
		GSF_RS(string_len + 1);
		VPTR = memcpy(PTR, string, string_len) + string_len;
		*PTR++ = '\n';
		return 1;
	}
	GSF_RS(1);
	*PTR++ = '\n';
	while (*string != '\0') {
		const char* end = strchr(string, '\n');
		if (!end) end = strchr(string, '\0');
		size_t line_len = end - string;
		GSF_RS(depth * 2 + line_len + 1);
		for (size_t i = 0; i < depth * 2; i++) {
			*PTR++ = ' ';
		}
		VPTR = memcpy(PTR, string, line_len) + line_len;
		*PTR++ = '\n';
		if (*end == '\0') break;
		string = end + 1;
	}
	return 1;
}

int gsf_txt_enum_write(struct gsf_writer* writer, const gsf_value* value, size_t depth) {
	const char* variant_str = gsf_value_decode_enum_str(value);
	size_t variant_len = strlen(variant_str);
	GSF_RS(1 + variant_len + 1);
	*PTR++ = ' ';
	VPTR = memcpy(PTR, variant_str, variant_len) + variant_len;
	*PTR++ = '\n';
	return 1;
}

int gsf_txt_array_write(struct gsf_writer* writer, const gsf_value* value, size_t depth) {
	// TODO multiline if a lot of values? maybe a flag?
	const gsf_array* array = gsf_value_decode_array(value);
	const gsf_type* elem = gsf_type_get_element_type(gsf_value_get_type(value));
	enum scalar_type scalar_type = get_scalar_type(elem);
	if (scalar_type == -1) GSF_RAISE(gsf_ewrongtype);
	const void* vals = gsf_array_as_any(array);
	uint8_t width = gsf_type_scalar_width(elem);
	GSF_RS(1); // ' '
	for (size_t i = 0; i < gsf_array_get_size(array); i++) {
		*PTR++ = ' ';
		if (!write_scalar[scalar_type](writer, vals)) return 0;
		GSF_RS(2); // ';' and ' ' or '\n'
		*PTR++ = ';';
		vals += width;
	}
	*PTR++ = '\n';
	return 1;
}

int gsf_txt_container_write(struct gsf_writer* writer, const gsf_value* value, size_t depth) {
	if (depth != 0 || !(writer->flags & GSF_TXT_WRITE_NO_HEADER_NEWLINE)) {
		GSF_RS(1);
		*PTR++ = '\n';
	}

	const void* container;
	const gsf_type* type = gsf_value_get_type(value);
	const gsf_type* (*get_element_info)(const char** key_out, const void* container, size_t index, int flags);
	int (*write_element)(struct gsf_writer* writer, const void* container, size_t index, size_t depth);
	size_t elements_amount;
	if (gsf_type_is_list(type)) {
		container = gsf_value_decode_list(value);
		elements_amount = gsf_list_element_count(container);
		get_element_info = &gsf_txt_list_get_element;
		write_element = &gsf_txt_list_write_element;
	} else if (gsf_type_is_map(type)) {
		container = gsf_value_decode_map(value);
		elements_amount = gsf_map_element_count(container);
		get_element_info = &gsf_txt_map_get_element;
		write_element = &gsf_txt_map_write_element;
	} else if (gsf_type_is_object(type)) {
		container = gsf_value_decode_object(value);
		elements_amount = gsf_class_label_count(gsf_object_get_class(container));
		get_element_info = &gsf_txt_object_get_element;
		write_element = &gsf_txt_object_write_element;
	} else GSF_RAISEF(gsf_eukntype, "unknown type '%s'", gsf_type_to_string(type));
	if (!container) return 0;

	for (size_t index = 0; index < elements_amount; index++) {
		const char* key = 0;
		const gsf_type* elem_type = get_element_info(&key, container, index, writer->flags);
		if (!elem_type && gsf_errno != 0) return 0;

		GSF_RS(depth * 2);
		for (size_t i = 0; i < depth * 2; i++) {
			*PTR++ = ' ';
		}

		if (key) {
			size_t key_len = strlen(key);
			GSF_RS(key_len);
			VPTR = memcpy(PTR, key, key_len) + key_len;
		}
		if (elem_type) {
			GSF_RS_TYPE();
			*PTR++ = '.';
			VPTR = gsf_type_to_string_buf(elem_type, PTR, GSF_SL);
		}
		GSF_RS(1);
		*PTR++ = ':';

		if (!write_element(writer, container, index, depth + 1)) return 0;
	}

	return 1;
}

const gsf_type* gsf_txt_list_get_element(const char** key_out, const void* list, size_t index, int flags) {
	const gsf_type* default_type = gsf_typing_get_default(gsf_list_get_typing(list));
	const gsf_type* type = gsf_value_get_type(gsf_list_get_element(list, index));
	int match_result = default_type ? gsf_type_match(type, default_type) : 0;
	if (match_result < 0) return 0;
	if (flags & GSF_TXT_WRITE_ARTICLE_STRONG_TYPES || !match_result) return type;
	else return 0;
}

const gsf_type* gsf_txt_map_get_element(const char** key_out, const void* map, size_t index, int flags) {
	*key_out = gsf_map_get_key(map, index);
	const gsf_type* default_type = gsf_typing_get_default(gsf_map_get_typing(map));
	const gsf_type* type = gsf_value_get_type(gsf_map_get_value(map, index));
	int match_result = default_type ? gsf_type_match(type, default_type) : 0;
	if (match_result < 0) return 0;
	if (flags & GSF_TXT_WRITE_ARTICLE_STRONG_TYPES || !match_result) return type;
	else return 0;
}

const gsf_type* gsf_txt_object_get_element(const char** key_out, const void* object, size_t index, int flags) {
	const gsf_label* label = gsf_class_get_label(gsf_object_get_class(object), index);
	*key_out = gsf_label_get_key(label);
	const gsf_type* default_type = gsf_typing_get_default(gsf_label_get_typing(label));
	const gsf_type* type = gsf_value_get_type(gsf_object_access_at_label(object, label));
	if (!type) GSF_RAISEF(
		gsf_evalunset, "label %s of object of class %s",
		*key_out, gsf_class_get_name(gsf_object_get_class(object))
	);
	int match_result = default_type ? gsf_type_match(type, default_type) : 0;
	if (match_result < 0) return 0;
	if (flags & GSF_TXT_WRITE_ARTICLE_STRONG_TYPES || !match_result) return type;
	else return 0;
}

int gsf_txt_list_write_element(struct gsf_writer* writer, const void* list, size_t index, size_t depth) {
	const gsf_value* value = gsf_list_get_element(list, index);
	return gsf_txt_value_write(writer, value, depth);
}

int gsf_txt_map_write_element(struct gsf_writer* writer, const void* map, size_t index, size_t depth) {
	const gsf_value* value = gsf_map_get_value(map, index);
	return gsf_txt_value_write(writer, value, depth);
}

int gsf_txt_object_write_element(struct gsf_writer* writer, const void* object, size_t index, size_t depth) {
	const gsf_value* value = gsf_object_access_at_label(object, gsf_class_get_label(gsf_object_get_class(object), index));
	return gsf_txt_value_write(writer, value, depth);
}


static enum scalar_type get_scalar_type(const gsf_type* type) {
	// TODO replace this after type system enhancements
	unsigned char width = gsf_type_scalar_width(type);
	if (gsf_type_is_int(type)) {
		if (width == 1) return i8;
		else if (width == 2) return i16;
		else if (width == 4) return i32;
		else if (width == 8) return i64;
	} else if (gsf_type_is_uint(type)) {
		if (width == 1) return u8;
		else if (width == 2) return u16;
		else if (width == 4) return u32;
		else if (width == 8) return u64;
	} else if (gsf_type_is_float(type)) {
		if (width == 4) return f32;
		else if (width == 8) return f64;
	}
	return -1;
}

#define write_int_fn(S, A)				\
static int write_i##S(					\
		struct gsf_writer* writer,		\
		const int##S##_t* value) {		\
	int##S##_t x = *value;				\
	if (x < 0) {						\
		GSF_RS(1);						\
		*PTR++ = '-';					\
	}									\
	uint##S##_t absolute = A(x);		\
	return write_u##S(writer,			\
		&absolute);						\
}

write_int_fn(8, abs)
write_int_fn(16, abs)
write_int_fn(32, abs)
write_int_fn(64, llabs)
#undef write_int_fn

#define write_uint_fn(S)				\
static int write_u##S(					\
		struct gsf_writer* writer,		\
		const uint##S##_t* value) {		\
	uint##S##_t x = *value;				\
	if (x == 0) {						\
		GSF_RS(1);						\
		*PTR++ = '0';					\
		return 1;						\
	}									\
	unsigned len = 0;					\
	while (x > 0) {						\
		x /= 10;						\
		len++;							\
	}									\
	GSF_RS(len);						\
	VPTR += len - 1;					\
	x = *value;							\
	while (x > 0) {						\
		GSF_RS(1);						\
		*PTR-- = '0' + (x % 10);		\
		x /= 10;						\
	}									\
	VPTR += len + 1;					\
	return 1;							\
}

write_uint_fn(8)
write_uint_fn(16)
write_uint_fn(32)
write_uint_fn(64)
#undef write_uint_fn

// TODO precision?
static int write_f32(struct gsf_writer* writer, const float* value) {
	float x = *value;
	unsigned len = snprintf(0, 0, "%.9f", x);
	GSF_RS(len + 1);
	VPTR += snprintf(PTR, len + 1, "%.9f", x);
	return 1;
}

static int write_f64(struct gsf_writer* writer, const double* value) {
	double x = *value;
	unsigned len = snprintf(0, 0, "%.17lg", x);
	GSF_RS(len + 1);
	VPTR += snprintf(PTR, len + 1, "%.17lg", x);
	return 1;
}
