#include "gsf_data.h"

#include <string.h>
#include <stdbool.h>
#include "gsf_types.h"
#include "gsf_itm.h"
#include "gsf_util.c"

struct gsf_array {
	const gsf_type* value_type;
	size_t size;
	union {
		void* any;
		int8_t* i08; int16_t* i16; int32_t* i32; int64_t* i64;
		uint8_t* u08; uint16_t* u16; uint32_t* u32; uint64_t* u64;
		float* f32; double* f64;
	} values;
};
void gsf_destroy_array(gsf_array* array);

struct gsf_list {
	const gsf_typing* element_typing;
	size_t size;
	size_t count;
	gsf_value* elements;
};
void gsf_destroy_list(gsf_list* list);

struct gsf_map {
	const gsf_typing* element_typing;
	size_t size;
	size_t count;
	char** keys;
	gsf_value* values;
};
void gsf_destroy_map(gsf_map* map);

struct gsf_object {
	const gsf_class* class;
	gsf_value* values;
	size_t* next; // is a pointer for valid access in const objects

};
gsf_object* gsf_create_object(const gsf_class* class);
void gsf_destroy_object(gsf_object* object);

struct gsf_value {
	const gsf_typing* typing;
	const gsf_type* current_type;
	union {
		// FIXME primitives are the reason why the value holder system is kinda dumb
		int8_t i08; int16_t i16; int32_t i32; int64_t i64;
		uint8_t u08; uint16_t u16; uint32_t u32; uint64_t u64;
		float f32; double f64;
		char* str;
		gsf_array* arr; gsf_list* lst; gsf_map* map;
		gsf_object* obj; const gsf_variant* enu;
	} value;
};
gsf_value* gsf_create_value(const gsf_typing* typing);
void gsf_destroy_value(gsf_value* value);
const void* gsf_value_decode_any(const gsf_value* holder);
void* gsf_value_encode_any(gsf_value* holder, const gsf_type* type); // doesn't check validity
void gsf_value_any_fail(gsf_value* holder);

struct gsf_article {
	bool schemaless;
	union {
		const gsf_schema* schema;
		gsf_itm* itm;
	};
	gsf_value* root;
};


gsf_article* gsf_create_article(void) {
	return gsf_calloc(sizeof(gsf_article));
}

void gsf_delete_article(gsf_article* article) {
	if (!article) return;
	gsf_article_destroy(article);
	gsf_free(article);
}

gsf_article* gsf_article_init(gsf_article* article, const gsf_schema* schema) {
	if (!article) GSF_RAISE(gsf_enullptr);
	article->root = gsf_calloc(sizeof(gsf_value));
	if (!article->root) GSF_RAISE(gsf_emem);
	if (schema) {
		article->schemaless = false;
		article->root->typing = gsf_schema_get_root(schema);
		article->schema = schema;
	} else {
		article->schemaless = true;
		article->itm = gsf_create_itm();
		if (!article->itm || !gsf_itm_init(article->itm)) return 0;
		article->root->typing = gsf_itm_typing_any(article->itm);
	}
	return article;
}

gsf_article* gsf_article_new(gsf_schema* schema) {
	gsf_article* article = gsf_create_article();
	if (article) gsf_article_init(article, schema);
	if (!article) gsf_delete_article(article);
	return article;
}

void gsf_article_destroy(gsf_article* article) {
	if (!article || !article->root) return;
	gsf_destroy_value(article->root);
	gsf_free(article->root);
	if (article->schemaless) gsf_delete_itm(article->itm);
	article->root = 0;
}

const gsf_schema* gsf_article_get_schema(const gsf_article* article) {
	if (article->schemaless) return 0;
	return article->schema;
}

gsf_value* (gsf_article_access_root)(const gsf_article* article) {
	return article->root;
}

gsf_object* gsf_create_object(const gsf_class* class) {
	gsf_object* object = gsf_alloc(sizeof(gsf_object));
	size_t values_size = gsf_class_label_count(class);
	gsf_value* values = gsf_calloc(values_size * sizeof(gsf_value));
	size_t* next = gsf_calloc(sizeof(size_t));
	if (!object || !values || !next) {
		gsf_free(object);
		gsf_free(values);
		gsf_free(next);
		GSF_RAISE(gsf_emem);
	}
	for (size_t i = 0; i < values_size; i++) {
		values[i].typing = gsf_label_get_typing(gsf_class_get_label(class, i));
	}
	object->class = class;
	object->values = values;
	object->next = next;
	return object;
}

void gsf_destroy_object(gsf_object* object) {
	if (!object) return;
	for (size_t i = 0; i < gsf_class_label_count(object->class); i++) {
		gsf_destroy_value(object->values + i);
	}
	gsf_free(object->values);
	gsf_free(object->next);
}

const gsf_class* gsf_object_get_class(const gsf_object* object) {
	if (!object) GSF_RAISE(gsf_enullptr);
	return object->class;
}

gsf_value* (gsf_object_access_at_label)(const gsf_object* object, const gsf_label* label) {
	if (!object) GSF_RAISE(gsf_enullptr);
	size_t index = gsf_class_index_at_label(object->class, label);
	if (!index--) return 0;
	*object->next = index + 1;
	return object->values + index;
}

gsf_value* (gsf_object_access_at_key)(const gsf_object* object, const char* key) {
	if (!object) GSF_RAISE(gsf_enullptr);
	size_t index = gsf_class_index_at_key(object->class, key);
	if (!index--) return 0;
	*object->next = index + 1;
	return object->values + index;
}

void gsf_object_access_iter(const gsf_object* object) {
	*object->next = 0;
}

gsf_value* (gsf_object_access_next)(const gsf_object* object) {
	if (*object->next >= gsf_class_label_count(object->class)) return 0;
	return object->values + (*object->next)++;
}

size_t gsf_object_apply_defaults(gsf_object* object) {
	if (!object) GSF_RAISE(gsf_enullptr);
	size_t count = 0;
	for (size_t i = 0; i < gsf_class_label_count(object->class); i++) {
		const gsf_label* label = gsf_class_get_label(object->class, i);
		gsf_value* value = gsf_object_access_at_label(object, label);
		if (gsf_value_get_type(value) != 0) continue;
		const gsf_value* default_val = gsf_label_get_default(label);
		if (default_val == 0) continue;
		gsf_value_copy(value, default_val);
	}
	return count;
}

size_t gsf_object_count_unset(const gsf_object* object) {
	size_t result = 0;
	const gsf_class* class = gsf_object_get_class(object);
	for (size_t i = 0; i < gsf_class_label_count(class); i++) {
		const gsf_label* label = gsf_class_get_label(class, i);
		if (!gsf_value_get_type(gsf_object_access_at_label(object, label))) {
			result++;
		}
	}
	return result;
}

size_t gsf_list_get_size(const gsf_list* list) {
	if (!list) GSF_RAISE(gsf_enullptr);
	return list->size;
}

gsf_list* gsf_list_resize(gsf_list* list, size_t size) {
	if (!list) GSF_RAISE(gsf_enullptr);
	gsf_value* to_destroy = 0;
	if (list->size > size) {
		// shrinking - we need to destroy the elements
		size_t to_destroy_size = (list->size - size) * sizeof(gsf_value);
		to_destroy = gsf_alloc(to_destroy_size);
		if (!to_destroy) GSF_RAISE(gsf_emem);
		memcpy(to_destroy, list->elements + size, to_destroy_size);
	}
	gsf_value* elements = gsf_crealloc(list->elements, list->size * sizeof(gsf_value), size * sizeof(gsf_value));
	if (!elements) GSF_RAISE(gsf_emem);
	if (to_destroy) {
		for (gsf_value* val = to_destroy; to_destroy - val < list->size - size; val++) {
			gsf_destroy_value(val);
		}
		gsf_free(to_destroy);
	}
	for (gsf_value* val = elements + list->size; val - elements < size; val++) {
		val->typing = list->element_typing;
	}
	list->size = size;
	list->elements = elements;
	return list;
}


size_t gsf_list_element_count(const gsf_list* list) {
	if (!list) GSF_RAISE(gsf_enullptr);
	return list->count;
}

gsf_value* (gsf_list_get_element)(const gsf_list* list, size_t index) {
	if (!list) GSF_RAISE(gsf_enullptr);
	if (index >= list->count) return 0;
	return list->elements + index;
}

gsf_value* gsf_list_add_element(gsf_list* list) {
	if (!list) GSF_RAISE(gsf_enullptr);
	if (list->count == list->size) {
		list->size++;
		gsf_value* elements = gsf_crealloc(list->elements, (list->size - 1) * sizeof(gsf_value), list->size * sizeof(gsf_value));
		if (!elements) {
			list->size--;
			GSF_RAISE(gsf_emem);
		}
		list->elements = elements;
		(list->elements + list->count)->typing = list->element_typing;
	}
	return list->elements + list->count++;
}

int gsf_list_remove_element(gsf_list* list, size_t index) {
	if (!list) GSF_RAISE(gsf_enullptr);
	if (index >= list->count) return 0;
	gsf_destroy_value(list->elements + index);
	memmove(list->elements + index, list->elements + index + 1, (list->count - index - 1) * sizeof(gsf_value));
	list->count -= 1;
	return 1;
}

const gsf_typing* gsf_list_get_typing(const gsf_list* list) {
	return list->element_typing;
}

size_t gsf_map_get_size(const gsf_map* map) {
	if (!map) GSF_RAISE(gsf_enullptr);
	return map->size;
}

gsf_map* gsf_map_resize(gsf_map* map, size_t size) {
	if (!map) GSF_RAISE(gsf_enullptr);
	char** keys_to_destroy = 0;
	gsf_value* vals_to_destroy;
	if (map->size > size) {
		// shrinking - we need to destroy the elements
		size_t keys_to_destroy_size = (map->size - size) * sizeof(char*);
		size_t vals_to_destroy_size = (map->size - size) * sizeof(gsf_value);
		keys_to_destroy = gsf_alloc(keys_to_destroy_size);
		vals_to_destroy = gsf_alloc(vals_to_destroy_size);
		if (!keys_to_destroy || !vals_to_destroy) GSF_RAISE(gsf_emem);
		memcpy(keys_to_destroy, map->keys + size, keys_to_destroy_size);
		memcpy(vals_to_destroy, map->values + size, vals_to_destroy_size);
	}
	char** keys = gsf_realloc(map->keys, size * sizeof(char*));
	if (!keys) GSF_RAISE(gsf_emem);
	gsf_value* values = gsf_crealloc(map->values, map->size * sizeof(gsf_value), size * sizeof(gsf_value));
	if (!values) {
		map->keys = keys;
		GSF_RAISE(gsf_emem);
	}
	if (keys_to_destroy) {
		for (size_t i = 0; i < map->size - size; i++) {
			gsf_free(keys_to_destroy[i]);
			gsf_destroy_value(vals_to_destroy + i);
		}
		gsf_free(keys_to_destroy);
		gsf_free(vals_to_destroy);
	}
	for (gsf_value* value = values + map->size; value - values < size; value++) {
		value->typing = map->element_typing;
	}
	map->size = size;
	map->keys = keys;
	map->values = values;
	return map;
}

size_t gsf_map_element_count(const gsf_map* map) {
	if (!map) GSF_RAISE(gsf_enullptr);
	return map->count;
}

const char* gsf_map_get_key(const gsf_map* map, size_t index) {
	if (!map) GSF_RAISE(gsf_enullptr);
	if (index >= map->count) return 0;
	return map->keys[index];
}

gsf_value* (gsf_map_get_value)(const gsf_map* map, size_t index) {
	if (!map) GSF_RAISE(gsf_enullptr);
	if (index >= map->count) return 0;
	return map->values + index;
}

gsf_value* (gsf_map_get_value_at_key)(const gsf_map* map, const char* query) {
	if (!map || !query) GSF_RAISE(gsf_enullptr);
	for (size_t i = 0; i < map->count; i++) {
		if (strcmp(map->keys[i], query) == 0) return map->values + i;
	}
	return 0;
}

gsf_value* gsf_map_add_element(gsf_map* map, const char* key) {
	if (!map) GSF_RAISE(gsf_enullptr);
	if (gsf_map_get_value_at_key(map, key)) GSF_RAISE(gsf_ekeytaken);
	if (map->count == map->size) {
		map->size++;
		char** keys = gsf_realloc(map->keys, map->size * sizeof(char**));
		if (!keys) {
			map->size--;
			GSF_RAISE(gsf_emem);
		}
		gsf_value* values = gsf_crealloc(map->values, (map->size - 1) * sizeof(gsf_value), map->size * sizeof(gsf_value));
		if (!values) {
			map->keys = keys; // that one was successfully reallocated
			map->size--;
			GSF_RAISE(gsf_emem);
		}
		map->keys = keys;
		map->values = values;
		(map->values + map->count)->typing = map->element_typing;
	}
	char* new_key = gsf_alloc(strlen(key) + 1);
	if (!new_key) GSF_RAISE(gsf_emem);
	strcpy(new_key, key);
	map->keys[map->count] = new_key;
	return map->values + map->count++;
}

int gsf_map_remove_element(gsf_map* map, size_t index) {
	if (!map) GSF_RAISE(gsf_enullptr);
	if (index >= map->count) return 0;
	gsf_free(map->keys[index]);
	gsf_destroy_value(map->values + index);
	memmove(map->keys + index, map->keys + index + 1, (map->count - index - 1) * sizeof(char*));
	memmove(map->values + index, map->values + index + 1, (map->count - index - 1) * sizeof(gsf_value));
	map->count -= 1;
	return 1;
}

int gsf_map_remove_at_key(gsf_map* map, const char* query) {
	if (!map || !query) GSF_RAISE(gsf_enullptr);
	for (size_t i = 0; i < map->count; i++) {
		if (strcmp(map->keys[i], query) != 0) continue;
		return gsf_map_remove_element(map, i);
	}
	return 0;
}

const gsf_typing* gsf_map_get_typing(const gsf_map* map) {
	return map->element_typing;
}

size_t gsf_array_get_size(const gsf_array* array) {
	return array->size;
}

gsf_array* gsf_array_resize(gsf_array* array, size_t size) {
	void* values = gsf_crealloc(
			array->values.any, array->size * gsf_type_scalar_width(array->value_type),
			size * gsf_type_scalar_width(array->value_type));
	if (!values) GSF_RAISE(gsf_emem);
	array->values.any = values;
	array->size = size;
	return array;
}

const gsf_type* gsf_array_get_type(const gsf_array* array) {
	return array->value_type;
}

void* gsf_array_as_any(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	return holder->values.any;
}

int8_t* gsf_array_as_i8(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "i8");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.i08;
}

int16_t* gsf_array_as_i16(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "i16");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.i16;
}

int32_t* gsf_array_as_i32(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "i32");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.i32;
}

int64_t* gsf_array_as_i64(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "i64");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.i64;
}

uint8_t* gsf_array_as_u8(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "u8");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.u08;
}

uint16_t* gsf_array_as_u16(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "u16");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.u16;
}

uint32_t* gsf_array_as_u32(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "u32");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.u32;
}

uint64_t* gsf_array_as_u64(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "u64");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.u64;
}

float* gsf_array_as_f32(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "f32");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.f32;
}

double* gsf_array_as_f64(const gsf_array* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	int result = gsf_type_match(holder->value_type, "f64");
	if (result < 0) return 0;
	if (result == 0) GSF_RAISE(gsf_eforbtype);
	return holder->values.f64;
}


gsf_value* gsf_create_value(const gsf_typing* typing) {
	gsf_value* value = gsf_calloc(sizeof(gsf_value));
	value->typing = typing;
	return value;
}

void gsf_destroy_value(gsf_value* value) {
	if (!value) return;
	if (value->current_type) {
		if (gsf_type_is_string(value->current_type)) {
			gsf_free(value->value.str);
		} else if (gsf_type_is_array(value->current_type)) {
			gsf_destroy_array(value->value.arr);
			gsf_free(value->value.arr);
		} else if (gsf_type_is_list(value->current_type)) {
			gsf_destroy_list(value->value.lst);
			gsf_free(value->value.lst);
		}  else if (gsf_type_is_map(value->current_type)) {
			gsf_destroy_map(value->value.map);
			gsf_free(value->value.map);
		} else if (gsf_type_is_object(value->current_type)) {
			gsf_destroy_object(value->value.obj);
			gsf_free(value->value.obj);
		}
	}
	value->current_type = 0;
}

gsf_value* gsf_value_copy(gsf_value* dst, const gsf_value* src) {
	const gsf_typing* typing = dst->typing;
	const gsf_type* type = 0;
	if (gsf_typing_match_type(typing, src->current_type, &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(dst);
	dst->typing = typing;
	dst->current_type = type;
	if (gsf_type_is_scalar(type) || gsf_type_is_enum(type)) {
		memcpy(&dst->value, &src->value, sizeof(void*));
	} else if (gsf_type_is_string(type)) {
		dst->value.str = gsf_alloc(strlen(src->value.str) + 1);
		if (!dst->value.str) GSF_RAISE(gsf_emem);
		strcpy(dst->value.str, src->value.str);
	} else if (gsf_type_is_array(type)) {
		const gsf_array* src_array = src->value.arr;
		gsf_array* dst_array = gsf_value_encode_array(dst, src_array->value_type, src_array->size);
		if (!dst_array) return 0;
		size_t size = src_array->size * gsf_type_scalar_width(src_array->value_type);
		memcpy(dst_array->values.any, src_array->values.any, size);
		dst->value.arr = dst_array;
	} else if (gsf_type_is_list(type)) {
		const gsf_list* src_list = src->value.lst;
		gsf_list* dst_list = gsf_value_encode_list(dst, src_list->element_typing, src_list->size);
		if (!dst_list) return 0;
		for (size_t i = 0; i < gsf_list_element_count(src_list); i++) {
			gsf_value* dst_element = gsf_list_add_element(dst_list);
			if (!dst_element) {
				gsf_destroy_list(dst_list);
				gsf_free(dst_list);
				return 0;
			}
			const gsf_value* src_element = gsf_list_get_element(src_list, i);
			if (!gsf_value_copy(dst_element, src_element)) {
				gsf_destroy_list(dst_list);
				gsf_free(dst_list);
				return 0;
			}
		}
		dst->value.lst = dst_list;
	} else if (gsf_type_is_map(type)) {
		const gsf_map* src_map = src->value.map;
		gsf_map* dst_map = gsf_value_encode_map(dst, src_map->element_typing, src_map->size);
		if (!dst_map) return 0;
		for (size_t i = 0; i < gsf_map_element_count(src_map); i++) {
			gsf_value* dst_element = gsf_map_add_element(dst_map, gsf_map_get_key(src_map, i));
			if (!dst_element) {
				gsf_destroy_map(dst_map);
				gsf_free(dst_map);
				return 0;
			}
			const gsf_value* src_element = gsf_map_get_value(src_map, i);
			if (!gsf_value_copy(dst_element, src_element)) {
				gsf_destroy_map(dst_map);
				gsf_free(dst_map);
				return 0;
			}
		}
		dst->value.map = dst_map;
	} else if (gsf_type_is_object(type)) {
		const gsf_object* src_object = src->value.obj;
		const gsf_class* class = gsf_type_get_object_class(type);
		gsf_object* dst_object = gsf_value_encode_object(dst, class);
		if (!dst_object) return 0;
		for (size_t i = 0; i < gsf_class_label_count(class); i++) {
			const gsf_label* label = gsf_class_get_label(class, i);
			const gsf_value* src_element = gsf_object_access_at_label(src_object, label);
			gsf_value* dst_element = gsf_object_access_at_label(dst_object, label);
			if (!src_element || !gsf_value_copy(dst_element, src_element)) {
				gsf_destroy_object(dst_object);
				gsf_free(dst_object);
				return 0;
			}
		}
		dst->value.obj = dst_object;
	}
	return dst;
}

const gsf_typing* gsf_value_get_typing(const gsf_value* value) {
	if (!value) GSF_RAISE(gsf_enullptr);
	return value->typing;
}

const gsf_type* gsf_value_get_type(const gsf_value* value) {
	if (!value) GSF_RAISE(gsf_enullptr);
	return value->current_type;
}

void gsf_value_unset(gsf_value* value) {
	const gsf_typing* typing = value->typing;
	gsf_destroy_value(value);
	value->typing = typing;
}

int gsf_value_decode_null(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	return 1;
}

gsf_value* gsf_value_encode_null(gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "nul", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	return holder;
}

const gsf_object* gsf_value_decode_object(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	if (!gsf_type_is_object(holder->current_type)) GSF_RAISE(gsf_eforbtype);
	return holder->value.obj;
}

gsf_object* gsf_value_encode_object(gsf_value* holder, const gsf_class* class) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type;
	if (class) {
		type = gsf_class_as_type(class);
		int result = gsf_typing_match_type(holder->typing, type, &type);
		if (result < 0) return 0;
		if (!result) GSF_RAISE(gsf_eforbtype);
	} else {
		type = gsf_typing_get_default(holder->typing);
		if (!type) GSF_RAISE(gsf_enullptr); // null 'class'
		if (!gsf_type_is_object(type)) GSF_RAISE(gsf_eforbtype);
		class = gsf_type_get_object_class(type);
	}
	gsf_object* object = gsf_create_object(class);
	if (!object) return 0;
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.obj = object;
	return object;
}

gsf_object* gsf_value_as_object(gsf_value* holder, const gsf_class* class) {
	if (!class) {
		const gsf_type* default_type = gsf_typing_get_default(gsf_value_get_typing(holder));
		if (!default_type || !gsf_type_is_object(default_type)) {
			if (!default_type) GSF_RAISE(gsf_enullptr); // null 'class'
			else GSF_RAISE(gsf_eforbtype);
		}
		class = gsf_type_get_object_class(default_type);
	}
	if (
		holder->current_type &&
		gsf_type_is_object(holder->current_type) &&
		gsf_type_get_object_class(holder->current_type) == class
	) return holder->value.obj;
	else return gsf_value_encode_object(holder, class);
}

void gsf_destroy_map(gsf_map* map) {
	if (!map) return;
	for (size_t i = 0; i < map->count; i++) {
		gsf_free(map->keys[i]);
		gsf_destroy_value(map->values + i);
	}
	gsf_free(map->keys);
	gsf_free(map->values);
}

const gsf_map* gsf_value_decode_map(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	if (!gsf_type_is_map(holder->current_type)) GSF_RAISE(gsf_eforbtype);
	return holder->value.map;
}

gsf_map* gsf_value_encode_map(gsf_value* holder, const gsf_typing_or_char* typ, size_t size) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type;
	if (typ) {
		type = gsf_itm_type_make_map(gsf_typing_manager(holder->typing), typ);
		if (!type) return 0;
		int result = gsf_typing_match_type(holder->typing, type, &type);
		if (result < 0) return 0;
		if (!result) GSF_RAISE(gsf_eforbtype);
	} else {
		type = gsf_typing_get_default(holder->typing);
		if (!type) GSF_RAISE(gsf_enullptr); // null 'typ'
		if (!gsf_type_is_map(type)) GSF_RAISE(gsf_eforbtype);
	}
	gsf_map* map = gsf_alloc(sizeof(gsf_map));
	char** keys = gsf_alloc(size * sizeof(char*));
	gsf_value* values = gsf_calloc(size * sizeof(gsf_value));
	if (!map || !keys || !values) {
		gsf_free(map);
		gsf_free(keys);
		gsf_free(values);
		GSF_RAISE(gsf_emem);
	}
	map->element_typing = gsf_type_get_element_typing(type);
	for (gsf_value* val = values; val - values < size; val++) {
		val->typing = map->element_typing;
	}
	map->count = 0;
	map->size = size;
	map->keys = keys;
	map->values = values;
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.map = map;
	return map;
}

gsf_map* gsf_value_as_map(gsf_value* holder, const gsf_typing_or_char* typ, size_t size) {
	if (!typ) {
		const gsf_type* default_type = gsf_typing_get_default(gsf_value_get_typing(holder));
		if (!default_type || !gsf_type_is_map(default_type)) {
			if (!default_type) GSF_RAISE(gsf_enullptr); // null 'typ'
			else GSF_RAISE(gsf_eforbtype);
		}
		typ = gsf_type_get_element_typing(default_type);
	}
	if (holder->current_type) {
		int match_res = gsf_typing_match(gsf_type_get_element_typing(holder->current_type), typ);
		if (match_res < 0) return 0;
		if (gsf_type_is_map(holder->current_type) && match_res) {
			gsf_map* map = holder->value.map;
			if (map->size < size) gsf_map_resize(map, size);
			return map;
		}
	}
	return gsf_value_encode_map(holder, typ, size);
}

void gsf_destroy_list(gsf_list* list) {
	if (!list) return;
	for (size_t i = 0; i < list->count; i++) {
		gsf_destroy_value(list->elements + i);
	}
	gsf_free(list->elements);
}

const gsf_list* gsf_value_decode_list(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	if (!gsf_type_is_list(holder->current_type)) GSF_RAISE(gsf_eforbtype);
	return holder->value.lst;
}

gsf_list* gsf_value_encode_list(gsf_value* holder, const gsf_typing_or_char* typ, size_t size) {
	if (!holder) GSF_RAISE(gsf_enullptr);

	const gsf_type* type;
	if (typ) {
		type = gsf_itm_type_make_list(gsf_typing_manager(holder->typing), typ);
		if (!type) return 0;
		int result = gsf_typing_match_type(holder->typing, type, &type);
		if (result < 0) return 0;
		if (!result) GSF_RAISE(gsf_eforbtype);
	} else {
		type = gsf_typing_get_default(holder->typing);
		if (!type) GSF_RAISE(gsf_enullptr); // null 'typ'
		if (!gsf_type_is_list(type)) GSF_RAISE(gsf_eforbtype);
	}

	gsf_list* list = gsf_alloc(sizeof(gsf_list));
	gsf_value* elements = gsf_calloc(size * sizeof(gsf_value));
	if (!list || !elements) {
		gsf_free(list);
		gsf_free(elements);
		GSF_RAISE(gsf_emem);
	}
	list->element_typing = gsf_type_get_element_typing(type);
	for (gsf_value* val = elements; val - elements < size; val++) {
		val->typing = list->element_typing;
	}
	list->count = 0;
	list->size = size;
	list->elements = elements;
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.lst = list;
	return list;
}


gsf_list* gsf_value_as_list(gsf_value* holder, const gsf_typing_or_char* typ, size_t size) {
	if (!typ) {
		const gsf_type* default_type = gsf_typing_get_default(gsf_value_get_typing(holder));
		if (!default_type || !gsf_type_is_list(default_type)) {
			if (!default_type) GSF_RAISE(gsf_enullptr); // null 'typ'
			else GSF_RAISE(gsf_eforbtype);
		}
		typ = gsf_type_get_element_typing(default_type);
	}
	if (holder->current_type) {
		int match_res = gsf_typing_match(gsf_type_get_element_typing(holder->current_type), typ);
		if (match_res < 0) return 0;
		if (gsf_type_is_list(holder->current_type) && match_res) {
			gsf_list* list = holder->value.lst;
			if (list->size < size) gsf_list_resize(list, size);
			return list;
		}
	}
	return gsf_value_encode_list(holder, typ, size);
}

void gsf_destroy_array(gsf_array* array) {
	if (!array) return;
	gsf_free(array->values.any);
}

const gsf_array* gsf_value_decode_array(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	if (!gsf_type_is_array(holder->current_type)) GSF_RAISE(gsf_eforbtype);
	return holder->value.arr;
}

gsf_array* gsf_value_encode_array(gsf_value* holder, const gsf_type_or_char* typ, size_t size) {
	if (!holder) GSF_RAISE(gsf_enullptr);

	const gsf_type* type;
	if (typ) {
		type = gsf_itm_type_make_array(gsf_typing_manager(holder->typing), typ);
		if (!type) return 0;
		int result = gsf_typing_match_type(holder->typing, type, &type);
		if (result < 0) return 0;
		if (!result) GSF_RAISE(gsf_eforbtype);
	} else {
		type = gsf_typing_get_default(holder->typing);
		if (!type) GSF_RAISE(gsf_enullptr); // null 'typ'
		if (!gsf_type_is_array(type)) GSF_RAISE(gsf_eforbtype);
	}

	gsf_array* array = gsf_alloc(sizeof(gsf_array));
	void* values = gsf_calloc(size * gsf_type_scalar_width(gsf_type_get_element_type(type)));
	if (!array || !values) {
		gsf_free(array);
		gsf_free(values);
		GSF_RAISE(gsf_emem);
	}
	array->value_type = gsf_type_get_element_type(type);
	array->size = size;
	array->values.any = values;
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.arr = array;
	return array;
}

gsf_array* gsf_value_as_array(gsf_value* holder, const gsf_type_or_char* typ, size_t size) {
	if (!typ) {
		const gsf_type* default_type = gsf_typing_get_default(gsf_value_get_typing(holder));
		if (!default_type || !gsf_type_is_array(default_type)) {
			if (!default_type) GSF_RAISE(gsf_enullptr); // null 'typ'
			else GSF_RAISE(gsf_eforbtype);
		}
		typ = gsf_type_get_element_type(default_type);
	}
	if (holder->current_type) {
		int match_result = gsf_type_match(gsf_type_get_element_type(holder->current_type), typ);
		if (match_result < 0) return 0;
		if (gsf_type_is_array(holder->current_type) && match_result) {
			gsf_array* array = holder->value.arr;
			if (array->size < size) gsf_array_resize(array, size);
			return array;
		}
	}
	return gsf_value_encode_array(holder, typ, size);
}

const gsf_variant* gsf_value_decode_enum(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_is_enum(holder->current_type);
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	return holder->value.enu;
}
int gsf_value_encode_enum(gsf_value* holder, const gsf_enum* enum_, const gsf_variant* variant) {
	if (!holder) GSF_RAISE(gsf_enullptr);

	const gsf_type* type;
	if (enum_) {
		type = gsf_enum_as_type(enum_);
		int result = gsf_typing_match_type(holder->typing, type, &type);
		if (result < 0) return 0;
		if (!result) GSF_RAISE(gsf_eforbtype);
	} else {
		type = gsf_typing_get_default(holder->typing);
		if (!type) GSF_RAISE(gsf_enullptr); // null 'enum_'
		if (!gsf_type_is_enum(type)) GSF_RAISE(gsf_eforbtype);
		enum_ = gsf_type_get_enum(type);
	}

	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.enu = variant;
	return 1;
}

const char* gsf_value_decode_enum_str(const gsf_value* holder) {
	const gsf_variant* variant = gsf_value_decode_enum(holder);
	if (!variant) return 0;
	return gsf_variant_get_name(variant);
}

int gsf_value_encode_enum_str(gsf_value* holder, const gsf_enum* type, const char* name) {
	// TODO support null 'type'
	gsf_variant* variant = gsf_enum_get_variant_by_name(type, name);
	if (!variant) GSF_RAISEF(gsf_euknenuvar, "got '%s' for enum '%s'", name, gsf_enum_get_name(type));
	return gsf_value_encode_enum(holder, type, variant);
}

int gsf_value_decode_enum_idx(const gsf_value* holder, size_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_variant* variant = gsf_value_decode_enum(holder);
	if (!variant) return 0;
	if (!gsf_enum_index_of(gsf_type_get_enum(holder->current_type), variant, out)) return 0;
	return 1;
}
int gsf_value_encode_enum_idx(gsf_value* holder, const gsf_enum* type, size_t variant) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!gsf_value_encode_enum(holder, type, 0)) return 0;
	holder->value.enu = gsf_enum_get_variant(gsf_type_get_enum(holder->current_type), variant);
	if (!holder->value.enu) {
		gsf_value_unset(holder);
		return 0;
	}
	return 1;
}

int gsf_value_decode_i8(const gsf_value* holder, int8_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "i8");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.i08;
	return 1;
}

gsf_value* gsf_value_encode_i8(gsf_value* holder, int8_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "i8", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.i08 = value;
	return holder;
}

int gsf_value_decode_i16(const gsf_value* holder, int16_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "i16");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.i16;
	return 1;
}

gsf_value* gsf_value_encode_i16(gsf_value* holder, int16_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "i16", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.i16 = value;
	return holder;
}

int gsf_value_decode_i32(const gsf_value* holder, int32_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "i32");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.i32;
	return 1;
}

gsf_value* gsf_value_encode_i32(gsf_value* holder, int32_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "i32", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.i32 = value;
	return holder;
}

int gsf_value_decode_i64(const gsf_value* holder, int64_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "i64");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.i64;
	return 1;
}

gsf_value* gsf_value_encode_i64(gsf_value* holder, int64_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "i64", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.i64 = value;
	return holder;
}

int gsf_value_decode_u8(const gsf_value* holder, uint8_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "u8");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.u08;
	return 1;
}

gsf_value* gsf_value_encode_u8(gsf_value* holder, uint8_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "u8", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.u08 = value;
	return holder;
}

int gsf_value_decode_u16(const gsf_value* holder, uint16_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "u16");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.u16;
	return 1;
}

gsf_value* gsf_value_encode_u16(gsf_value* holder, uint16_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "u16", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.u16 = value;
	return holder;
}

int gsf_value_decode_u32(const gsf_value* holder, uint32_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "u32");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.u32;
	return 1;
}

gsf_value* gsf_value_encode_u32(gsf_value* holder, uint32_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "u32", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.u32 = value;
	return holder;
}

int gsf_value_decode_u64(const gsf_value* holder, uint64_t* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "u64");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.u64;
	return 1;
}

gsf_value* gsf_value_encode_u64(gsf_value* holder, uint64_t value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "u64", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.u64 = value;
	return holder;
}

int gsf_value_decode_f32(const gsf_value* holder, float* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "f32");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.f32;
	return 1;
}

gsf_value* gsf_value_encode_f32(gsf_value* holder, float value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "f32", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.f32 = value;
	return holder;
}

int gsf_value_decode_f64(const gsf_value* holder, double* out) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "f64");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	*out = holder->value.f64;
	return 1;
}

gsf_value* gsf_value_encode_f64(gsf_value* holder, double value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "f64", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_eforbtype);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.f64 = value;
	return holder;
}

const char* gsf_value_decode_str(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	int result = gsf_type_match(holder->current_type, "str");
	if (result < 0) return 0;
	if (!result) GSF_RAISE(gsf_eforbtype);
	return holder->value.str;
}

gsf_value* gsf_value_encode_str(gsf_value* holder, const char* value) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	const gsf_type* type = 0;
	if (gsf_typing_match_type(holder->typing, "str", &type) < 0) return 0;
	if (!type) GSF_RAISE(gsf_ewrongtype);
	char* string = gsf_alloc(strlen(value) + 1);
	if (!string) GSF_RAISE(gsf_emem);
	gsf_destroy_value(holder);
	holder->current_type = type;
	holder->value.str = string;
	strcpy(holder->value.str, value);
	return holder;
}

const void* gsf_value_decode_any(const gsf_value* holder) {
	if (!holder) GSF_RAISE(gsf_enullptr);
	if (!holder->current_type) GSF_RAISE(gsf_evalunset);
	return &holder->value;
}

void* gsf_value_encode_any(gsf_value* holder, const gsf_type* type) {
	holder->current_type = type;
	return &holder->value;
}

void gsf_value_any_fail(gsf_value* holder) {
	holder->current_type = 0;
}
