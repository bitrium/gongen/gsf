#include "gsf_codec.h"

#include <string.h>

#include "gsf_types.h"
#include "gsf_data.h"
#include "gsf_err.h"

#include "gsf_util.c"

extern const void* gsf_value_decode_any(const gsf_value* holder);

// current position
#define PTR (uint8_t*) writer->ptr
// uncasted current position
#define VPTR writer->ptr
// current space left
#define GSF_SL (writer->end - writer->ptr)
// require space
#define GSF_RS(space) do {								\
	if (GSF_SL >= space) break;							\
	int ret = writer->write(writer, 2048 + GSF_SL);		\
	if (ret <= 0) {										\
		if (ret == 0) GSF_RAISE(gsf_eshortbuf);			\
		return 0;										\
	}													\
	if (GSF_SL < space) GSF_RAISE(gsf_eshortbuf);		\
} while (0)

int gsf_bin_value_write(struct gsf_writer* writer, const gsf_value* value);
int gsf_bin_vsize_write(struct gsf_writer* writer, size_t value);

int gsf_bin_article_write(struct gsf_writer* writer, const gsf_article* article) {
	GSF_RS(1);

	if (gsf_article_get_schema(article)) {
		const gsf_descriptor* desc = gsf_schema_get_descriptor(gsf_article_get_schema(article));
		const char* group = gsf_descriptor_get_group(desc);
		size_t group_size = strlen(group) + 1;
		const char* name = gsf_descriptor_get_name(desc);
		size_t name_size = strlen(name) + 1;
		const char* version = gsf_descriptor_get_version(desc);
		size_t version_size = strlen(version) + 1;
		size_t desc_size = group_size + name_size + version_size;
		// descriptor size
		if (!gsf_bin_vsize_write(writer, desc_size)) return 0;
		GSF_RS(desc_size);
		// descriptor
		memcpy(PTR, group, group_size);
		VPTR += group_size;
		memcpy(PTR, name, name_size);
		VPTR += name_size;
		memcpy(PTR, version, version_size);
		VPTR += version_size;
	} else {
		// schemaless
		if (!gsf_bin_vsize_write(writer, 0)) return 0;
	}

	// root value
	return gsf_bin_value_write(writer, gsf_article_access_root(article));
}

int gsf_bin_value_write(struct gsf_writer* writer, const gsf_value* holder) {
	const gsf_typing* typing = gsf_value_get_typing(holder);
	const gsf_type* type = gsf_value_get_type(holder);
	if (gsf_typing_is_any(typing)) {
		// free type choice
		// TODO use binary type format
		size_t type_len = strlen(gsf_type_to_string(type));
		if (!gsf_bin_vsize_write(writer, type_len)) return 0;
		GSF_RS(type_len);
		GSF_ASSERT(VPTR = gsf_type_to_string_buf(type, VPTR, type_len));
	} else if (gsf_typing_is_omnibus(typing)) {
		// type selection
		size_t choice;
		GSF_ASSERT(gsf_typing_find(typing, type, &choice) > 0);
		if (!gsf_bin_vsize_write(writer, choice)) return 0;
	}
	if (gsf_type_is_null(type)) {
	} else if (gsf_type_is_scalar(type)) {
		size_t width = gsf_type_scalar_width(type);
		GSF_RS(width);
		const void* value = gsf_value_decode_any(holder);
		// value
		memcpy(PTR, value, width);
		VPTR += width;
	} else if (gsf_type_is_enum(type)) {
		size_t index;
		if (!gsf_value_decode_enum_idx(holder, &index)) return 0;
		if (!gsf_bin_vsize_write(writer, index)) return 0;
	} else if (gsf_type_is_string(type)) {
		const char* string = gsf_value_decode_str(holder);
		size_t count = strlen(string);
		// size
		if (!gsf_bin_vsize_write(writer, count)) return 0;
		GSF_RS(count);
		// chars
		memcpy(PTR, string, count);
		VPTR += count;
	} else if (gsf_type_is_array(type)) {
		const gsf_array* array = gsf_value_decode_array(holder);
		// count
		size_t count = gsf_array_get_size(array);
		if (!gsf_bin_vsize_write(writer, count)) return 0;
		size_t width = gsf_type_scalar_width(gsf_array_get_type(array));
		size_t space = count * width;
		GSF_RS(space);
		void* values = gsf_array_as_any(array);
		// values
		memcpy(PTR, values, space);
		VPTR += space;
	} else if (gsf_type_is_object(type)) {
		const gsf_object* object = gsf_value_decode_object(holder);
		const gsf_class* class = gsf_object_get_class(object);
		// count
		size_t count = gsf_class_label_count(class);
		if (!gsf_bin_vsize_write(writer, count)) return 0;
		// starts (set in loop)
		// GSF_RS(count * sizeof(uint64_t));
		// uint64_t* starts_ptr = (uint64_t*) ptr;
		// ptr += count * sizeof(uint64_t);
		// fields
//		void* fields = ptr;
		for (size_t i = 0; i < count; i++) {
			const gsf_label* label = gsf_class_get_label(class, i);
			// start
			// *starts_ptr++ = (uint64_t) (ptr - fields);
			// index
			if (!gsf_bin_vsize_write(writer, i)) return 0;
			// value
			const gsf_value* child = gsf_object_access_at_label(object, label);
			if (!gsf_bin_value_write(writer, child)) return 0;
		}
	} else if (gsf_type_is_list(type)) {
		const gsf_list* list = gsf_value_decode_list(holder);
		// count
		size_t count = gsf_list_element_count(list);
		if (!gsf_bin_vsize_write(writer, count)) return 0;
		// starts (set in loop)
		// GSF_RS(count * sizeof(uint64_t));
		// void* starts_ptr = ptr;
		// ptr += count * sizeof(uint64_t);
		// values
		for (size_t i = 0; i < count; i++) {
			// start
			// *((uint64_t*) starts_ptr) = (uint64_t) (ptr - fields);
			// value
			const gsf_value* value = gsf_list_get_element(list, i);
			if (!gsf_bin_value_write(writer, value)) return 0;
		}
	} else if (gsf_type_is_map(type)) {
		const gsf_map* map = gsf_value_decode_map(holder);
		// count
		size_t count = gsf_map_element_count(map);
		if (!gsf_bin_vsize_write(writer, count)) return 0;
		// starts (set in loop)
		// GSF_RS(count * sizeof(uint64_t));
		// void* starts_ptr = ptr;
		// ptr += count * sizeof(uint64_t);
		// map
		for (size_t i = 0; i < count; i++) {
			// start
			// *((uint64_t*) starts_ptr) = (uint64_t) (ptr - fields);
			// key size
			const char* key = gsf_map_get_key(map, i);
			size_t key_size = strlen(key);
			if (!gsf_bin_vsize_write(writer, key_size)) return 0;
			// key
			GSF_RS(key_size);
			memcpy(PTR, key, key_size);
			VPTR += key_size;
			// value
			const gsf_value* value = gsf_map_get_value(map, i);
			if (!gsf_bin_value_write(writer, value)) return 0;
		}
	} else GSF_RAISE(gsf_ewrongtype);
	return 1;
}

int gsf_bin_vsize_write(struct gsf_writer* writer, size_t value) {
	// find biggest byte used
	uint8_t byte = (uint8_t) sizeof(size_t);
	if (byte > 8) byte = 8;
	uint64_t byte_bits = ~(SIZE_MAX >> 8);
	while (byte_bits > 0xFF) {
		if (value & byte_bits) break;
		byte_bits >>= 8;
		byte--;
	}

	// convert to variable length
	uint8_t i = 0;
	uint8_t bytes[9] = {0};
	while (byte > 0) {
		uint8_t value_byte = (value & byte_bits) >> (8 * (byte - 1));
		bytes[i] |= value_byte >> (8 - byte - 1) | 0x01;
		i++;
		bytes[i] |= value_byte << byte;
		byte--;
		byte_bits >>= 8;
	}

	// write value
	uint8_t count = i;
	i = 0;
	if (bytes[0] >> 1 == 0) {
		// skip first byte if empty
		i++;
	}
	GSF_RS(count - i + 1);
	do {
		*(PTR++) = bytes[i];
	} while (bytes[i++] & 0x01);

	return 1;
}
