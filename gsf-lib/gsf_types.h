/**
 * @file gsf_types.h
 * @brief Interface for querying properties of GSF types and typings.
 *
 * For manual creation or manipulation of types/typings, or general type/typing
 * management, see 'gsf_itm.h'.
 *
 * For definition/explanation of types and typings, see the GSF specification.
 */
#pragma once

#include <stdint.h>
#include "gsf_com.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void gsf_typing_or_char;
typedef void gsf_type_or_char;

typedef struct gsf_class gsf_class;
typedef struct gsf_enum gsf_enum;

/**
 * @brief Gets a typing specification with one type.
 *
 * This can be used for programmatical typing construction. More types can be added using
 * @link gsf_typing_with_type @endlink and the default can be changed or removed using
 * @link gsf_typing_with_default @endlink or @link gsf_typing_without_default @endlink.
 *
 * @param type The first and default type or its null-terminated string representation.
 * @return Produced typing specification or null pointer on error.
 */
const gsf_typing* gsf_typing_make(const gsf_type* type);
/**
 * @brief Gets an array type specification.
 * @param elems Element type specification or its null-terminated string representation.
 * @return Produced type specification or null pointer on error.
 */
const gsf_type* gsf_type_make_array(const gsf_type* elems);
/**
 * @brief Gets a list type specification.
 * @param elems Element typing specification or its null-terminated string representation.
 * @return Produced type specification or null pointer on error.
 */
const gsf_type* gsf_type_make_list(const gsf_typing* elems);
/**
 * @brief Gets a map type specification.
 * @param elems Element typing specification or its null-terminated string representation.
 * @return Produced type specification or null pointer on error.
 */
const gsf_type* gsf_type_make_map(const gsf_typing* elems);

/**
 * @brief Gets a copy of this typing but with another type available.
 *
 * `is_default` may be used for more flags in future.
 * @param self Original typing specification.
 * @param type The appended type or its null-terminated string representation.
 * @param is_default 1 if the type is to be the default or 0 in the other case.
 * @return Produced typing or null pointer on error.
 */
const gsf_typing* gsf_typing_with_type(const gsf_typing* self,
		const gsf_type_or_char* type, int is_default);
/**
 * @brief Gets a copy of this typing but with modified default.
 * @param self Original typing specification.
 * @param index Index of the new default type.
 * @return Produced typing or null pointer on error.
 */
const gsf_typing* gsf_typing_with_default(const gsf_typing* self, size_t index);
/**
 * @brief Gets a copy of this typing but with unset default.
 *
 * For non-omnibus typings (ones with just one type) this has no effect since typings
 * with one type will always use it as the default. Use only with omnibus typings.
 *
 * @param self Original typing specification.
 * @return Produced typing or null pointer on error.
 */
const gsf_typing* gsf_typing_without_default(const gsf_typing* self);

/**
 * @brief Writes a string representation of a typing specification to a buffer.
 * @see gsf_itm_typing_to_str_buf
 * @param self Typing specification.
 * @param buf Buffer for null-terminated string typing specification.
 * @param buf_size Max size to write.
 * @return End of written string or null pointer on error.
 */
char* gsf_typing_to_string_buf(const gsf_typing* self, char* buf, size_t buf_size);
/**
 * @brief Obtains a string representation of a typing specification.
 * @param self Typing specification.
 * @return The string representation.
 */
const char* gsf_typing_to_string(const gsf_typing* self);

int gsf_typing_is_any(const gsf_typing* self);
/**
 * @brief Gets amount of types of a typing.
 * @param self Typing specification.
 * @return The result count or 0 for the 'any' typing.
 */
size_t gsf_typing_type_count(const gsf_typing* self);
/**
 * @brief Gets the type specification listed by a typing specification by index.
 * @param self Typing specification.
 * @param index The type index.
 * @return The found manager-owned type or null pointer if index is out of bounds.
 */
const gsf_type* gsf_typing_get_type(const gsf_typing* self, size_t index);
/**
 * @brief Gets the type specification of the default type of a typing specification.
 * @param self Typing specification.
 * @return Default type specification or null poniter if there is no default.
 */
const gsf_type* gsf_typing_get_default(const gsf_typing* self);

/**
 * @brief Verifies whether a typing can hold a given type.
 * @param self Typing specification.
 * @param preset Checked type specification or its null-terminated representation.
 * @param found Output variable, on success set to matched type spec, or 0 for no action.
 * @return Positive on success, zero on fail, or negative on error.
 */
int gsf_typing_match_type(const gsf_typing* self, const gsf_type_or_char* preset,
		const gsf_type** found);
/**
 * @brief Verifies whether a typing fits into another.
 *
 * Unlike the strict variant, checked typing may miss types from preset.
 * Strictness also applies to children checks.
 * @param self Checked typing specification.
 * @param preset Preset specification to check against.
 * @return Positive on succecss, zero on fail, or negative on error.
 */
int gsf_typing_match(const gsf_typing* self, const gsf_typing_or_char* preset);
/**
 * @brief Verifies whether a typing matches another.
 * @param self Checked typing specification.
 * @param preset Preset specification to check against.
 * @return Positive on succecss, zero on fail, or negative on error.
 */
int gsf_typing_match_strict(const gsf_typing* self, const gsf_typing_or_char* preset);

/**
 * @brief Checks whether the typing provides any alternative.
 * @param self Checked typing specification.
 * @return Positive if allows more than one type, zero if there is no variety.
 */
int gsf_typing_is_omnibus(const gsf_typing* self);

/**
 * @brief Gets index of the matching type in a typing.
 * @param self Typing specification.
 * @param preset The requested type or its null-terminated string representation.
 * @param out Output variable (the resulting index).
 * @return Positive on succes, zero on error.
 */
int gsf_typing_find(const gsf_typing* self, const gsf_type_or_char* preset, size_t* out);


/**
 * @brief Writes a string representation of a type specification to a buffer.
 * @see gsf_itm_typing_to_str_buf
 * @param self Type specification.
 * @param buf Buffer for null-terminated string type specification.
 * @param buf_size Max size to write.
 * @return End of written string or null pointer on error.
 */
char* gsf_type_to_string_buf(const gsf_type* self, char* buf, size_t buf_size);
/**
 * @brief Obtains a string representation of a type specification.
 * @param self Type specification.
 * @return The string representation.
 */
const char* gsf_type_to_string(const gsf_type* self);

/**
 * @brief Verifies whether a type matches another.
 *
 * For element typing, @link gsf_typing_match @endlink is used.
 * @param self Checked type specification.
 * @param preset Type specification to check against.
 * @return Positive on success, zero on fail, or negative on error.
 */
int gsf_type_match(const gsf_type* self, const gsf_type_or_char* preset);
/**
 * @brief Verifies whether a type matches another.
 *
 * For element typing, @link gsf_typing_match_strict @endlink is used.
 * @param self Checked type specification.
 * @param preset Type specification to check against.
 * @return Positive on success, zero on fail, or negative on error.
 */
int gsf_type_match_strict(const gsf_type* self, const gsf_type_or_char* preset);

uint8_t gsf_type_scalar_width(const gsf_type* type);
int gsf_type_is_null(const gsf_type* self);
int gsf_type_is_primitive(const gsf_type* self); // also allows null
int gsf_type_is_string(const gsf_type* self);
int gsf_type_is_scalar(const gsf_type* self);
int gsf_type_is_int(const gsf_type* self);
int gsf_type_is_uint(const gsf_type* self);
int gsf_type_is_float(const gsf_type* self);
int gsf_type_is_enum(const gsf_type* self);
int gsf_type_is_container(const gsf_type* self);
int gsf_type_is_collection(const gsf_type* self);
int gsf_type_is_array(const gsf_type* self);
int gsf_type_is_list(const gsf_type* self);
int gsf_type_is_map(const gsf_type* self);
int gsf_type_is_object(const gsf_type* self);

/**
 * @brief For array types, gets the element type specification.
 * @param self "Parent" type specification.
 * @return Element type specification.
 */
const gsf_type* gsf_type_get_element_type(const gsf_type* self);
/**
 * @brief For list and map types, gets the element typing specification.
 * @param self "Parent" type specification.
 * @return Element typing specification.
 */
const gsf_typing* gsf_type_get_element_typing(const gsf_type* self);
/**
 * @brief For object types, gets the class of the objects.
 * @param self Type specification.
 * @return Object class for that type.
 */
const gsf_class* gsf_type_get_object_class(const gsf_type* self);
/**
 * @brief For enum types, gets the enum of the variants.
 * @param self Type specification.
 * @return Enum represented by that type.
 */
const gsf_enum* gsf_type_get_enum(const gsf_type* self);

#ifdef __cplusplus
} // extern "C"
#undef class
#endif
