#include "gsf_codec.h"

#include "gsf_context.h"
#include "gsf_data.h"
#include "gsf_util.c"

extern int gsf_context_add_entry(gsf_context* self, gsf_schema** schema_out, gsf_descriptor** desc_out);

extern int gsf_txt_schema_read(struct gsf_reader* reader, gsf_schema* schema, gsf_descriptor* descriptor);
extern int gsf_txt_article_read_desc(struct gsf_reader* reader, gsf_descriptor* descriptor);
extern int gsf_txt_article_read_proper(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema);
extern int gsf_txt_article_write(struct gsf_writer* writer, const gsf_article* article);

extern int gsf_bin_article_read_desc(struct gsf_reader* reader, gsf_descriptor* descriptor);
extern int gsf_bin_article_read_proper(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema);
extern int gsf_bin_article_write(struct gsf_writer* writer, const gsf_article* article);

static int reader_read_idle(struct gsf_reader* self, size_t count) {
	if (self->end - self->ptr >= count) return 1;
	return 0;
}

int gsf_reader_create_buf(struct gsf_reader* reader, const void* buf, size_t size) {
	reader->read = reader_read_idle;
	reader->user = 0;
	reader->ptr = buf;
	reader->end = buf + size;
	return 1;
}
void gsf_reader_delete_buf(struct gsf_reader* reader) {
	reader->read = 0;
	reader->ptr = 0;
	reader->end = 0;
}


static int writer_write_idle(struct gsf_writer* self, size_t count) {
	if (self->end - self->ptr >= count) return 1;
	return -1;
}

int gsf_writer_create_buf(struct gsf_writer* writer, void* buf, size_t size) {
	writer->write = writer_write_idle;
	writer->user = 0;
	writer->ptr = buf;
	writer->end = buf + size;
	return 1;
}
void gsf_writer_delete_buf(struct gsf_writer* writer) {
	writer->write = 0;
	writer->ptr = 0;
	writer->end = 0;
}

struct writer_growing_buf {
	void* own_buf;
	void** buf;
	size_t size;
};
static int writer_write_growing_buf(struct gsf_writer* self, size_t count) {
	struct writer_growing_buf* data = self->user;
	if (self->end - self->ptr >= count) return 1;
	count -= self->end - self->ptr;
	void* buf = gsf_realloc(*data->buf, data->size + count);
	if (!buf) GSF_RAISE(gsf_emem);
	data->size += count;
	self->ptr = buf + (self->ptr - *data->buf);
	*data->buf = buf;
	self->end = buf + data->size;
	return 1;
}
int gsf_writer_create_growing_buf(struct gsf_writer* writer, void** buf, size_t initial_size) {
	struct writer_growing_buf* data = gsf_alloc(sizeof(struct writer_growing_buf));
	if (!data) GSF_RAISE(gsf_emem);
	data->own_buf = 0;
	data->buf = buf;
	if (data->buf == 0) {
		data->own_buf = gsf_alloc(initial_size);
		if (!data->own_buf) GSF_RAISE(gsf_emem);
		data->buf = &data->own_buf;
	}
	data->size = initial_size;
	writer->write = writer_write_growing_buf;
	writer->user = data;
	writer->ptr = *data->buf;
	writer->end = *data->buf + data->size;
	return 1;
}
void gsf_writer_delete_growing_buf(struct gsf_writer* writer) {
	struct writer_growing_buf* data = writer->user;
	gsf_free(data->own_buf);
	gsf_free(data);
	writer->write = 0;
	writer->user = 0;
	writer->ptr = 0;
	writer->end = 0;
}


int gsf_read_schema(struct gsf_reader* reader, gsf_schema* schema, gsf_descriptor* desc) {
	if (reader->format == GSF_CODEC_TEXTUAL) {
		return gsf_txt_schema_read(reader, schema, desc);
	} else GSF_RAISE(gsf_ecfmtunsup);
}
int gsf_read_schema_to_ctx(struct gsf_reader* reader, gsf_context* ctx) {
	gsf_schema* schema;
	gsf_descriptor* desc;
	if (!gsf_context_add_entry(ctx, &schema, &desc)) return 0;
	if (!gsf_read_schema(reader, schema, desc)) return 0;
	return gsf_context_schema_count(ctx);
}
int gsf_read_article_desc(struct gsf_reader* reader, gsf_descriptor* desc) {
	if (reader->format == GSF_CODEC_TEXTUAL) {
		return gsf_txt_article_read_desc(reader, desc);
	} else if (reader->format == GSF_CODEC_BINARY) {
		return gsf_bin_article_read_desc(reader, desc);
	} else GSF_RAISE(gsf_ecfmtunsup);
}
int gsf_read_article_proper(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema) {
	if (reader->format == GSF_CODEC_TEXTUAL) {
		return gsf_txt_article_read_proper(reader, article, schema);
	} else if (reader->format == GSF_CODEC_BINARY) {
		return gsf_bin_article_read_proper(reader, article, schema);
	} else GSF_RAISE(gsf_ecfmtunsup);
}
int gsf_read_article(struct gsf_reader* reader, gsf_article* article, const gsf_schema* schema) {
	if (!gsf_read_article_desc(reader, 0)) {
		if (gsf_errno) return 0;
	}
	return gsf_read_article_proper(reader, article, schema);
}
int gsf_read_article_ctx(struct gsf_reader* reader, gsf_article* article, const gsf_context* ctx) {
	gsf_descriptor* desc = gsf_create_descriptor();
	if (!desc) return 0;
	if (!gsf_read_article_desc(reader, desc)) {
		if (gsf_errno) return 0;
		GSF_RAISE(gsf_euknschema, "no descriptor declaration");
	}
	const gsf_schema* schema = gsf_context_get_schema(ctx, desc);
	if (!schema) GSF_RAISEF(gsf_euknschema, "%s", gsf_descriptor_to_str(desc));
	gsf_delete_descriptor(desc);
	return gsf_read_article_proper(reader, article, schema);
}
int gsf_write_schema(struct gsf_writer* writer, const gsf_schema* schema) {
	GSF_RAISE(gsf_ecfmtunsup);
}
int gsf_write_article(struct gsf_writer* writer, const gsf_article* article) {
	if (writer->format == GSF_CODEC_TEXTUAL) {
		return gsf_txt_article_write(writer, article);
	} else if (writer->format == GSF_CODEC_BINARY) {
		return gsf_bin_article_write(writer, article);
	} else GSF_RAISE(gsf_ecfmtunsup);
}
