#include "gsf_com.h"

#include <memory.h>

void* (*gsf_alloc)(size_t) = &malloc;
void* gsf_calloc_function(size_t size) {
	void* p = gsf_alloc(size);
	memset(p, 0, size);
	return p;
}
void* (*gsf_calloc)(size_t) = &gsf_calloc_function;
void* (*gsf_realloc)(void*, size_t) = &realloc;
void* gsf_crealloc_function(void* p, size_t old_size, size_t new_size) {
	p = gsf_realloc(p, new_size);
	if (p && new_size > old_size) {
		memset(((char*) p) + old_size, 0, new_size - old_size);
	}
	return p;
}
void* (*gsf_crealloc)(void*, size_t, size_t) = &gsf_crealloc_function;
void (*gsf_free)(void*) = &free;

void gsf_set_allocator(void* fun_alloc(size_t), void *fun_realloc(void*, size_t), void fun_free(void*)) {
	if (fun_alloc) gsf_alloc = fun_alloc;
	if (fun_realloc) gsf_realloc = fun_realloc;
	if (fun_free) gsf_free = fun_free;
}


#include "gsf_types.h"
#include "gsf_data.h"

#define GSF_FAIL_MASTER() do {master_schema = 0; return 0;} while (0)
static gsf_schema* master_schema = 0;
gsf_schema* gsf_get_master_schema() {
	if (master_schema) return master_schema;

	gsf_descriptor* master_descriptor = gsf_descriptor_new("gsf", "Schema", "1");
	if (!master_descriptor) GSF_FAIL_MASTER();
	master_schema = gsf_schema_new(master_descriptor, "Schema", 1);
	if (!master_schema) GSF_FAIL_MASTER();
	gsf_class* master_class = gsf_schema_add_class(master_schema, "Schema", 5);
	if (!master_class) GSF_FAIL_MASTER();
	if (!gsf_class_add_label(master_class, "group", "str")) GSF_FAIL_MASTER();
	if (!gsf_class_add_label(master_class, "name", "str")) GSF_FAIL_MASTER();
	if (!gsf_class_add_label(master_class, "version", "str")) GSF_FAIL_MASTER();
	if (!gsf_class_add_label(master_class, "root", "str")) GSF_FAIL_MASTER();
	if (!gsf_class_add_label(master_class, "classes", "map[map[str]]")) GSF_FAIL_MASTER();
	gsf_label* label = gsf_class_add_label(master_class, "enums", "map[lst[str]]");
	if (!label) GSF_FAIL_MASTER();
	gsf_value_encode_map(gsf_label_access_default(label), 0, 0);
	return master_schema;
}
