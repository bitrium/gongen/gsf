# Gong Serialization Format

GSF is an all-purpose serialization format. With its built-in schema ecosystem, the format is great for large-scale
serialization needs while still leaving space for applications not needing any schemas (like simple configurations) by
providing the schemaless mode. Values written in GSF are hard-typed making article (document) validation easy and
leaving no space for parsing problems with types. GSF avoids having to mark the types every time though by using the
schemas.

The specification of the format is not yet available. Both the format and the libraries are still in early development
and stability is not yet achieved.

The format was originally created for the GongEn game engine, but soon its usage was expanded into multiple
side-projects. Its name is not only shortened to GSF, but sometimes also to GongSF to avoid name confusion.

# The libraries

This repository provides a **C library** that contains support for the GSF format. The library can be included using the
general `gongsf.h` header, but more specific headers are available under the `gongsf` directory. In this project all
headers are located in the `gsf-lib` directory together with sources, but during installation they are renamed to the
aforementioned locations. Functions and other symbols of the library are prefixed with `gsf_`.

A **Python binding** for the C library are available as the `gongsf` package. The C library is required to either be
installed on the system, or built with the binary and include directory paths set in the Python environment: the
`GSF_LIB` variable set to the binary shared library and the `GSF_INCLUDE` to the include directory.

Major and minor versions of both libraries are linked, but the patch versions are changed separately.

This repository also contains outdated code of a **C++ binding** which is unusable at the moment.
